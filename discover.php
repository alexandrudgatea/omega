<?php include ("templates/header.php"); ?>

<div id="mainContent" class="discover">
	<!--	this is the landingpage nav-->
	<nav class="navbar navbar-default page-navbar" role="navigation">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".page-menu">
					<span class="visible-xs mobile-menu-toggle"> Explore the OWLC <i class="fa fa-angle-down"></i> </span>
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".mobile-join">
					<span class="visible-xs mobile-menu"> Join</span>
				</button>

		</div>
		<div class="container-fluid">
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse page-menu">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#">About the OWLC</a></li>
					<!--page-nav dropdown begin-->
					<li class="dropdown">
						<a href="http://google.com" class="dropdown-toggle" data-toggle="dropdown">Programs</a>
						<ul class="dropdown-menu">
							<li><a href="http://google.com">2016 Workshops</a></li>
							<li><a href="http://google.com">Leadership in Sustainable Education Award</a></li>
							<li><a href="http://google.com">OCSL Past Programs</a></li>

							<!-- in case necessary this is markup for lvl2 dropdown-->
							<!--
								<li class="dropdown-submenu">
									<a class="submenu-drop" tabindex="-1" href="#">New dropdown </a>
									<ul class="dropdown-menu">
										<li><a href="#">2nd level dropdown</a></li>
										<li><a href="#">2nd level dropdown</a></li>
										<li><a href="#">2nd level dropdown</a></li>
									</ul>
								</li>
-->
						</ul>
					</li>
					<!--page-nav dropdown end-->
					<li><a href="#">Resources</a></li>
					<li><a href="#">Support the OWLC</a></li>
				</ul>
			</div>
			<div class="collapse navbar-collapse mobile-join">
				<ul class="nav navbar-nav navbar-right visible-xs">
					<li>
						<div class="lp-not-logged">
							<div class="container">
								<div class="row">
									<div class="col-sm-12 center-block text-center">
										<form class="form-inline">
											<div class="form-group">
												<label for="owlcSignUp">Get OWLC Updates: </label>
												<input type="email" class="form-control" id="owlcSignUp" placeholder="you@email.com">
											</div>
											<button type="submit" class="btn btn-default">Sign Up</button>
										</form>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 text-center">
										<a href="#" class="log-in">
												Already Registered? Login 
												<span class="fa-stack fa-lg">
													<i class="fa fa-circle fa-stack-1x"></i>
													<i class="fa fa-angle-right fa-stack-1x fa-inverse"></i>
												</span>
											</a>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="lp-logged">
							<div class="container">
								<div class="row">
									<div class="col-sm-12 center-block text-center">
										<form class="form-inline">
											<div class="form-group">
												<div class="checkbox">
													<label for="owlcUpdates">
															<input type="checkbox" value="" id="owlcUpdates">Get OWLC Updates
														</label>
												</div>
											</div>
										</form>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 text-center">
										<a href="#" class="view-profile interractive-text">
												View your profile 
												<span class="fa-stack fa-lg">
													<i class="fa fa-circle fa-stack-1x "></i>
													<i class="fa fa-angle-right fa-stack-1x fa-inverse"></i>
												</span>
											</a>
									</div>
								</div>
							</div>
						</div>
					</li>

				</ul>
			</div>
		</div>
		<!-- /.navbar-collapse -->
	</nav>

	<div id="hero" class="landingpage background-image" style="background: url('images/hero_owlc.jpg')">
		<div class="container full-height">
			<div class="row">
				<div class="col-sm-12 text-center">
					<div class="hero-content field-text">
						<h1 class="initiative-title">Omega Women’s Leadership Center</h1>
						<h3 class="tagline">Nurturing relationships, healthy families and communities.</h3>
						<a href="#" class="btn" target="_blank"><i class="fa fa-facebook"></i> Like OWLC on Facebook</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--	===== not logged content =====-->
	<div class="lp-not-logged">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 center-block text-center">
					<form class="form-inline">
						<div class="form-group">
							<label for="owlcSignUp">Get OWLC Updates: </label>
							<input type="email" class="form-control" id="owlcSignUp" placeholder="you@email.com">
						</div>
						<button type="submit" class="btn btn-default">Sign Up</button>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 text-center">
					<a href="#" class="log-in">
						Already Registered? Login 
						<span class="fa-stack fa-lg">
							<i class="fa fa-circle fa-stack-1x"></i>
							<i class="fa fa-angle-right fa-stack-1x fa-inverse"></i>
						</span>
					</a>
				</div>
			</div>
		</div>
	</div>

	<!--	===== logged in content =====-->
	<div class="lp-logged">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 center-block text-center">
					<form class="form-inline">
						<div class="form-group">
							<div class="checkbox">
								<label for="owlcUpdates">
										<input type="checkbox" value="" id="owlcUpdates">Get OWLC Updates
									</label>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 text-center">
					<a href="#" class="view-profile interractive-text">
						View your profile 
						<span class="fa-stack fa-lg">
							<i class="fa fa-circle fa-stack-1x "></i>
							<i class="fa fa-angle-right fa-stack-1x fa-inverse"></i>
						</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<!--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
	<!--===== video carosuel =====-->
	<div id="videoCarousel">
		<div class="video-carousel">
			<ul class="list-inline clearfix">
				<li class="item">
					<div class="tile-frame">
						<div class="panel panel-default panel-idea tile">
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite1">
									<label for="favourite1"></label>
								</div>
							</div>
							<div class="panel-heading" style="background: url('images/panel_idea3.jpg')">

							</div>
							<div class="panel-body panel-audio-video">
								<div class="content-type">
									<a href="#"><i class="fa fa-play-circle-o"></i></a>
									<span>Video</span>
								</div>
								<div class="title">
									<h1>Learning to Let Go</h1>
								</div>
								<div class="person">
									<ul class="featuring list-unstyled list-inline">
										<li>Bobby McFerrin</li>
										<li class="duration">00:13:12</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="item active">
					<div class="tile-frame">
						<div class="panel panel-default panel-idea tile">
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite2">
									<label for="favourite2"></label>
								</div>
							</div>
							<div class="panel-heading" style="background: url('images/panel_idea.jpg')">

							</div>
							<div class="panel-body panel-audio-video">
								<div class="content-type">
									<a href="#"><i class="fa fa-play-circle-o"></i></a>
									<span>Video</span>
								</div>
								<div class="title">
									<h1>Learning to Let Go</h1>
								</div>
								<div class="person">
									<ul class="featuring list-unstyled list-inline">
										<li>Bobby McFerrin</li>
										<li class="duration">00:13:12</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="item">
					<div class="tile-frame">
						<div class="panel panel-default panel-idea tile">
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite3">
									<label for="favourite3"></label>
								</div>
							</div>
							<div class="panel-heading" style="background: url('images/panel_idea2.jpg')">

							</div>
							<div class="panel-body panel-audio-video">
								<div class="content-type">
									<a href="#"><i class="fa fa-play-circle-o"></i></a>
									<span>Video</span>
								</div>
								<div class="title">
									<h1>Learning to Let Go</h1>
								</div>
								<div class="person">
									<ul class="featuring list-unstyled list-inline">
										<li>Bobby McFerrin</li>
										<li class="duration">00:13:12</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="item">
					<div class="tile-frame">
						<div class="panel panel-default panel-idea tile">
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite4">
									<label for="favourite4"></label>
								</div>
							</div>
							<div class="panel-heading" style="background: url('images/panel_idea3.jpg')">

							</div>
							<div class="panel-body panel-audio-video">
								<div class="content-type">
									<a href="#"><i class="fa fa-play-circle-o"></i></a>
									<span>Video</span>
								</div>
								<div class="title">
									<h1>Learning to Let Go</h1>
								</div>
								<div class="person">
									<ul class="featuring list-unstyled list-inline">
										<li>Bobby McFerrin</li>
										<li class="duration">00:13:12</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="item">
					<div class="tile-frame">
						<div class="panel panel-default tile  panel-workshop">
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite4">
									<label for="favourite4"></label>
								</div>
							</div>
							<div class="panel-body">
								<div class="content-type">
									<span>Workshop</span>
								</div>
								<div class="title">
									<h1>Collaborating With Nature</h1>
								</div>
								<div class="person">
									<ul class="list-unstyled list-inline person-images">
										<li>
											<img src="images/person.jpg">
										</li>
										<li> <img src="images/person.jpg"></li>
									</ul>
									<ul class="list-unstyled list-inline person-names">
										<li>Thomas Robertson</li>
										<li>Marry Anderson</li>
									</ul>
								</div>
								<div class="date-location">
									<ul class="list-inline">
										<li><em>Feb 28 - Mar 06 2016</em></li>
										<li><em>Rhinebeck, NY</em></li>
									</ul>
								</div>
							</div>
							<div class="panel-footer">
								<a href="#" class="expand-button">
									<i class="fa fa-plus"></i>
								</a>
								<div class="expanded-tile-content animated fadeInUp">
									<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
									<a href="#" class="btn white-transparent-button">Learn More</a>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="item">
					<div class="tile-frame">
						<div class="panel panel-default tile  panel-learn">
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite7">
									<label for="favourite7"></label>
								</div>
							</div>
							<div class="panel-body">
								<div class="content-type">
									<span>E-course</span>
								</div>
								<div class="title">
									<h1>Growing &amp; Sustaining A Yoga Service Organization</h1>
								</div>
								<div class="person">
									<ul class="list-unstyled list-inline person-images">
										<li>
											<img src="images/person.jpg">
										</li>
										<li> <img src="images/person.jpg"></li>
										<li> <img src="images/person.jpg"></li>
									</ul>
									<ul class="list-unstyled list-inline person-names">
										<li>Thomas Robertson</li>
										<li>Marry Anderson</li>
										<li>Marry Anderson</li>
									</ul>
								</div>
							</div>
							<div class="panel-footer">
								<a href="#" class="expand-button">
									<i class="fa fa-plus"></i>
								</a>
								<div class="expanded-tile-content animated fadeInUp">
									<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
									<a href="#" class="btn white-transparent-button">Learn More</a>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="item">
					<div class="tile-frame">
						<div class="panel panel-default tile  panel-collection">
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite10">
									<label for="favourite10"></label>
								</div>
							</div>
							<div class="panel-body">
								<div class="content-type">
									<span>Playlist</span>
								</div>
								<div class="title">
									<h1>Pema Chodron's Words of Wisdom</h1>
								</div>
								<div class="image-tiles">
									<ul class="list-inline">
										<li style="background:url('images/workshop_events.jpg');"></li>
										<li style="background:url('images/person.jpg');"></li>
										<li style="background:url('images/online_learning.jpg');"></li>
										<li style="background:url('images/idea.jpg');"></li>
									</ul>
								</div>
								<div class="person">
									<ul class="list-unstyled list-inline person-names">
										<li>Thomas Robertson</li>
										<li>Marry Anderson</li>
										<li>Marry Anderson</li>
									</ul>
								</div>
							</div>
							<div class="panel-footer">
								<a href="#" class="expand-button">
									<i class="fa fa-plus"></i>
								</a>
								<div class="expanded-tile-content animated fadeInUp">
									<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
									<a href="#" class="btn white-transparent-button">Learn More</a>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="item">
					<div class="tile-frame">
						<div class="panel panel-default tile  panel-category" style="background: url('images/workshop_events.jpg')">
							<div class="panel-body">
								<div class="category">
									<h1><a href="#">Workshops &amp; Events</a></h1>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="item">
					<div class="tile-frame">
						<div class="panel panel-default tile  panel-general-purpose">
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite13">
									<label for="favourite13"></label>
								</div>
							</div>
							<div class="panel-heading" style="background: url('images/panel_idea.jpg')">

							</div>
							<div class="panel-body panel-article">
								<div class="content-type">
									<span>Take Action</span>
								</div>
								<div class="title">
									<h1>Learning to let go</h1>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="item">
					<div class="tile-frame">
						<div class="panel panel-default tile  panel-person">
							<div class="panel-body clearfix">
								<div class="person-image">
									<img src="images/person.jpg">
								</div>
								<div class="person-bio">
									<span class="block field-text person-ocupation">										Teacher
									</span>
									<span class="block field-text person-name">
										Armando Tolken
									</span>
									<span class="block field-text person-info">
										Armando is a great guy and knows so much about life, happiness, and where to get an amazing lobster roll in Santa Fe, he also …
									</span>
								</div>
							</div>

						</div>
					</div>
				</li>
			</ul>
		</div>
		<div class="jcarousel-control-prev left carousel-control"></div>
		<div class="jcarousel-control-next right carousel-control"></div>
	</div>
	<!--===TAB CONTENT===-->
	<div role="tabpanel">
		<!-- Nav tabs -->
		<div class="container">
			<!--tabs-->
			<div class="row">
				<div class="col-sm-12 lp-tabs">
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active">
							<a href="#owlcEverything" aria-controls="owlcEverything" role="tab" data-toggle="tab">Everything</a>
						</li>
						<li role="presentation">
							<a href="#owlcVideo" aria-controls="owlcVideo" role="tab" data-toggle="tab">Video Library</a>
						</li>
						<li role="presentation">
							<a href="#owlcAction" aria-controls="owlcAction" role="tab" data-toggle="tab">OWLC in Action</a>
						</li>
					</ul>
					<button type="button" class="btn filter-btn mobile-filter-btn visible-xs" data-toggle="modal" data-target=".filterModal"><i class="fa fa-filter"></i> Filter By Topics </button>
				</div>


			</div>
			<!--filter list-->
			<div class="row">


				<div class="col-sm-12">
					<div class="view-filters">
						<form action="/" method="get" id="views-exposed-form-discover-workshop-events-panel-pane-1" accept-charset="UTF-8" class="jquery-once-1-processed">
							<div>
								<div class="views-exposed-form form-workshops-events">

									<div class="row">
										<div class="col-sm-12 lp-filter form-inline">

											<div class="form-group">
												<input type="text" class="form-control" id="edit-title" name="title" placeholder="Search Workshops &amp; Events" data-role="tagsinput">
											</div>
											<div class="form-group">
												<button type="submit" class="btn">Search</button>
											</div>

											<div class="form-group date">
												<button type="button" class="btn dates-popover"><i class="fa fa-calendar-o" aria-hidden="true"></i> All dates</button>
												<div class="allDates hidden">
													<div id="edit-field-workshop-date-value-min-wrapper">
														<div id="edit-field-workshop-date-value-min-inside-wrapper">
															<div class="container-inline-date">
																<div class="form-item form-item-field-workshop-date-value-min form-type-date-popup form-group"> <label class="control-label" for="edit-field-workshop-date-value-min">Start date</label>
																	<div id="edit-field-workshop-date-value-min" class="date-padding clearfix">
																		<div class="form-item form-item-field-workshop-date-value-min-date form-type-textfield form-group"><input class="bef-datepicker form-control form-text" title="" data-toggle="tooltip" type="text" id="edit-field-workshop-date-value-min-datepicker-popup-0" name="field_workshop_date_value[min][date]" value="" size="20" maxlength="30" data-original-title=" E.g., 03/27/2017">
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div id="edit-field-workshop-date-value-max-wrapper">
														<div id="edit-field-workshop-date-value-max-inside-wrapper">
															<div class="container-inline-date">
																<div class="form-item form-item-field-workshop-date-value-max form-type-date-popup form-group"> <label class="control-label" for="edit-field-workshop-date-value-max">End date</label>
																	<div id="edit-field-workshop-date-value-max" class="date-padding clearfix">
																		<div class="form-item form-item-field-workshop-date-value-max-date form-type-textfield form-group"><input class="bef-datepicker form-control form-text" title="" data-toggle="tooltip" type="text" id="edit-field-workshop-date-value-max-datepicker-popup-0" name="field_workshop_date_value[max][date]" value="" size="20" maxlength="30" data-original-title=" E.g., 03/27/2017">
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>


											<div class="form-group">
												<div class="form-item form-item-field-workshop-campus-nid form-type-select form-group"><select class="form-control form-select" id="edit-field-workshop-campus-nid" name="field_workshop_campus_nid"><option value="All" selected="selected">- Any -</option><option value="5021">Costa Rica</option><option value="5198">Mount Madonna, CA</option><option value="5610">New York, NY</option><option value="5020">Rhinebeck, NY</option><option value="5022">Sheraton Hotel, NYC</option><option value="4691">Washington D.C.</option></select></div>
											</div>

											<div class="hidden">
												<div class="form-item form-item-field-learning-paths-tid form-type-textfield form-autocomplete form-group" role="application">
													<div class="input-group"><input class="form-control form-text" type="text" id="edit-field-learning-paths-tid" name="field_learning_paths_tid" value="" size="60" maxlength="128" autocomplete="OFF" aria-autocomplete="list"><span class="input-group-addon"><span class="icon glyphicon glyphicon-refresh" aria-hidden="true"></span></span>
													</div><input type="hidden" id="edit-field-learning-paths-tid-autocomplete" value="http://test2.eomega.org/index.php?q=admin/views/ajax/autocomplete/taxonomy/18" disabled="disabled" class="autocomplete autocomplete-processed"><span class="element-invisible" aria-live="assertive" id="edit-field-learning-paths-tid-autocomplete-aria-live"></span><span class="element-invisible" aria-live="assertive" id="edit-field-learning-paths-tid-autocomplete-aria-live"></span></div>
											</div>

											<div class="form-group">
												<button type="button" class="btn filter-btn" data-toggle="modal" data-target="#filterModal-workshops-events"><i class="fa fa-filter" aria-hidden="true"></i> Filter By Topics
                </button>
											</div>
											<script>
												jQuery(document).ready(function($) {
													$('.form-workshops-events #edit-title').focusout(function() {
														$('.form-workshops-events #edit-meta-keywords-metatags-quick').val($('.form-workshops-events #edit-title').val());
													});
												});

											</script>
										</div>
									</div>

									<div class="views-exposed-widgets clearfix">
									</div>
									<!-- Modal -->
									<div class="modal fade filterModal" id="filterModal-workshops-events" tabindex="-1" role="dialog" aria-labelledby="filterByTopics">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
													<h4 class="modal-title" id="filterByTopics">Filter By Topics</h4>
												</div>
												<div class="modal-body text-left">
													<div class="panel-group" id="filterOptionsAccordion" role="tablist" aria-multiselectable="true">
														<div class="panel panel-default">
															<div class="panel-heading" role="tab" id="parent-body-mind-spirit1">
																<h4 class="panel-title">
																	<a class="collapsed" role="button" data-toggle="collapse" data-parent="#filterOptionsAccordion" href="#body-mind-spirit1" aria-expanded="false" aria-controls="body-mind-spirit1">
                                            Body Mind &amp; Spirit                                        </a>
																</h4>
															</div>
															<div id="body-mind-spirit1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="parent-body-mind-spirit1">
																<div class="panel-body">
																	<ul class="suboptions-list list-unstyled">
																		<li>
																			<a href="#!" class="suboption all">All</a>
																		</li>
																	</ul>
																	<ul class="suboptions-list list-unstyled">
																		<li><a href="#!" class="suboption">Intuition</a></li>
																		<li><a href="#!" class="suboption">Meditation</a></li>
																		<li><a href="#!" class="suboption">Mind &amp; Psychology</a></li>
																		<li><a href="#!" class="suboption">Mindfulness</a></li>
																		<li><a href="#!" class="suboption">Personal Growth</a></li>
																		<li><a href="#!" class="suboption">Qigong &amp; Tai Chi</a></li>
																		<li><a href="#!" class="suboption">Spirituality</a></li>
																		<li><a href="#!" class="suboption">Yoga</a></li>
																	</ul>
																</div>
															</div>
														</div>
														<div class="panel panel-default">
															<div class="panel-heading" role="tab" id="parent-health-healing1">
																<h4 class="panel-title">
																	<a class="collapsed" role="button" data-toggle="collapse" data-parent="#filterOptionsAccordion" href="#health-healing1" aria-expanded="false" aria-controls="health-healing1">
                                            Health &amp; Healing                                        </a>
																</h4>
															</div>
															<div id="health-healing1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="parent-health-healing1">
																<div class="panel-body">
																	<ul class="suboptions-list list-unstyled">
																		<li>
																			<a href="#!" class="suboption all">All</a>
																		</li>
																	</ul>
																	<ul class="suboptions-list list-unstyled">
																		<li><a href="#!" class="suboption">Body Work</a></li>
																		<li><a href="#!" class="suboption">Energy Healing</a></li>
																		<li><a href="#!" class="suboption">Fitness</a></li>
																		<li><a href="#!" class="suboption">Holistic Health</a></li>
																		<li><a href="#!" class="suboption">Nutrition</a></li>
																	</ul>
																</div>
															</div>
														</div>
														<div class="panel panel-default">
															<div class="panel-heading" role="tab" id="parent-creative-expression1">
																<h4 class="panel-title">
																	<a class="collapsed" role="button" data-toggle="collapse" data-parent="#filterOptionsAccordion" href="#creative-expression1" aria-expanded="false" aria-controls="creative-expression1">
                                            Creative Expression                                        </a>
																</h4>
															</div>
															<div id="creative-expression1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="parent-creative-expression1">
																<div class="panel-body">
																	<ul class="suboptions-list list-unstyled">
																		<li>
																			<a href="#!" class="suboption all">All</a>
																		</li>
																	</ul>
																	<ul class="suboptions-list list-unstyled">
																		<li><a href="#!" class="suboption">Cooking</a></li>
																		<li><a href="#!" class="suboption">Creativity</a></li>
																		<li><a href="#!" class="suboption">Dance &amp; Movement</a></li>
																		<li><a href="#!" class="suboption">Literary Arts</a></li>
																		<li><a href="#!" class="suboption">Music &amp; Voice</a></li>
																		<li><a href="#!" class="suboption">Theatrical Arts</a></li>
																		<li><a href="#!" class="suboption">Visual Arts</a></li>
																	</ul>
																</div>
															</div>
														</div>
														<div class="panel panel-default">
															<div class="panel-heading" role="tab" id="parent-relationships-family1">
																<h4 class="panel-title">
																	<a class="collapsed" role="button" data-toggle="collapse" data-parent="#filterOptionsAccordion" href="#relationships-family1" aria-expanded="false" aria-controls="relationships-family1">
                                            Relationships &amp; Family                                        </a>
																</h4>
															</div>
															<div id="relationships-family1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="parent-relationships-family1">
																<div class="panel-body">
																	<ul class="suboptions-list list-unstyled">
																		<li>
																			<a href="#!" class="suboption all">All</a>
																		</li>
																	</ul>
																	<ul class="suboptions-list list-unstyled">
																		<li><a href="#!" class="suboption">Community</a></li>
																		<li><a href="#!" class="suboption">Family</a></li>
																		<li><a href="#!" class="suboption">Relationships</a></li>
																	</ul>
																</div>
															</div>
														</div>
														<div class="panel panel-default">
															<div class="panel-heading" role="tab" id="parent-leadership-work1">
																<h4 class="panel-title">
																	<a class="collapsed" role="button" data-toggle="collapse" data-parent="#filterOptionsAccordion" href="#leadership-work1" aria-expanded="false" aria-controls="leadership-work1">
                                            Leadership &amp; Work                                        </a>
																</h4>
															</div>
															<div id="leadership-work1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="parent-leadership-work1">
																<div class="panel-body">
																	<ul class="suboptions-list list-unstyled">
																		<li>
																			<a href="#!" class="suboption all">All</a>
																		</li>
																	</ul>
																	<ul class="suboptions-list list-unstyled">
																		<li><a href="#!" class="suboption">Leadership Training</a></li>
																		<li><a href="#!" class="suboption">Money</a></li>
																		<li><a href="#!" class="suboption">Skills</a></li>
																		<li><a href="#!" class="suboption">Social Change</a></li>
																		<li><a href="#!" class="suboption">Work &amp; Career</a></li>
																	</ul>
																</div>
															</div>
														</div>
														<div class="panel panel-default">
															<div class="panel-heading" role="tab" id="parent-sustainable-living1">
																<h4 class="panel-title">
																	<a class="collapsed" role="button" data-toggle="collapse" data-parent="#filterOptionsAccordion" href="#sustainable-living1" aria-expanded="false" aria-controls="sustainable-living1">
                                            Sustainable Living                                        </a>
																</h4>
															</div>
															<div id="sustainable-living1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="parent-sustainable-living1">
																<div class="panel-body">
																	<ul class="suboptions-list list-unstyled">
																		<li>
																			<a href="#!" class="suboption all">All</a>
																		</li>
																	</ul>
																	<ul class="suboptions-list list-unstyled">
																		<li><a href="#!" class="suboption">Design</a></li>
																		<li><a href="#!" class="suboption">Energy</a></li>
																		<li><a href="#!" class="suboption">Food</a></li>
																		<li><a href="#!" class="suboption">Living</a></li>
																		<li><a href="#!" class="suboption">Planet</a></li>
																		<li><a href="#!" class="suboption">Water</a></li>
																	</ul>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn">Submit</button>
													<button type="button" class="btn reset-btn">Reset</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="views-exposed-widgets clearfix">
									<div class="form-item form-item-sort-bef-combine form-type-select form-group"> <label class="control-label" for="edit-sort-bef-combine">Sort by</label>
										<select class="form-control form-select" id="edit-sort-bef-combine" name="sort_bef_combine"><option value="created ASC">Oldest</option><option value="created DESC">Most Recent</option><option value="title ASC" selected="selected">A/Z</option><option value="title DESC">Z/A</option></select></div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!--tab contents-->
			<!--fiecare tabpanel de mai jos are un ID. te rog sa le referentiezi bine cu nav-tabs de mai sus, sa fei la fel la href si la aria controls-->
			<div class="row">
				<div class="col-sm-12">
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="owlcEverything">
							<div class="filter-results text-center interractive-text">
								<h2 class="interactive-text">Body Mind &amp; Spirit/ Yoga</h2>
								<p class="mb20">See all the ways we are cultivating a more compassionate, connected, balanced world.</p>

								<div class="tags-carousel-wrapper">
									<!-- Carousel -->
									<div class="jcarousel tags-carousel" data-jcarousel="true">
										<ul>
											<li><a href="#!">abc</a></li>
											<li><a href="#!">Yoga</a></li>
											<li><a href="#!">Omega</a></li>
											<li><a href="#!">Tag</a></li>
											<li><a href="#!">Body Mind </a></li>
											<li><a href="#!">Spirit</a></li>
											<li><a href="#!">Body Spirit</a></li>
											<li><a href="#!">Mind</a></li>
											<li><a href="#!">Spirit</a></li>
											<li><a href="#!">Body Mind &amp; Spirit</a></li>
											<li><a href="#!">Yoga</a></li>
											<li><a href="#!">Omega</a></li>
											<li><a href="#!">Tag</a></li>
											<li><a href="#!">Body Mind </a></li>
											<li><a href="#!">Spirit</a></li>
											<li><a href="#!">Body Spirit</a></li>
											<li><a href="#!">Mind</a></li>
											<li><a href="#!">Spirit</a></li>
										</ul>

									</div>
									<!-- Prev/next controls -->
									<a href="#" class="tags-carousel-control-prev" data-jcarouselcontrol="true"><i class="fa fa-angle-left"></i></a>
									<a href="#" class="tags-carousel-control-next inactive" data-jcarouselcontrol="true"><i class="fa fa-angle-right"></i></a>
								</div>
								<script>
									//	======================================
									//	mobile set tags-carousel width 
									//	======================================
									setTimeout(function() {
										if (windowWidth < 768) {
											$(".tags-carousel").each(function(index) {
												var totalWidth = 0;
												$(this).find("li").each(function() {
													totalWidth += Math.ceil($(this).outerWidth(true));
												});
												$(this).find("ul").width(totalWidth);
											});
										}
									}, 500);

								</script>

								<!--clear filters button-->
								<button class="btn clear-filters mt20">Clear filters <i class="fa fa-times"></i></button>

							</div>
							<div class="tiles-grid">
								<div class="tiles-row row">
									<div class="col-sm-12 headline-col">
										<div class="row">
											<div class="col-sm-12 col-md-6">
												<h2 class="headline-title">Featured</h2>
											</div>
											<div class="col-sm-12 col-md-6">
												<a href="#" class="btn see-all pull-right">See all</a>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="row">
											<div class="col-sm-4">
												<div class="panel panel-default panel-idea tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite2">
															<label for="favourite2"></label>
														</div>
													</div>
													<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
													</div>
													<div class="panel-body panel-audio-video">
														<div class="content-type">
															<a href="#"><i class="fa fa-play-circle-o"></i></a>
															<span>Video</span>
														</div>
														<div class="title">
															<h1>Bobby McFerrin: Circle Songs from Omega</h1>
														</div>
														<div class="person">
															<ul class="by list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
															<ul class="featuring list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="duration-date">
															<em class="duration">13 minute watch</em>
															<em class="date-release">13th of September, 2016</em>
														</div>
													</div>
													<div class="panel-footer">
														<a href="#" class="expand-button">
															<i class="fa fa-plus"></i>
														</a>
														<div class="expanded-tile-content animated fadeInUp">
															<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
															<a href="#" class="btn white-transparent-button">Learn More</a>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="panel panel-default panel-collection tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite10">
															<label for="favourite10"></label>
														</div>
													</div>
													<div class="panel-body">
														<div class="content-type">
															<span>Playlist</span>
														</div>
														<div class="title">
															<h1>Pema Chodron's Words of Wisdom</h1>
														</div>
														<div class="image-tiles">
															<ul class="list-inline">
																<li style="background:url('images/workshop_events.jpg');"></li>
																<li style="background:url('images/person.jpg');"></li>
																<li style="background:url('images/online_learning.jpg');"></li>
																<li style="background:url('images/idea.jpg');"></li>
															</ul>
														</div>
														<div class="person">
															<ul class="list-unstyled list-inline person-names">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
													</div>
													<div class="panel-footer">
														<a href="#" class="expand-button">
															<i class="fa fa-plus"></i>
														</a>
														<div class="expanded-tile-content animated fadeInUp">
															<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
															<a href="#" class="btn white-transparent-button">Learn More</a>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="panel panel-default panel-learn tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite7">
															<label for="favourite7"></label>
														</div>
													</div>
													<div class="panel-body">
														<div class="content-type">
															<span>E-course</span>
														</div>
														<div class="title">
															<h1>Growing &amp; Sustaining A Yoga Service Organization</h1>
														</div>
														<div class="person">

														</div>
													</div>
													<div class="panel-footer">
														<a href="#" class="expand-button">
															<i class="fa fa-plus"></i>
														</a>
														<div class="expanded-tile-content animated fadeInUp">
															<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
															<a href="#" class="btn white-transparent-button">Learn More</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tiles-row row">
									<div class="col-sm-12 headline-col">
										<div class="row">
											<div class="col-sm-12 col-md-6">
												<h2 class="headline-title">Workshops &amp; Events</h2>
											</div>
											<div class="col-sm-12 col-md-6">
												<a href="#" class="btn see-all pull-right">See all</a>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="row">
											<div class="col-sm-4">
												<div class="panel panel-default panel-workshop tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite4">
															<label for="favourite4"></label>
														</div>
													</div>
													<div class="panel-body">
														<div class="content-type">
															<span>Workshop</span>
														</div>
														<div class="title">
															<h1>Collaborating With Nature</h1>
														</div>
														<div class="person">
															<ul class="list-unstyled list-inline person-images">
																<li>
																	<img src="images/person.jpg">
																</li>
																<li> <img src="images/person.jpg"></li>
															</ul>
															<ul class="list-unstyled list-inline person-names">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="date-location">
															<ul class="list-inline">
																<li><em>Feb 28 - Mar 06 2016</em></li>
																<li><em>Rhinebeck, NY</em></li>
															</ul>
														</div>
													</div>
													<div class="panel-footer">
														<a href="#" class="expand-button">
															<i class="fa fa-plus"></i>
														</a>
														<div class="expanded-tile-content animated fadeInUp">
															<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
															<a href="#" class="btn white-transparent-button">Learn More</a>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="panel panel-default panel-workshop tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite5">
															<label for="favourite5"></label>
														</div>
													</div>
													<div class="panel-body">
														<div class="content-type">
															<span>Workshop</span>
														</div>
														<div class="title">
															<h1>Collaborating With Nature</h1>
														</div>
														<div class="person">
															<ul class="list-unstyled list-inline person-images">
																<li>
																	<img src="images/person.jpg">
																</li>
																<li> <img src="images/person.jpg"></li>
															</ul>
															<ul class="list-unstyled list-inline person-names">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="date-location">
															<ul class="list-inline">
																<li><em>Feb 28 - Mar 06 2016</em></li>
																<li><em>Rhinebeck, NY</em></li>
															</ul>
														</div>
													</div>
													<div class="panel-footer">
														<a href="#" class="expand-button">
															<i class="fa fa-plus"></i>
														</a>
														<div class="expanded-tile-content animated fadeInUp">
															<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
															<a href="#" class="btn white-transparent-button">Learn More</a>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="panel panel-default panel-workshop tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite6">
															<label for="favourite6"></label>
														</div>
													</div>
													<div class="panel-body">
														<div class="content-type">
															<span>Workshop</span>
														</div>
														<div class="title">
															<h1>Collaborating With Nature</h1>
														</div>
														<div class="person">
															<ul class="list-unstyled list-inline person-images">
																<li>
																	<img src="images/person.jpg">
																</li>
																<li> <img src="images/person.jpg"></li>
															</ul>
															<ul class="list-unstyled list-inline person-names">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="date-location">
															<ul class="list-inline">
																<li><em>Feb 28 - Mar 06 2016</em></li>
																<li><em>Rhinebeck, NY</em></li>
															</ul>
														</div>
													</div>
													<div class="panel-footer">
														<a href="#" class="expand-button">
															<i class="fa fa-plus"></i>
														</a>
														<div class="expanded-tile-content animated fadeInUp">
															<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
															<a href="#" class="btn white-transparent-button">Learn More</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tiles-row row">
									<div class="col-sm-12 headline-col">
										<div class="row">
											<div class="col-sm-12 col-md-6">
												<h2 class="headline-title">Ideas</h2>
											</div>
											<div class="col-sm-12 col-md-6">
												<a href="#" class="btn see-all pull-right">See all</a>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="row">
											<div class="col-sm-4">
												<div class="panel panel-default panel-idea tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite1">
															<label for="favourite1"></label>
														</div>
													</div>
													<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
													</div>
													<div class="panel-body panel-article">
														<div class="content-type">
															<span>Article</span>
														</div>
														<div class="title">
															<h1>Bobby McFerrin: Circle Songs from Omega</h1>
														</div>
														<div class="person">
															<ul class="by list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
															<span class="featuring">Featuring:</span>
															<ul class="featuring list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="duration-date">
															<em class="duration">13 minute watch</em>
															<em class="date-release">13th of September, 2016</em>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="panel panel-default panel-idea  tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite2">
															<label for="favourite2"></label>
														</div>
													</div>
													<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
													</div>
													<div class="panel-body panel-audio-video">
														<div class="content-type">
															<a href="#"><i class="fa fa-play-circle-o"></i></a>
															<span>Video</span>
														</div>
														<div class="title">
															<h1>Bobby McFerrin: Circle Songs from Omega</h1>
														</div>
														<div class="person">
															<ul class="by list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
															<ul class="featuring list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="duration-date">
															<em class="duration">13 minute watch</em>
															<em class="date-release">13th of September, 2016</em>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="panel panel-default panel-idea tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite3">
															<label for="favourite3"></label>
														</div>
													</div>
													<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
													</div>
													<div class="panel-body panel-press">
														<div class="content-type">
															<span>Press Release</span>
														</div>
														<div class="title">
															<h1>Bobby McFerrin: Circle Songs from Omega</h1>
														</div>
														<div class="person">
															<ul class="by list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
															<span class="featuring">Featuring:</span>
															<ul class="featuring list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="duration-date">
															<em class="duration">13 minute watch</em>
															<em class="date-release">13<sup>th</sup> of September, 2016</em>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tiles-row row">
									<div class="col-sm-12 headline-col">
										<div class="row">
											<div class="col-sm-12 col-md-6">
												<h2 class="headline-title">People</h2>
											</div>
											<div class="col-sm-12 col-md-6">
												<a href="#" class="btn see-all pull-right">See all</a>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="row">
											<div class="col-sm-6">
												<div class="panel panel-default tile  panel-person">
													<div class="panel-body clearfix">
														<div class="person-image">
															<img src="images/person.jpg">
														</div>
														<div class="person-bio">
															<span class="block field-text person-ocupation">Teacher</span>
															<span class="block field-text person-name">Armando Tolken</span>
															<span class="block field-text person-info">Armando is a great guy and knows so much about life, happiness, and where to get an amazing lobster roll in Santa Fe, he also …</span>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="panel panel-default tile  panel-person">
													<div class="panel-body clearfix">
														<div class="person-image">
															<img src="images/person.jpg">
														</div>
														<div class="person-bio">
															<span class="block field-text person-ocupation">Teacher</span>
															<span class="block field-text person-name">Armando Tolken</span>
															<span class="block field-text person-info">Armando is a great guy and knows so much about life, happiness, and where to get an amazing lobster roll in Santa Fe, he also …</span>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="panel panel-default tile  panel-person">
													<div class="panel-body clearfix">
														<div class="person-image">
															<img src="images/person.jpg">
														</div>
														<div class="person-bio">
															<span class="block field-text person-ocupation">Teacher</span>
															<span class="block field-text person-name">Armando Tolken</span>
															<span class="block field-text person-info">Armando is a great guy and knows so much about life, happiness, and where to get an amazing lobster roll in Santa Fe, he also …</span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="owlcVideo">

							<div class="tiles-grid">
								<div class="tiles-row row">
									<div class="col-sm-12 headline-col">
										<div class="row">
											<div class="col-sm-12 col-md-6">
												<h2 class="headline-title">Workshops &amp; Events</h2>
											</div>
											<div class="col-sm-12 col-md-6">
												<a href="#" class="btn see-all pull-right">See all</a>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="row">
											<div class="col-sm-4">
												<div class="panel panel-default panel-workshop tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite4">
															<label for="favourite4"></label>
														</div>
													</div>
													<div class="panel-body">
														<div class="content-type">
															<span>Workshop</span>
														</div>
														<div class="title">
															<h1>Collaborating With Nature</h1>
														</div>
														<div class="person">
															<ul class="list-unstyled list-inline person-images">
																<li>
																	<img src="images/person.jpg">
																</li>
																<li> <img src="images/person.jpg"></li>
															</ul>
															<ul class="list-unstyled list-inline person-names">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="date-location">
															<ul class="list-inline">
																<li><em>Feb 28 - Mar 06 2016</em></li>
																<li><em>Rhinebeck, NY</em></li>
															</ul>
														</div>
													</div>
													<div class="panel-footer">
														<a href="#" class="expand-button">
															<i class="fa fa-plus"></i>
														</a>
														<div class="expanded-tile-content animated fadeInUp">
															<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
															<a href="#" class="btn white-transparent-button">Learn More</a>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="panel panel-default panel-workshop tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite5">
															<label for="favourite5"></label>
														</div>
													</div>
													<div class="panel-body">
														<div class="content-type">
															<span>Workshop</span>
														</div>
														<div class="title">
															<h1>Collaborating With Nature</h1>
														</div>
														<div class="person">
															<ul class="list-unstyled list-inline person-images">
																<li>
																	<img src="images/person.jpg">
																</li>
																<li> <img src="images/person.jpg"></li>
															</ul>
															<ul class="list-unstyled list-inline person-names">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="date-location">
															<ul class="list-inline">
																<li><em>Feb 28 - Mar 06 2016</em></li>
																<li><em>Rhinebeck, NY</em></li>
															</ul>
														</div>
													</div>
													<div class="panel-footer">
														<a href="#" class="expand-button">
															<i class="fa fa-plus"></i>
														</a>
														<div class="expanded-tile-content animated fadeInUp">
															<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
															<a href="#" class="btn white-transparent-button">Learn More</a>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="panel panel-default panel-workshop tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite6">
															<label for="favourite6"></label>
														</div>
													</div>
													<div class="panel-body">
														<div class="content-type">
															<span>Workshop</span>
														</div>
														<div class="title">
															<h1>Collaborating With Nature</h1>
														</div>
														<div class="person">
															<ul class="list-unstyled list-inline person-images">
																<li>
																	<img src="images/person.jpg">
																</li>
																<li> <img src="images/person.jpg"></li>
															</ul>
															<ul class="list-unstyled list-inline person-names">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="date-location">
															<ul class="list-inline">
																<li><em>Feb 28 - Mar 06 2016</em></li>
																<li><em>Rhinebeck, NY</em></li>
															</ul>
														</div>
													</div>
													<div class="panel-footer">
														<a href="#" class="expand-button">
															<i class="fa fa-plus"></i>
														</a>
														<div class="expanded-tile-content animated fadeInUp">
															<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
															<a href="#" class="btn white-transparent-button">Learn More</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tiles-row row">
									<div class="col-sm-12 headline-col">
										<div class="row">
											<div class="col-sm-12 col-md-6">
												<h2 class="headline-title">Ideas</h2>
											</div>
											<div class="col-sm-12 col-md-6">
												<a href="#" class="btn see-all pull-right">See all</a>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="row">
											<div class="col-sm-4">
												<div class="panel panel-default panel-idea tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite1">
															<label for="favourite1"></label>
														</div>
													</div>
													<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
													</div>
													<div class="panel-body panel-article">
														<div class="content-type">
															<span>Article</span>
														</div>
														<div class="title">
															<h1>Bobby McFerrin: Circle Songs from Omega</h1>
														</div>
														<div class="person">
															<ul class="by list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
															<span class="featuring">Featuring:</span>
															<ul class="featuring list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="duration-date">
															<em class="duration">13 minute watch</em>
															<em class="date-release">13th of September, 2016</em>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="panel panel-default panel-idea  tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite2">
															<label for="favourite2"></label>
														</div>
													</div>
													<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
													</div>
													<div class="panel-body panel-audio-video">
														<div class="content-type">
															<a href="#"><i class="fa fa-play-circle-o"></i></a>
															<span>Video</span>
														</div>
														<div class="title">
															<h1>Bobby McFerrin: Circle Songs from Omega</h1>
														</div>
														<div class="person">
															<ul class="by list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
															<ul class="featuring list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="duration-date">
															<em class="duration">13 minute watch</em>
															<em class="date-release">13th of September, 2016</em>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="panel panel-default panel-idea tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite3">
															<label for="favourite3"></label>
														</div>
													</div>
													<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
													</div>
													<div class="panel-body panel-press">
														<div class="content-type">
															<span>Press Release</span>
														</div>
														<div class="title">
															<h1>Bobby McFerrin: Circle Songs from Omega</h1>
														</div>
														<div class="person">
															<ul class="by list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
															<span class="featuring">Featuring:</span>
															<ul class="featuring list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="duration-date">
															<em class="duration">13 minute watch</em>
															<em class="date-release">13<sup>th</sup> of September, 2016</em>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="owlcAction">
							<div class="tiles-grid">
								<div class="tiles-row row">
									<div class="col-sm-12 headline-col">
										<div class="row">
											<div class="col-sm-12 col-md-6">
												<h2 class="headline-title">Ideas</h2>
											</div>
											<div class="col-sm-12 col-md-6">
												<a href="#" class="btn see-all pull-right">See all</a>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="row">
											<div class="col-sm-4">
												<div class="panel panel-default panel-idea tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite1">
															<label for="favourite1"></label>
														</div>
													</div>
													<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
													</div>
													<div class="panel-body panel-article">
														<div class="content-type">
															<span>Article</span>
														</div>
														<div class="title">
															<h1>Bobby McFerrin: Circle Songs from Omega</h1>
														</div>
														<div class="person">
															<ul class="by list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
															<span class="featuring">Featuring:</span>
															<ul class="featuring list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="duration-date">
															<em class="duration">13 minute watch</em>
															<em class="date-release">13th of September, 2016</em>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="panel panel-default panel-idea  tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite2">
															<label for="favourite2"></label>
														</div>
													</div>
													<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
													</div>
													<div class="panel-body panel-audio-video">
														<div class="content-type">
															<a href="#"><i class="fa fa-play-circle-o"></i></a>
															<span>Video</span>
														</div>
														<div class="title">
															<h1>Bobby McFerrin: Circle Songs from Omega</h1>
														</div>
														<div class="person">
															<ul class="by list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
															<ul class="featuring list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="duration-date">
															<em class="duration">13 minute watch</em>
															<em class="date-release">13th of September, 2016</em>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="panel panel-default panel-idea tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite3">
															<label for="favourite3"></label>
														</div>
													</div>
													<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
													</div>
													<div class="panel-body panel-press">
														<div class="content-type">
															<span>Press Release</span>
														</div>
														<div class="title">
															<h1>Bobby McFerrin: Circle Songs from Omega</h1>
														</div>
														<div class="person">
															<ul class="by list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
															<span class="featuring">Featuring:</span>
															<ul class="featuring list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="duration-date">
															<em class="duration">13 minute watch</em>
															<em class="date-release">13<sup>th</sup> of September, 2016</em>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->

		<!--OUR SPONSORS-->
		<div class="sponsors">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h1 class="section-title-big interractive-text text-center bottom-bar-orange">OWLC Sponsors</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 text-center">
						<ul class="sponsor-list list-inline">
							<li>
								<a href="#" target="_blank"><img src="images/sponsor1.png" alt="Sponsor 1"></a>
							</li>
							<li>
								<a href="#" target="_blank"><img src="images/sponsor2.png"></a>
							</li>
							<li>
								<a href="#" target="_blank"><img src="images/sponsor3.png"></a>
							</li>
							<li>
								<a href="#" target="_blank"><img src="images/sponsor4.png"></a>
							</li>
							<li>
								<a href="#" target="_blank"><img src="images/sponsor5.png"></a>
							</li>
							<li>
								<a href="#" target="_blank"><img src="images/sponsor6.png"></a>
							</li>
							<li>
								<a href="#" target="_blank"><img src="images/sponsor7.png"></a>
							</li>
							<li>
								<a href="#" target="_blank"><img src="images/sponsor8.png"></a>
							</li>
							<li>
								<a href="#" target="_blank"><img src="images/sponsor9.png"></a>
							</li>
							<li>
								<a href="#" target="_blank"><img src="images/sponsor10.png"></a>
							</li>
						</ul>
						<a href="#" class='interractive-text text-center see-more'>+ See more</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include ("templates/footer.php"); ?>
