<?php include ("templates/header.php"); ?>


<div class="main-container">

	<a id="main-content"></a>

	<div class="container">
		<div class="row">
			<div class="col-xs-12">
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
			</div>
		</div>
	</div>
	<div class="region region-content">
		<section id="block-system-main" class="block block-system clearfix">
			<link rel="shortcut icon" href="/events/images/favicon.ico" type="image/vnd.microsoft.icon">
			<style type="text/css" media="all">
				@import url("css/events.css");

			</style>

			<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>

			<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/headjs/0.99/head.min.js"></script>
			<script type="text/javascript">
				var switchTo5x = true;
				var url_and_token = '//test2.eomega.org/eventss?token=524db83f5c8dba';

				head.js("/events/scripts/jquery.fittext.min.js", "/events/scripts/events.js?1202", "//ws.sharethis.com/button/buttons.js", function() {
					stLight.options({
						publisher: "31c42fc2-5fa2-44f4-99a7-ffef670659b2"
					});
				});

			</script>
			<!--[if lt IE 9]>
<style type="text/css" media="all">@import url("/events/css/ie.css?1202"); </style>
<![endif]-->
			<!--[if lt IE 8]>
<script type="text/javascript">
    (function ($) {
        $(document).ready(function () {
            $('#live-event-video').html('<div id="ie-message"><h2>Your browser is currently not supported</h2>' +
                '<p><strong>Please upgrade to the latest version of Internet Explorer or download an alternate browser (Chrome, Firefox).</strong></p></div>');
        });
    })(jQuery);
</script>
<![endif]-->
			<!-- GTM noscript -->
			<noscript>
    &lt;iframe src="//www.googletagmanager.com/ns.html?id=GTM-5PCPWH"
            height="0" width="0" style="display:none;visibility:hidden"&gt;&lt;/iframe&gt;
</noscript>
			<!-- End GTM noscript -->
			<div class="container">
				<div id="eventsHeader">
					<span class="corner-status">&nbsp;</span>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div id="navigation">
								<div class="content">
									<ul class="list-inline">
										<li class="first"><a href="/" title="Home">Home</a></li>
										<li><a href="#live-event-comments" title="Comment">Comment</a></li>
										<li><a href="//www.eomega.org/online-learning/FAQ" title="FAQ" target="_blank">FAQ</a></li>
										<li class="last"><a href="#live-event-schedule" title="Schedule">Schedule</a></li>
										<li id="logout" class="last">

										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div id="sharethis">
								<span class='st_sharethis_large'></span>
								<span class='st_facebook_large'></span>
								<span class='st_twitter_large'></span>
								<span class='st_linkedin_large'></span>
								<span class='st_email_large'></span>
							</div>
						</div>
					</div>
				</div>
				<div id="main" class="row">
					<div class="col-xs-12">


						<div id="live-event-info">
							<h2>Find Your Own Strength</h2>
							<h3></h3>

							<p>
								Thank you for watching Omega's video on demand. Please select the video below to watch Find Your Own Strength. If you are watching on a mobile or tablet, please select the "mobile and tablet" version in the menu. You can use the scroll bar on the bottom of the video to fast forward or rewind.
							</p>

							<p>
								<span style="line-height: 1.538em;">If you have further questions, please watch our&nbsp;<a href="http://www.eomega.org/videos/omega-live-stream-tutorial-how-to-watch-video-on-demand">video on demand tutorial</a>,&nbsp;</span>
							</p>

							<p>
								<em style="line-height: 1.538em;"><span style="line-height: 1.538em;">Recommended browsers include Google Chrome, Firefox, Safari, and Internet Explorer 8 and higher.&nbsp;</span></em>
							</p>
						</div>

						<div id="live-event-video">
							<div id="containing-block-0" class="containing-block" style="display: block;">
								<div class="video">
									<div class="video_title" data-title="Find Your Own Strength - Mobile and Tablet"></div>
									<div class="video_skip" data-skip="738"></div>
									<div class="video_bc_id" data-video-bc-id="2683333643001" src="https://players.brightcove.net/1967227982001/default_default/index.html?videoId=2683333643001">
										<!-- Start of Brightcove Player -->
										<div style="display:none"></div>
										<!-- By use of this code snippet, I agree to the Brightcove Publisher T and C found at https://accounts.brightcove.com/en/terms-and-conditions/. --><object type="application/x-shockwave-flash" id="2683333643001" width="480" height="270" class="BrightcoveExperience BrightcoveExperienceID_5813" seamlesstabbing="undefined" data="https://secure.brightcove.com/services/viewer/federated_f9?&amp;width=480&amp;height=270&amp;flashID=2683333643001&amp;identifierClassName=BrightcoveExperienceID_5813&amp;bgcolor=%23FFFFFF&amp;playerID=1968932195001&amp;playerKey=AQ~~%2CAAABygfs0LE~%2CsL8r4sbCmjRsPadK2vlBiT8ptQLN-Jrf&amp;secureConnections=true&amp;secureHTMLConnections=true&amp;isVid=true&amp;isUI=true&amp;dynamicStreaming=true&amp;%40videoPlayer=2683333643001&amp;includeAPI=true&amp;templateLoadHandler=BCLS.onTemplateLoad&amp;templateReadyHandler=brightcove%5B%22templateReadyHandler2683333643001%22%5D&amp;templateErrorHandler=BCLS.onTemplateError&amp;showNoContentMessage=false&amp;autoStart=&amp;debuggerID=&amp;originalTemplateReadyHandler=BCLS.onTemplateReady&amp;startTime=1500972825177"><param name="allowScriptAccess" value="always"><param name="allowFullScreen" value="true"><param name="seamlessTabbing" value="false"><param name="swliveconnect" value="true"><param name="wmode" value="window"><param name="quality" value="high"><param name="bgcolor" value="#FFFFFF"></object>
										<!-- This script tag will cause the Brightcove Players defined above it to be created as soon as the line is read by the browser. If you wish to have the player instantiated only after the rest of the HTML is processed and the page load is complete, remove the line. -->
										<!-- End of Brightcove Player -->
										<script type="text/javascript" src="https://sadmin.brightcove.com/js/api/SmartPlayerAPI.js"></script>
									</div>
								</div>
							</div>


							<div id="video-message" class="video-placeholder" style="display: none;">
								<div id="video-message-inner" class="video-placeholder-inner">
									<h3>Please select a video.</h3>

									<p>Please select a video from the dropdown below.</p>
								</div>
							</div>

							<div id="video-controls">
								<span class="select">
                    <select id="video-select">
                        <option value="">Watch More Videos -</option>
                        <option value="0">Find Your Own Strength - Mobile and Tablet</option>                    </select>
                  </span>

								<div id="video-counter" class="">
									<span id="video-current">1</span> of 1 </div>
							</div>
						</div>

						<div id="sidebar">


						</div>

						<div id="live-event-comments">
							<h2>Join The Discussion</h2>
							<p>The discussion for this event has been closed.</p>
						</div>


						<div id="live-event-schedule">
							<h2>Event Schedule</h2>
							<table>
								<tbody>
									<tr>
										<td>
											8:00 p.m.-8:10 p.m.
										</td>
										<td>
											Omega slideshow (join us early and check your live stream)
										</td>
									</tr>
									<tr>
										<td>
											8:10 p.m.8:15 p.m.
										</td>
										<td>
											Carla Goldstein welcome (live stream program begins)
										</td>
									</tr>
									<tr>
										<td>
											8:15 p.m.-8:45 p.m.
										</td>
										<td>
											Elizabeth Lesser keynote
										</td>
									</tr>
									<tr>
										<td>
											8:45 p.m.-9:30 p.m.
										</td>
										<td>
											Elizabeth Lesser, Brené Brown, and Joan Roshi Halifax
										</td>
									</tr>
									<tr>
										<td>
											9:30 p.m.
										</td>
										<td>
											Live stream ends
										</td>
									</tr>
									<tr>
										<td colspan="2">
											* This live stream will be available on demand until 11pm Monday, September 23.
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="clearfix">
					</div>
				</div>
			</div>
		</section>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
			</div>
		</div>
	</div>

</div>


<?php include ("templates/footer.php"); ?>
