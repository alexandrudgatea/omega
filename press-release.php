<?php include ("templates/header.php"); ?>

<div id="mainContent" class="simple-page press-release">
		<div id="hero" class="simple-page background-image" style="background: url('/images/hero_owlc.jpg')"></div>
		<div id="pageContent">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 no-padding">
						<div class="page-body">
							<div class="page-header text-left">
								<a href="#!">Media Center</a>
								<p class="date">July 23, 2017</p>
								<h1>Omega Mindfulness &amp; Education Conference to Focus on Delivery &amp; Social Justice</h1>
								
							</div>
							<div class="page-text clearfix">
								
								<h2><strong>Omega is a place to explore the extraordinary potential that exists in all of us as individuals and together as a human family.</strong></h2>
								<p>
									Omega was founded on the holistic worldview that the well-being of each of us is deeply connected to the well-being of all living things. Since 1977, we have offered diverse and innovative educational experiences that inspire an integrated approach to personal and social change. Omega, a nonprofit organization, continues to be at the forefront of human development.
								</p>
								<p>
									Each year, more than 23,000 people attend workshops and educational programs delivered by hundreds of teachers, artists, healers, and thinkers on the leading edge of their disciplines. With special attention to our key initiatives in sustainability, women’s leadership, veterans care, and service, we bring awareness to issues that must be addressed in order for our society to heal and flourish.
								</p>
								<p>
									ACCOUNTABILITY We expect each of us to do what we say we will do, meet commitments, and be dependable and responsible.
								</p>
								<p>
									HOLISM We honor the mind, body, heart, and spirit in each individual, knowing the need to balance and blend all these elements. In our programming, we encourage authenticity as a means to build trust and as essential to the growth and development of the individual and the well-being of others and the world we share.
								</p>
								<p>
									INTEGRITY In business and in relationships, we conduct ourselves with honesty, fairness, truth, candor, and respect. We treat others as we ourselves would want to be treated. We focus on the collective good.
								</p>

								<p>
									SERVICE We value the practice of service and what it teaches us about ourselves, and our relation to others. We are attuned to and care about our participants’ needs and expectations. We treat each other with similar grace.
								</p>
								<p>
									SIMPLICITY We strive for clear, direct, and unambiguous communication.We seek true, underlying meaning, and employ spiritual guidance in that quest. In this way, we work to make sense of the complexities of life.
								</p>
								<p>
									SUSTAINABILITY We consider the impact of our actions. We advocate for fairness in the treatment of all species, make decisions for the common good, and encourage activism as a means to social justice. Our facilities are grounded in the awareness of our relationship to the environment. We endeavor to have our work in the world be regenerative and self-sustaining.
								</p>
								<p>
									TEAMWORK We work together, inclusively, collaboratively, with energy, intention, and commitment. We keep each other informed, share what we are thinking and doing, and expect the same in return.
								</p>
								<p>
									WELCOMING We invite people to find space here to feel safe, create community, feel at home, and find nourishment. Our environment is nurturing, relaxing, stimulating, and inspiring. are attuned to and care about our participants’ needs and expectations. We treat each other with similar grace.
								</p>
							</div>
							<div class="press-release-bottom-text">
								<h2><strong>About Omega Institute for Holistic Studies</strong></h2>
								<p>Founded in 1977, Omega Institute for Holistic Studies is the nation's most trusted source for wellness and personal growth. As a nonprofit organization, Omega otters diVerse and innovative educational experiences that inspire an integrated approach to personal and social change. Located on 250 acres in the beautiful Hudson Valley, Omega welcomes more than 23,000 people to its workshops, conferences, and retreats in Rhinebeck, New York, and at exceptional locations around the world. eOmega.org</p>
							</div>
							<div class="press-release-link teal">
								<a href="#!">mindfulnes_and_education_2016_press_release_forimmediaterelease.pdf</a>
							</div>
							<div class="press-release-static-text">
								<h4><strong>Press Contact</strong></h4>
								<ul class="list-unstyled">
									<li>Chrissa J. Pullicino, Manager of External Communications</li>
									<li>150 lake Drive Rhinebeck, NY 12572</li>
									<li><a href="tel: 845.266.4444">845.266.4444</a>, ext. 404</li>
									<li><a href="mailto:chrissap@eomega.org">chrissap@eomega.org</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php include ("templates/footer.php"); ?>

