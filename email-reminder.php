<?php include ("templates/header.php"); ?>
<div id="mainContent">
	<section id="block-system-main" class="block block-system clearfix">
		<form action="/online-workshops/email-reminder" method="post" id="omega-online-workshop-email-reminder" accept-charset="UTF-8">
			<div>
				<p>Have you lost the link you received to view an online workshop or event? You can use the form below to be re-sent email reminders for all active programs you have purchased.</p>
				<div class="form-item form-item-email form-type-textfield form-group">
					<label class="control-label" for="edit-email">Email Address <span class="form-required" title="This field is required.">*</span></label>
					<input class="form-control form-text required" title="" data-toggle="tooltip" type="text" id="edit-email" name="email" value="" size="60" maxlength="128" data-original-title="A reminder email will be sent to this email with details on how to access an online event.">
				</div>
				<button type="submit" id="edit-submit" name="op" value="Send Reminder Email" class="btn btn-default form-submit">Send Reminder Email</button>
				<input type="hidden" name="form_build_id" value="form-SWJocA_XFdkCCDH7BHxjBGXK3yC18RhDl8xd-NNJcj0">
				<input type="hidden" name="form_id" value="omega_online_workshop_email_reminder">
			</div>
		</form>
	</section>
</div>
<?php include ("templates/footer.php"); ?>
