<?php include ("templates/header.php"); ?>
<div id="mainContent">
	<div class="main-container container">

		<a id="main-content"></a>
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h2 class="element-invisible">Primary tabs</h2>
					<ul class="tabs--primary nav nav-tabs">
						<li class="active"><a href="/user/register" class="active">Create new account<span class="element-invisible">(active tab)</span></a></li>
						<li><a href="/user">Log in</a></li>
						<li><a href="/user/password">Request new password</a></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
				</div>
			</div>
		</div>
		<div class="region region-content">
			<section id="block-system-main" class="block block-system clearfix">


				<form class="user-info-from-cookie" enctype="multipart/form-data" action="/user/register" method="post" id="user-register-form" accept-charset="UTF-8">
					<div>
						<div class="container register">
							<div class="row">
								<div class="col-xs-12">
									<div id="edit-account" class="form-wrapper form-group">
										<div class="form-item form-item-name form-type-textfield form-group"> <label class="control-label" for="edit-name">Username <span class="form-required" title="This field is required.">*</span></label>
											<input class="username form-control form-text required" title="" data-toggle="tooltip" type="text" id="edit-name" name="name" value="" size="60" maxlength="60" data-original-title="Spaces are allowed; punctuation is not allowed except for periods, hyphens, apostrophes, and underscores."></div>
										<div class="form-item form-item-mail form-type-textfield form-group"> <label class="control-label" for="edit-mail">E-mail address <span class="form-required" title="This field is required.">*</span></label>
											<input class="form-control form-text required" title="" data-toggle="tooltip" type="text" id="edit-mail" name="mail" value="" size="60" maxlength="254" data-original-title="A valid e-mail address. All e-mails from the system will be sent to this address. The e-mail address is not made public and will only be used if you wish to receive a new password or wish to receive certain news or notifications by e-mail."></div>
										<div class="form-item form-item-pass form-type-password-confirm form-group">
											<div class="row">
												<div class="form-item form-item-pass-pass1 form-type-password form-group col-sm-6 col-md-4 has-feedback"> <label class="control-label" for="edit-pass-pass1">Password <span class="form-required" title="This field is required.">*</span><div class="label" aria-live="assertive"></div></label>
													<input class="password-field form-control form-text required password-processed" type="password" id="edit-pass-pass1" name="pass[pass1]" size="25" maxlength="128"><span class="glyphicon form-control-feedback"></span>
													<div class="progress">
														<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
													</div>
												</div>
												<div class="form-item form-item-pass-pass2 form-type-password form-group col-sm-6 col-md-4 has-feedback"> <label class="control-label" for="edit-pass-pass2">Confirm password <span class="form-required" title="This field is required.">*</span></label>
													<input class="password-confirm form-control form-text required" type="password" id="edit-pass-pass2" name="pass[pass2]" size="25" maxlength="128"></div>
												<div class="help-block password-help"></div>
											</div>
										</div>
									</div><input type="hidden" name="form_build_id" value="form-nicpj0PsnPHn4ZZDf5DNDKIQtA5fNnrPzyLDBt7y5VU">
									<input type="hidden" name="form_id" value="user_register_form">
									<input type="hidden" name="honeypot_time" value="1495792438|9CSJ-F2Zv0Ba4q3cNbcVDFOwQFIG60Pg7pJ-K9Lzza8">
									<div class="field-type-list-text field-name-field-user-salutation field-widget-options-select form-wrapper form-group" id="edit-field-user-salutation">
										<div class="form-item form-item-field-user-salutation-und form-type-select form-group"> <label class="control-label" for="edit-field-user-salutation-und">Salutation</label>
											<select class="form-control form-select" id="edit-field-user-salutation-und" name="field_user_salutation[und]"><option value="_none">- None -</option><option value="dr">Dr.</option><option value="ms">Ms.</option><option value="mrs">Mrs.</option><option value="mr">Mr.</option></select></div>
									</div>
									<div class="field-type-text field-name-field-user-first-name field-widget-text-textfield form-wrapper form-group" id="edit-field-user-first-name">
										<div id="field-user-first-name-add-more-wrapper">
											<div class="form-item form-item-field-user-first-name-und-0-value form-type-textfield form-group"> <label class="control-label" for="edit-field-user-first-name-und-0-value">First Name</label>
												<input class="text-full form-control form-text" type="text" id="edit-field-user-first-name-und-0-value" name="field_user_first_name[und][0][value]" value="" size="60" maxlength="255"></div>
										</div>
									</div>
									<div class="field-type-text field-name-field-user-middle-name field-widget-text-textfield form-wrapper form-group" id="edit-field-user-middle-name">
										<div id="field-user-middle-name-add-more-wrapper">
											<div class="form-item form-item-field-user-middle-name-und-0-value form-type-textfield form-group"> <label class="control-label" for="edit-field-user-middle-name-und-0-value">Middle Name</label>
												<input class="text-full form-control form-text" type="text" id="edit-field-user-middle-name-und-0-value" name="field_user_middle_name[und][0][value]" value="" size="60" maxlength="255"></div>
										</div>
									</div>
									<div class="field-type-text field-name-field-user-last-name field-widget-text-textfield form-wrapper form-group" id="edit-field-user-last-name">
										<div id="field-user-last-name-add-more-wrapper">
											<div class="form-item form-item-field-user-last-name-und-0-value form-type-textfield form-group"> <label class="control-label" for="edit-field-user-last-name-und-0-value">Last Name</label>
												<input class="text-full form-control form-text" type="text" id="edit-field-user-last-name-und-0-value" name="field_user_last_name[und][0][value]" value="" size="60" maxlength="255"></div>
										</div>
									</div>
									<div class="field-type-list-text field-name-field-user-suffix field-widget-options-select form-wrapper form-group" id="edit-field-user-suffix">
										<div class="form-item form-item-field-user-suffix-und form-type-select form-group"> <label class="control-label" for="edit-field-user-suffix-und">Suffix</label>
											<select class="form-control form-select" id="edit-field-user-suffix-und" name="field_user_suffix[und]"><option value="_none">- None -</option><option value="jr">Jr.</option><option value="sr">Sr.</option><option value="ii">II</option><option value="iii">III</option><option value="iv">IV</option></select></div>
									</div>
									<div class="field-type-text field-name-field-user-preferred-name field-widget-text-textfield form-wrapper form-group" id="edit-field-user-preferred-name">
										<div id="field-user-preferred-name-add-more-wrapper">
											<div class="form-item form-item-field-user-preferred-name-und-0-value form-type-textfield form-group"> <label class="control-label" for="edit-field-user-preferred-name-und-0-value">Preferred Name</label>
												<input class="text-full form-control form-text" title="" data-toggle="tooltip" type="text" id="edit-field-user-preferred-name-und-0-value" name="field_user_preferred_name[und][0][value]" value="" size="60" maxlength="255" data-original-title="Nickname, Spiritual name, etc"></div>
										</div>
									</div>
									<div class="field-type-text field-name-field-user-job-title field-widget-text-textfield form-wrapper form-group" id="edit-field-user-job-title">
										<div id="field-user-job-title-add-more-wrapper">
											<div class="form-item form-item-field-user-job-title-und-0-value form-type-textfield form-group"> <label class="control-label" for="edit-field-user-job-title-und-0-value">Job Title</label>
												<input class="text-full form-control form-text" type="text" id="edit-field-user-job-title-und-0-value" name="field_user_job_title[und][0][value]" value="" size="60" maxlength="255"></div>
										</div>
									</div>
									<div class="field-type-addressfield field-name-field-user-address field-widget-addressfield-standard form-wrapper form-group" id="edit-field-user-address">
										<div id="field-user-address-add-more-wrapper">
											<div id="addressfield-wrapper">
												<fieldset id="edit-field-user-address-und-0" class="panel panel-default form-wrapper">
													<legend class="panel-heading">
														<span class="panel-title fieldset-legend">Address</span>
													</legend>
													<div class="panel-body">
														<div class="addressfield-container-inline name-block">
															<div class="form-item form-item-field-user-address-und-0-organisation-name form-type-textfield form-group"> <label class="control-label" for="edit-field-user-address-und-0-organisation-name">Company</label>
																<input class="organisation-name form-control form-text" type="text" id="edit-field-user-address-und-0-organisation-name" name="field_user_address[und][0][organisation_name]" value="" size="30" maxlength="128"></div>
														</div>
														<div class="form-item form-item-field-user-address-und-0-country form-type-select form-group"> <label class="control-label" for="edit-field-user-address-und-0-country">Country</label>
															<select class="country form-control form-select ajax-processed" id="edit-field-user-address-und-0-country" name="field_user_address[und][0][country]" size="0"><option value="AF">Afghanistan</option><option value="AX">Aland Islands</option><option value="AL">Albania</option><option value="DZ">Algeria</option><option value="AS">American Samoa</option><option value="AD">Andorra</option><option value="AO">Angola</option><option value="AI">Anguilla</option><option value="AQ">Antarctica</option><option value="AG">Antigua and Barbuda</option><option value="AR">Argentina</option><option value="AM">Armenia</option><option value="AW">Aruba</option><option value="AU">Australia</option><option value="AT">Austria</option><option value="AZ">Azerbaijan</option><option value="BS">Bahamas</option><option value="BH">Bahrain</option><option value="BD">Bangladesh</option><option value="BB">Barbados</option><option value="BY">Belarus</option><option value="BE">Belgium</option><option value="BZ">Belize</option><option value="BJ">Benin</option><option value="BM">Bermuda</option><option value="BT">Bhutan</option><option value="BO">Bolivia</option><option value="BA">Bosnia and Herzegovina</option><option value="BW">Botswana</option><option value="BV">Bouvet Island</option><option value="BR">Brazil</option><option value="IO">British Indian Ocean Territory</option><option value="VG">British Virgin Islands</option><option value="BN">Brunei</option><option value="BG">Bulgaria</option><option value="BF">Burkina Faso</option><option value="BI">Burundi</option><option value="KH">Cambodia</option><option value="CM">Cameroon</option><option value="CA">Canada</option><option value="CV">Cape Verde</option><option value="BQ">Caribbean Netherlands</option><option value="KY">Cayman Islands</option><option value="CF">Central African Republic</option><option value="TD">Chad</option><option value="CL">Chile</option><option value="CN">China</option><option value="CX">Christmas Island</option><option value="CC">Cocos (Keeling) Islands</option><option value="CO">Colombia</option><option value="KM">Comoros</option><option value="CG">Congo (Brazzaville)</option><option value="CD">Congo (Kinshasa)</option><option value="CK">Cook Islands</option><option value="CR">Costa Rica</option><option value="HR">Croatia</option><option value="CU">Cuba</option><option value="CW">Curaçao</option><option value="CY">Cyprus</option><option value="CZ">Czech Republic</option><option value="DK">Denmark</option><option value="DJ">Djibouti</option><option value="DM">Dominica</option><option value="DO">Dominican Republic</option><option value="EC">Ecuador</option><option value="EG">Egypt</option><option value="SV">El Salvador</option><option value="GQ">Equatorial Guinea</option><option value="ER">Eritrea</option><option value="EE">Estonia</option><option value="ET">Ethiopia</option><option value="FK">Falkland Islands</option><option value="FO">Faroe Islands</option><option value="FJ">Fiji</option><option value="FI">Finland</option><option value="FR">France</option><option value="GF">French Guiana</option><option value="PF">French Polynesia</option><option value="TF">French Southern Territories</option><option value="GA">Gabon</option><option value="GM">Gambia</option><option value="GE">Georgia</option><option value="DE">Germany</option><option value="GH">Ghana</option><option value="GI">Gibraltar</option><option value="GR">Greece</option><option value="GL">Greenland</option><option value="GD">Grenada</option><option value="GP">Guadeloupe</option><option value="GU">Guam</option><option value="GT">Guatemala</option><option value="GG">Guernsey</option><option value="GN">Guinea</option><option value="GW">Guinea-Bissau</option><option value="GY">Guyana</option><option value="HT">Haiti</option><option value="HM">Heard Island and McDonald Islands</option><option value="HN">Honduras</option><option value="HK">Hong Kong S.A.R., China</option><option value="HU">Hungary</option><option value="IS">Iceland</option><option value="IN">India</option><option value="ID">Indonesia</option><option value="IR">Iran</option><option value="IQ">Iraq</option><option value="IE">Ireland</option><option value="IM">Isle of Man</option><option value="IL">Israel</option><option value="IT">Italy</option><option value="CI">Ivory Coast</option><option value="JM">Jamaica</option><option value="JP">Japan</option><option value="JE">Jersey</option><option value="JO">Jordan</option><option value="KZ">Kazakhstan</option><option value="KE">Kenya</option><option value="KI">Kiribati</option><option value="KW">Kuwait</option><option value="KG">Kyrgyzstan</option><option value="LA">Laos</option><option value="LV">Latvia</option><option value="LB">Lebanon</option><option value="LS">Lesotho</option><option value="LR">Liberia</option><option value="LY">Libya</option><option value="LI">Liechtenstein</option><option value="LT">Lithuania</option><option value="LU">Luxembourg</option><option value="MO">Macao S.A.R., China</option><option value="MK">Macedonia</option><option value="MG">Madagascar</option><option value="MW">Malawi</option><option value="MY">Malaysia</option><option value="MV">Maldives</option><option value="ML">Mali</option><option value="MT">Malta</option><option value="MH">Marshall Islands</option><option value="MQ">Martinique</option><option value="MR">Mauritania</option><option value="MU">Mauritius</option><option value="YT">Mayotte</option><option value="MX">Mexico</option><option value="FM">Micronesia</option><option value="MD">Moldova</option><option value="MC">Monaco</option><option value="MN">Mongolia</option><option value="ME">Montenegro</option><option value="MS">Montserrat</option><option value="MA">Morocco</option><option value="MZ">Mozambique</option><option value="MM">Myanmar</option><option value="NA">Namibia</option><option value="NR">Nauru</option><option value="NP">Nepal</option><option value="NL">Netherlands</option><option value="AN">Netherlands Antilles</option><option value="NC">New Caledonia</option><option value="NZ">New Zealand</option><option value="NI">Nicaragua</option><option value="NE">Niger</option><option value="NG">Nigeria</option><option value="NU">Niue</option><option value="NF">Norfolk Island</option><option value="MP">Northern Mariana Islands</option><option value="KP">North Korea</option><option value="NO">Norway</option><option value="OM">Oman</option><option value="PK">Pakistan</option><option value="PW">Palau</option><option value="PS">Palestinian Territory</option><option value="PA">Panama</option><option value="PG">Papua New Guinea</option><option value="PY">Paraguay</option><option value="PE">Peru</option><option value="PH">Philippines</option><option value="PN">Pitcairn</option><option value="PL">Poland</option><option value="PT">Portugal</option><option value="PR">Puerto Rico</option><option value="QA">Qatar</option><option value="RE">Reunion</option><option value="RO">Romania</option><option value="RU">Russia</option><option value="RW">Rwanda</option><option value="BL">Saint Barthélemy</option><option value="SH">Saint Helena</option><option value="KN">Saint Kitts and Nevis</option><option value="LC">Saint Lucia</option><option value="MF">Saint Martin (French part)</option><option value="PM">Saint Pierre and Miquelon</option><option value="VC">Saint Vincent and the Grenadines</option><option value="WS">Samoa</option><option value="SM">San Marino</option><option value="ST">Sao Tome and Principe</option><option value="SA">Saudi Arabia</option><option value="SN">Senegal</option><option value="RS">Serbia</option><option value="SC">Seychelles</option><option value="SL">Sierra Leone</option><option value="SG">Singapore</option><option value="SX">Sint Maarten</option><option value="SK">Slovakia</option><option value="SI">Slovenia</option><option value="SB">Solomon Islands</option><option value="SO">Somalia</option><option value="ZA">South Africa</option><option value="GS">South Georgia and the South Sandwich Islands</option><option value="KR">South Korea</option><option value="SS">South Sudan</option><option value="ES">Spain</option><option value="LK">Sri Lanka</option><option value="SD">Sudan</option><option value="SR">Suriname</option><option value="SJ">Svalbard and Jan Mayen</option><option value="SZ">Swaziland</option><option value="SE">Sweden</option><option value="CH">Switzerland</option><option value="SY">Syria</option><option value="TW">Taiwan</option><option value="TJ">Tajikistan</option><option value="TZ">Tanzania</option><option value="TH">Thailand</option><option value="TL">Timor-Leste</option><option value="TG">Togo</option><option value="TK">Tokelau</option><option value="TO">Tonga</option><option value="TT">Trinidad and Tobago</option><option value="TN">Tunisia</option><option value="TR">Turkey</option><option value="TM">Turkmenistan</option><option value="TC">Turks and Caicos Islands</option><option value="TV">Tuvalu</option><option value="VI">U.S. Virgin Islands</option><option value="UG">Uganda</option><option value="UA">Ukraine</option><option value="AE">United Arab Emirates</option><option value="GB">United Kingdom</option><option value="US" selected="selected">United States</option><option value="UM">United States Minor Outlying Islands</option><option value="UY">Uruguay</option><option value="UZ">Uzbekistan</option><option value="VU">Vanuatu</option><option value="VA">Vatican</option><option value="VE">Venezuela</option><option value="VN">Vietnam</option><option value="WF">Wallis and Futuna</option><option value="EH">Western Sahara</option><option value="YE">Yemen</option><option value="ZM">Zambia</option><option value="ZW">Zimbabwe</option></select></div>
														<div class="addressfield-container-inline street-block country-US">
															<div class="form-item form-item-field-user-address-und-0-thoroughfare form-type-textfield form-group"> <label class="control-label" for="edit-field-user-address-und-0-thoroughfare">Address 1</label>
																<input class="thoroughfare form-control form-text" type="text" id="edit-field-user-address-und-0-thoroughfare" name="field_user_address[und][0][thoroughfare]" value="" size="30" maxlength="128"></div>
															<div class="form-item form-item-field-user-address-und-0-premise form-type-textfield form-group"> <label class="control-label" for="edit-field-user-address-und-0-premise">Address 2</label>
																<input class="premise form-control form-text" type="text" id="edit-field-user-address-und-0-premise" name="field_user_address[und][0][premise]" value="" size="30" maxlength="128"></div>
															<div class="form-item form-item-field-user-address-und-0-sub-premise form-type-textfield form-group"> <label class="control-label" for="edit-field-user-address-und-0-sub-premise">Address 3</label>
																<input class="sub-premise form-control form-text" type="text" id="edit-field-user-address-und-0-sub-premise" name="field_user_address[und][0][sub_premise]" value="" size="30" maxlength="128"></div>
														</div>
														<div class="addressfield-container-inline locality-block country-US">
															<div class="form-item form-item-field-user-address-und-0-locality form-type-textfield form-group"> <label class="control-label" for="edit-field-user-address-und-0-locality">City</label>
																<input class="locality form-control form-text" type="text" id="edit-field-user-address-und-0-locality" name="field_user_address[und][0][locality]" value="" size="30" maxlength="128"></div>&nbsp;
															<div class="form-item form-item-field-user-address-und-0-administrative-area form-type-select form-group"> <label class="control-label" for="edit-field-user-address-und-0-administrative-area">State</label>
																<select class="state form-control form-select" id="edit-field-user-address-und-0-administrative-area" name="field_user_address[und][0][administrative_area]" size="0"><option value="" selected="selected">--</option><option value="AL">Alabama</option><option value="AK">Alaska</option><option value="AZ">Arizona</option><option value="AR">Arkansas</option><option value="CA">California</option><option value="CO">Colorado</option><option value="CT">Connecticut</option><option value="DE">Delaware</option><option value="DC">District Of Columbia</option><option value="FL">Florida</option><option value="GA">Georgia</option><option value="HI">Hawaii</option><option value="ID">Idaho</option><option value="IL">Illinois</option><option value="IN">Indiana</option><option value="IA">Iowa</option><option value="KS">Kansas</option><option value="KY">Kentucky</option><option value="LA">Louisiana</option><option value="ME">Maine</option><option value="MD">Maryland</option><option value="MA">Massachusetts</option><option value="MI">Michigan</option><option value="MN">Minnesota</option><option value="MS">Mississippi</option><option value="MO">Missouri</option><option value="MT">Montana</option><option value="NE">Nebraska</option><option value="NV">Nevada</option><option value="NH">New Hampshire</option><option value="NJ">New Jersey</option><option value="NM">New Mexico</option><option value="NY">New York</option><option value="NC">North Carolina</option><option value="ND">North Dakota</option><option value="OH">Ohio</option><option value="OK">Oklahoma</option><option value="OR">Oregon</option><option value="PA">Pennsylvania</option><option value="RI">Rhode Island</option><option value="SC">South Carolina</option><option value="SD">South Dakota</option><option value="TN">Tennessee</option><option value="TX">Texas</option><option value="UT">Utah</option><option value="VT">Vermont</option><option value="VA">Virginia</option><option value="WA">Washington</option><option value="WV">West Virginia</option><option value="WI">Wisconsin</option><option value="WY">Wyoming</option><option value=" ">--</option><option value="AS">American Samoa</option><option value="FM">Federated States of Micronesia</option><option value="GU">Guam</option><option value="MH">Marshall Islands</option><option value="MP">Northern Mariana Islands</option><option value="PW">Palau</option><option value="PR">Puerto Rico</option><option value="VI">Virgin Islands</option></select></div>&nbsp;
															<div class="form-item form-item-field-user-address-und-0-postal-code form-type-textfield form-group"> <label class="control-label" for="edit-field-user-address-und-0-postal-code">ZIP Code</label>
																<input class="postal-code form-control form-text" type="text" id="edit-field-user-address-und-0-postal-code" name="field_user_address[und][0][postal_code]" value="" size="10" maxlength="128"></div>
														</div>
													</div>
												</fieldset>
											</div>
										</div>
									</div>
									<div class="field-type-text field-name-field-user-phone field-widget-text-textfield form-wrapper form-group" id="edit-field-user-phone">
										<div id="field-user-phone-add-more-wrapper">
											<div class="form-item form-item-field-user-phone-und-0-value form-type-textfield form-group"> <label class="control-label" for="edit-field-user-phone-und-0-value">Daytime Phone</label>
												<input class="text-full form-control form-text" type="text" id="edit-field-user-phone-und-0-value" name="field_user_phone[und][0][value]" value="" size="60" maxlength="255"></div>
										</div>
									</div>
									<div class="field-type-text field-name-field-user-evening-phone field-widget-text-textfield form-wrapper form-group" id="edit-field-user-evening-phone">
										<div id="field-user-evening-phone-add-more-wrapper">
											<div class="form-item form-item-field-user-evening-phone-und-0-value form-type-textfield form-group"> <label class="control-label" for="edit-field-user-evening-phone-und-0-value">Evening Phone</label>
												<input class="text-full form-control form-text" type="text" id="edit-field-user-evening-phone-und-0-value" name="field_user_evening_phone[und][0][value]" value="" size="60" maxlength="255"></div>
										</div>
									</div>
									<div class="field-type-text field-name-field-user-cell-phone field-widget-text-textfield form-wrapper form-group" id="edit-field-user-cell-phone">
										<div id="field-user-cell-phone-add-more-wrapper">
											<div class="form-item form-item-field-user-cell-phone-und-0-value form-type-textfield form-group"> <label class="control-label" for="edit-field-user-cell-phone-und-0-value">Cell Phone</label>
												<input class="text-full form-control form-text" type="text" id="edit-field-user-cell-phone-und-0-value" name="field_user_cell_phone[und][0][value]" value="" size="60" maxlength="255"></div>
										</div>
									</div>
									<div class="field-type-list-boolean field-name-field-user-receive-text-messages field-widget-options-onoff form-wrapper form-group" id="edit-field-user-receive-text-messages">
										<div class="form-item form-item-field-user-receive-text-messages-und form-type-checkbox checkbox"> <label class="control-label" for="edit-field-user-receive-text-messages-und"><input type="checkbox" id="edit-field-user-receive-text-messages-und" name="field_user_receive_text_messages[und]" value="1" class="form-checkbox">Receive Text Messages</label>
										</div>
									</div>
									<div class="field-type-list-boolean field-name-field-email-opt-in field-widget-options-onoff form-wrapper form-group" id="edit-field-email-opt-in">
										<div class="form-item form-item-field-email-opt-in-und form-type-checkbox checkbox"> <label class="control-label" for="edit-field-email-opt-in-und"><input type="checkbox" id="edit-field-email-opt-in-und" name="field_email_opt_in[und]" value="1" class="form-checkbox">I want to receive articles, videos, audio, announcements, and special offers from Omega.</label>
										</div>
									</div>
									<div class="field-type-taxonomy-term-reference field-name-field-user-areas-of-interest field-widget-options-buttons form-wrapper form-group" id="edit-field-user-areas-of-interest">
										<div class="form-item form-item-field-user-areas-of-interest-und form-type-checkboxes form-group"> <label class="control-label" for="edit-field-user-areas-of-interest-und">Areas of interest</label>
											<div id="edit-field-user-areas-of-interest-und" class="form-checkboxes">
												<div class="form-item form-item-field-user-areas-of-interest-und-184 form-type-checkbox checkbox"> <label class="control-label" for="edit-field-user-areas-of-interest-und-184"><input type="checkbox" id="edit-field-user-areas-of-interest-und-184" name="field_user_areas_of_interest[und][184]" value="184" class="form-checkbox">Body Mind &amp; Spirit</label>
												</div>
												<div class="form-item form-item-field-user-areas-of-interest-und-183 form-type-checkbox checkbox"> <label class="control-label" for="edit-field-user-areas-of-interest-und-183"><input type="checkbox" id="edit-field-user-areas-of-interest-und-183" name="field_user_areas_of_interest[und][183]" value="183" class="form-checkbox">Health &amp; Healing</label>
												</div>
												<div class="form-item form-item-field-user-areas-of-interest-und-185 form-type-checkbox checkbox"> <label class="control-label" for="edit-field-user-areas-of-interest-und-185"><input type="checkbox" id="edit-field-user-areas-of-interest-und-185" name="field_user_areas_of_interest[und][185]" value="185" class="form-checkbox">Creative Expression</label>
												</div>
												<div class="form-item form-item-field-user-areas-of-interest-und-187 form-type-checkbox checkbox"> <label class="control-label" for="edit-field-user-areas-of-interest-und-187"><input type="checkbox" id="edit-field-user-areas-of-interest-und-187" name="field_user_areas_of_interest[und][187]" value="187" class="form-checkbox">Relationships &amp; Family</label>
												</div>
												<div class="form-item form-item-field-user-areas-of-interest-und-698 form-type-checkbox checkbox"> <label class="control-label" for="edit-field-user-areas-of-interest-und-698"><input type="checkbox" id="edit-field-user-areas-of-interest-und-698" name="field_user_areas_of_interest[und][698]" value="698" class="form-checkbox">Leadership &amp; Work</label>
												</div>
												<div class="form-item form-item-field-user-areas-of-interest-und-188 form-type-checkbox checkbox"> <label class="control-label" for="edit-field-user-areas-of-interest-und-188"><input type="checkbox" id="edit-field-user-areas-of-interest-und-188" name="field_user_areas_of_interest[und][188]" value="188" class="form-checkbox">Sustainable Living</label>
												</div>
											</div>
										</div>
									</div>
									<div class="field-type-text field-name-field-user-gender field-widget-select-or-other form-wrapper form-group" id="edit-field-user-gender">
										<div class="form-item form-item-field-user-gender-und">
											<div class="select-or-other select-or-other-processed">
												<div class="form-item form-item-field-user-gender-und-select form-type-select form-group"> <label class="control-label" for="edit-field-user-gender-und-select">Gender</label>
													<select class="select-or-other-select form-control form-select" id="edit-field-user-gender-und-select" name="field_user_gender[und][select]"><option value="_none">- Select a value -</option><option value="F">Female</option><option value="M">Male</option><option value="select_or_other">Self-identified</option></select></div>
												<div class="form-item form-item-field-user-gender-und-other form-type-textfield form-group" style="display: none;"> <label class="control-label" for="edit-field-user-gender-und-other">Self-identified</label>
													<input class="select-or-other-other form-control form-text" type="text" id="edit-field-user-gender-und-other" name="field_user_gender[und][other]" value="" size="60" maxlength="255"></div>
											</div>
										</div>
									</div>
									<div class="field-type-taxonomy-term-reference field-name-field-user-industry-occupation field-widget-taxonomy-hs form-wrapper form-group" id="edit-field-user-industry-occupation">
										<div class="form-item form-item-field-user-industry-occupation-und form-type-hierarchical-select form-group"> <label class="control-label" for="edit-field-user-industry-occupation-und">Industry/Occupation</label>
											<div class="hierarchical-select-wrapper hierarchical-select-level-labels-style-none hierarchical-select-wrapper-for-name-edit-field-user-industry-occupation-und  hierarchical-select-wrapper-processed" id="hierarchical-select-5927fb36936bb-wrapper"><input type="hidden" name="field_user_industry_occupation[und][hsid]" value="5927fb36936bb">
												<div class="hierarchical-select clearfix">
													<div class="selects"><select class="form-control form-select" id="edit-field-user-industry-occupation-und-hierarchical-select-selects-0" name="field_user_industry_occupation[und][hierarchical_select][selects][0]" size="0"><option value="none" class="" selected="selected">&lt;none&gt;</option></select>
														<div class="grippie"></div>
													</div>
												</div>
												<div class="nojs" style="display: none;"><button class="update-button btn btn-info form-submit ajax-processed" type="submit" id="edit-field-user-industry-occupation-und-nojs-update-button" name="op" value="Update">Update</button>
													<div class="help-text"><noscript>&lt;span class="warning"&gt;You don't have Javascript enabled.&lt;/span&gt; &lt;span class="ask-to-hover"&gt;Hover for more information!&lt;/span&gt; But don't worry: you can still use this web site! You have two options:&lt;ul class="solutions"&gt;&lt;li&gt;&lt;span class="highlight"&gt;enable Javascript&lt;/span&gt; in your browser and then refresh this page, for a much enhanced experience.&lt;/li&gt;
&lt;li&gt;&lt;span class="highlight"&gt;click the &lt;em&gt;Update&lt;/em&gt; button&lt;/span&gt; every time you want to update the selection.&lt;/li&gt;
&lt;/ul&gt;</noscript></div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-actions form-wrapper form-group" id="edit-actions"><button type="submit" id="edit-submit" name="op" value="Create new account" class="btn btn-success form-submit">Create new account</button>
									</div>
									<div class="user_email_address_confirm-textfield">
										<div class="form-item form-item-user-email-address-confirm form-type-textfield form-group"> <label class="control-label" for="edit-user-email-address-confirm">Leave this field blank</label>
											<input autocomplete="off" class="form-control form-text" type="text" id="edit-user-email-address-confirm" name="user_email_address_confirm" value="" size="20" maxlength="128"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</section>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
				</div>
			</div>
		</div>

	</div>
</div>
<?php include ("templates/footer.php"); ?>
