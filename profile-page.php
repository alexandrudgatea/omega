<?php include ("templates/header.php"); ?>

<div id="block-system-main">
	<div id="mainContent" class="profile-page">
		<div class="user-info">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-4">
						<div class="user-profile-image">
							<div class="user-tile">
								<div class="user-image" style="background: url('images/person.jpg')"></div>
								<div class="change-picture">Edit</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-8">
						<div class="user-details">
							<div class="row">
								<div class="col-sm-12">
									<h2 class="user-name">Jane Hopewell</h2>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-12 col-sm-6">
									<h3 class="topics">Topics of interest</h3>
									<ul class="list-inline list-unstyled topic-list">
										<li><a href="#!">Yoga</a></li>
										<li><a href="#!">Meditation</a></li>
										<li><a href="#!">Wellness</a></li>
										<li><a href="#!">Creative Expression</a></li>
										<li><a href="#!">Relationships &amp; Family</a></li>
										<li><a href="#!">Woman in Leadership</a></li>
									</ul>
									<a href="#!" class="btn btn-outline"><i class="fa fa-plus"></i> Add Topic</a>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="user-bttons-group">
										<a href="#!" class="btn bt-default full-width mb10">Subscribe to Newsletter</a>
										<a href="#!" class="btn bt-default full-width">Order Catalogue</a>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="user-tiles">
			<div role="tabpanel">
				<!-- Nav tabs -->
				<div class="container">
					<!--tabs-->
					<div class="row">
						<div class="col-sm-12 lp-tabs pt30 pb30">
							<ul class="nav nav-tabs" role="tablist">
								<li role="presentation" class="active">
									<a href="#userRecommended" aria-controls="userRecommended" role="tab" data-toggle="tab">Recommended</a>
								</li>
								<li role="presentation">
									<a href="#userContent" aria-controls="userContent" role="tab" data-toggle="tab">My Content</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="userRecommended">
									<div class="tiles-grid">
										<div class="tiles-row row">
											<div class="col-sm-12 headline-col">
												<div class="row">
													<div class="col-sm-12 col-md-6">
														<h2 class="headline-title">Omega Women's Leadership Center</h2>
													</div>
													<div class="col-sm-12 col-md-6">
														<a href="#" class="btn see-all pull-right">See all</a>
													</div>
												</div>
											</div>
											<div class="col-sm-12">
												<div class="row">
													<div class="col-sm-4">
														<div class="panel panel-default panel-idea tile">
															<div class="favourite">
																<div class="checkbox">
																	<input type="checkbox" value="" id="favourite2">
																	<label for="favourite2"></label>
																</div>
															</div>
															<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
															</div>
															<div class="panel-body panel-audio-video">
																<div class="content-type">
																	<a href="#"><i class="fa fa-play-circle-o"></i></a>
																	<span>Video</span>
																</div>
																<div class="title">
																	<h1>Bobby McFerrin: Circle Songs from Omega</h1>
																</div>
																<div class="person">
																	<ul class="by list-unstyled list-inline">
																		<li>Thomas Robertson</li>
																		<li>Marry Anderson</li>
																	</ul>
																	<ul class="featuring list-unstyled list-inline">
																		<li>Thomas Robertson</li>
																		<li>Marry Anderson</li>
																	</ul>
																</div>
																<div class="duration-date">
																	<em class="duration">13 minute watch</em>
																	<em class="date-release">13th of September, 2016</em>
																</div>
															</div>
															<div class="panel-footer">
																<a href="#" class="expand-button">
																	<i class="fa fa-plus"></i>
																</a>
																<div class="expanded-tile-content animated fadeInUp">
																	<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
																	<a href="#" class="btn white-transparent-button">Learn More</a>
																</div>
															</div>
														</div>
													</div>
													<div class="col-sm-4">
														<div class="panel panel-default panel-collection tile">
															<div class="favourite">
																<div class="checkbox">
																	<input type="checkbox" value="" id="favourite10">
																	<label for="favourite10"></label>
																</div>
															</div>
															<div class="panel-body">
																<div class="content-type">
																	<span>Playlist</span>
																</div>
																<div class="title">
																	<h1>Pema Chodron's Words of Wisdom</h1>
																</div>
																<div class="image-tiles">
																	<ul class="list-inline">
																		<li style="background:url('images/workshop_events.jpg');"></li>
																		<li style="background:url('images/person.jpg');"></li>
																		<li style="background:url('images/online_learning.jpg');"></li>
																		<li style="background:url('images/idea.jpg');"></li>
																	</ul>
																</div>
																<div class="person">
																	<ul class="list-unstyled list-inline person-names">
																		<li>Thomas Robertson</li>
																		<li>Marry Anderson</li>
																		<li>Marry Anderson</li>
																	</ul>
																</div>
															</div>
															<div class="panel-footer">
																<a href="#" class="expand-button">
																	<i class="fa fa-plus"></i>
																</a>
																<div class="expanded-tile-content animated fadeInUp">
																	<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
																	<a href="#" class="btn white-transparent-button">Learn More</a>
																</div>
															</div>
														</div>
													</div>
													<div class="col-sm-4">
														<div class="panel panel-default panel-learn tile">
															<div class="favourite">
																<div class="checkbox">
																	<input type="checkbox" value="" id="favourite7">
																	<label for="favourite7"></label>
																</div>
															</div>
															<div class="panel-body">
																<div class="content-type">
																	<span>E-course</span>
																</div>
																<div class="title">
																	<h1>Growing &amp; Sustaining A Yoga Service Organization</h1>
																</div>
																<div class="person">

																</div>
															</div>
															<div class="panel-footer">
																<a href="#" class="expand-button">
																	<i class="fa fa-plus"></i>
																</a>
																<div class="expanded-tile-content animated fadeInUp">
																	<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
																	<a href="#" class="btn white-transparent-button">Learn More</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="tiles-row row">
											<div class="col-sm-12 headline-col">
												<div class="row">
													<div class="col-sm-12 col-md-6">
														<h2 class="headline-title">Body, Mind, and Spirit</h2>
													</div>
													<div class="col-sm-12 col-md-6">
														<a href="#" class="btn see-all pull-right">See all</a>
													</div>
												</div>
											</div>
											<div class="col-sm-12">
												<div class="row">
													<div class="col-sm-4">
														<div class="panel panel-default panel-workshop tile">
															<div class="favourite">
																<div class="checkbox">
																	<input type="checkbox" value="" id="favourite4">
																	<label for="favourite4"></label>
																</div>
															</div>
															<div class="panel-body">
																<div class="content-type">
																	<span>Workshop</span>
																</div>
																<div class="title">
																	<h1>Collaborating With Nature</h1>
																</div>
																<div class="person">
																	<ul class="list-unstyled list-inline person-images">
																		<li>
																			<img src="images/person.jpg">
																		</li>
																		<li> <img src="images/person.jpg"></li>
																	</ul>
																	<ul class="list-unstyled list-inline person-names">
																		<li>Thomas Robertson</li>
																		<li>Marry Anderson</li>
																	</ul>
																</div>
																<div class="date-location">
																	<ul class="list-inline">
																		<li><em>Feb 28 - Mar 06 2016</em></li>
																		<li><em>Rhinebeck, NY</em></li>
																	</ul>
																</div>
															</div>
															<div class="panel-footer">
																<a href="#" class="expand-button">
																	<i class="fa fa-plus"></i>
																</a>
																<div class="expanded-tile-content animated fadeInUp">
																	<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
																	<a href="#" class="btn white-transparent-button">Learn More</a>
																</div>
															</div>
														</div>
													</div>
													<div class="col-sm-4">
														<div class="panel panel-default panel-workshop tile">
															<div class="favourite">
																<div class="checkbox">
																	<input type="checkbox" value="" id="favourite5">
																	<label for="favourite5"></label>
																</div>
															</div>
															<div class="panel-body">
																<div class="content-type">
																	<span>Workshop</span>
																</div>
																<div class="title">
																	<h1>Collaborating With Nature</h1>
																</div>
																<div class="person">
																	<ul class="list-unstyled list-inline person-images">
																		<li>
																			<img src="images/person.jpg">
																		</li>
																		<li> <img src="images/person.jpg"></li>
																	</ul>
																	<ul class="list-unstyled list-inline person-names">
																		<li>Thomas Robertson</li>
																		<li>Marry Anderson</li>
																	</ul>
																</div>
																<div class="date-location">
																	<ul class="list-inline">
																		<li><em>Feb 28 - Mar 06 2016</em></li>
																		<li><em>Rhinebeck, NY</em></li>
																	</ul>
																</div>
															</div>
															<div class="panel-footer">
																<a href="#" class="expand-button">
																	<i class="fa fa-plus"></i>
																</a>
																<div class="expanded-tile-content animated fadeInUp">
																	<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
																	<a href="#" class="btn white-transparent-button">Learn More</a>
																</div>
															</div>
														</div>
													</div>
													<div class="col-sm-4">
														<div class="panel panel-default panel-workshop tile">
															<div class="favourite">
																<div class="checkbox">
																	<input type="checkbox" value="" id="favourite6">
																	<label for="favourite6"></label>
																</div>
															</div>
															<div class="panel-body">
																<div class="content-type">
																	<span>Workshop</span>
																</div>
																<div class="title">
																	<h1>Collaborating With Nature</h1>
																</div>
																<div class="person">
																	<ul class="list-unstyled list-inline person-images">
																		<li>
																			<img src="images/person.jpg">
																		</li>
																		<li> <img src="images/person.jpg"></li>
																	</ul>
																	<ul class="list-unstyled list-inline person-names">
																		<li>Thomas Robertson</li>
																		<li>Marry Anderson</li>
																	</ul>
																</div>
																<div class="date-location">
																	<ul class="list-inline">
																		<li><em>Feb 28 - Mar 06 2016</em></li>
																		<li><em>Rhinebeck, NY</em></li>
																	</ul>
																</div>
															</div>
															<div class="panel-footer">
																<a href="#" class="expand-button">
																	<i class="fa fa-plus"></i>
																</a>
																<div class="expanded-tile-content animated fadeInUp">
																	<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
																	<a href="#" class="btn white-transparent-button">Learn More</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="userContent">
									<div class="tiles-grid">
										<div class="tiles-row row">
											<div class="col-sm-12 headline-col">
												<div class="row">
													<div class="col-sm-12 col-md-6">
														<h2 class="headline-title">Workshops &amp; Events</h2>
													</div>
													<div class="col-sm-12 col-md-6">
														<a href="#" class="btn see-all pull-right">See all</a>
													</div>
												</div>
											</div>
											<div class="col-sm-12">
												<div class="row">
													<div class="col-sm-4">
														<div class="panel panel-default panel-workshop tile">
															<div class="favourite">
																<div class="checkbox">
																	<input type="checkbox" value="" id="favourite4">
																	<label for="favourite4"></label>
																</div>
															</div>
															<div class="panel-body">
																<div class="content-type">
																	<span>Workshop</span>
																</div>
																<div class="title">
																	<h1>Collaborating With Nature</h1>
																</div>
																<div class="person">
																	<ul class="list-unstyled list-inline person-images">
																		<li>
																			<img src="images/person.jpg">
																		</li>
																		<li> <img src="images/person.jpg"></li>
																	</ul>
																	<ul class="list-unstyled list-inline person-names">
																		<li>Thomas Robertson</li>
																		<li>Marry Anderson</li>
																	</ul>
																</div>
																<div class="date-location">
																	<ul class="list-inline">
																		<li><em>Feb 28 - Mar 06 2016</em></li>
																		<li><em>Rhinebeck, NY</em></li>
																	</ul>
																</div>
															</div>
															<div class="panel-footer">
																<a href="#" class="expand-button">
																	<i class="fa fa-plus"></i>
																</a>
																<div class="expanded-tile-content animated fadeInUp">
																	<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
																	<a href="#" class="btn white-transparent-button">Learn More</a>
																</div>
															</div>
														</div>
													</div>
													<div class="col-sm-4">
														<div class="panel panel-default panel-workshop tile">
															<div class="favourite">
																<div class="checkbox">
																	<input type="checkbox" value="" id="favourite5">
																	<label for="favourite5"></label>
																</div>
															</div>
															<div class="panel-body">
																<div class="content-type">
																	<span>Workshop</span>
																</div>
																<div class="title">
																	<h1>Collaborating With Nature</h1>
																</div>
																<div class="person">
																	<ul class="list-unstyled list-inline person-images">
																		<li>
																			<img src="images/person.jpg">
																		</li>
																		<li> <img src="images/person.jpg"></li>
																	</ul>
																	<ul class="list-unstyled list-inline person-names">
																		<li>Thomas Robertson</li>
																		<li>Marry Anderson</li>
																	</ul>
																</div>
																<div class="date-location">
																	<ul class="list-inline">
																		<li><em>Feb 28 - Mar 06 2016</em></li>
																		<li><em>Rhinebeck, NY</em></li>
																	</ul>
																</div>
															</div>
															<div class="panel-footer">
																<a href="#" class="expand-button">
																	<i class="fa fa-plus"></i>
																</a>
																<div class="expanded-tile-content animated fadeInUp">
																	<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
																	<a href="#" class="btn white-transparent-button">Learn More</a>
																</div>
															</div>
														</div>
													</div>
													<div class="col-sm-4">
														<div class="panel panel-default panel-workshop tile">
															<div class="favourite">
																<div class="checkbox">
																	<input type="checkbox" value="" id="favourite6">
																	<label for="favourite6"></label>
																</div>
															</div>
															<div class="panel-body">
																<div class="content-type">
																	<span>Workshop</span>
																</div>
																<div class="title">
																	<h1>Collaborating With Nature</h1>
																</div>
																<div class="person">
																	<ul class="list-unstyled list-inline person-images">
																		<li>
																			<img src="images/person.jpg">
																		</li>
																		<li> <img src="images/person.jpg"></li>
																	</ul>
																	<ul class="list-unstyled list-inline person-names">
																		<li>Thomas Robertson</li>
																		<li>Marry Anderson</li>
																	</ul>
																</div>
																<div class="date-location">
																	<ul class="list-inline">
																		<li><em>Feb 28 - Mar 06 2016</em></li>
																		<li><em>Rhinebeck, NY</em></li>
																	</ul>
																</div>
															</div>
															<div class="panel-footer">
																<a href="#" class="expand-button">
																	<i class="fa fa-plus"></i>
																</a>
																<div class="expanded-tile-content animated fadeInUp">
																	<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
																	<a href="#" class="btn white-transparent-button">Learn More</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="tiles-row row">
											<div class="col-sm-12 headline-col">
												<div class="row">
													<div class="col-sm-12 col-md-6">
														<h2 class="headline-title">Ideas</h2>
													</div>
													<div class="col-sm-12 col-md-6">
														<a href="#" class="btn see-all pull-right">See all</a>
													</div>
												</div>
											</div>
											<div class="col-sm-12">
												<div class="row">
													<div class="col-sm-4">
														<div class="panel panel-default panel-idea tile">
															<div class="favourite">
																<div class="checkbox">
																	<input type="checkbox" value="" id="favourite1">
																	<label for="favourite1"></label>
																</div>
															</div>
															<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
															</div>
															<div class="panel-body panel-article">
																<div class="content-type">
																	<span>Article</span>
																</div>
																<div class="title">
																	<h1>Bobby McFerrin: Circle Songs from Omega</h1>
																</div>
																<div class="person">
																	<ul class="by list-unstyled list-inline">
																		<li>Thomas Robertson</li>
																		<li>Marry Anderson</li>
																	</ul>
																	<span class="featuring">Featuring:</span>
																	<ul class="featuring list-unstyled list-inline">
																		<li>Thomas Robertson</li>
																		<li>Marry Anderson</li>
																	</ul>
																</div>
																<div class="duration-date">
																	<em class="duration">13 minute watch</em>
																	<em class="date-release">13th of September, 2016</em>
																</div>
															</div>
														</div>
													</div>
													<div class="col-sm-4">
														<div class="panel panel-default panel-idea  tile">
															<div class="favourite">
																<div class="checkbox">
																	<input type="checkbox" value="" id="favourite2">
																	<label for="favourite2"></label>
																</div>
															</div>
															<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
															</div>
															<div class="panel-body panel-audio-video">
																<div class="content-type">
																	<a href="#"><i class="fa fa-play-circle-o"></i></a>
																	<span>Video</span>
																</div>
																<div class="title">
																	<h1>Bobby McFerrin: Circle Songs from Omega</h1>
																</div>
																<div class="person">
																	<ul class="by list-unstyled list-inline">
																		<li>Thomas Robertson</li>
																		<li>Marry Anderson</li>
																	</ul>
																	<ul class="featuring list-unstyled list-inline">
																		<li>Thomas Robertson</li>
																		<li>Marry Anderson</li>
																	</ul>
																</div>
																<div class="duration-date">
																	<em class="duration">13 minute watch</em>
																	<em class="date-release">13th of September, 2016</em>
																</div>
															</div>
														</div>
													</div>
													<div class="col-sm-4">
														<div class="panel panel-default panel-idea tile">
															<div class="favourite">
																<div class="checkbox">
																	<input type="checkbox" value="" id="favourite3">
																	<label for="favourite3"></label>
																</div>
															</div>
															<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
															</div>
															<div class="panel-body panel-press">
																<div class="content-type">
																	<span>Press Release</span>
																</div>
																<div class="title">
																	<h1>Bobby McFerrin: Circle Songs from Omega</h1>
																</div>
																<div class="person">
																	<ul class="by list-unstyled list-inline">
																		<li>Thomas Robertson</li>
																		<li>Marry Anderson</li>
																	</ul>
																	<span class="featuring">Featuring:</span>
																	<ul class="featuring list-unstyled list-inline">
																		<li>Thomas Robertson</li>
																		<li>Marry Anderson</li>
																	</ul>
																</div>
																<div class="duration-date">
																	<em class="duration">13 minute watch</em>
																	<em class="date-release">13<sup>th</sup> of September, 2016</em>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include ("templates/footer.php"); ?>
