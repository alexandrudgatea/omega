<div id="footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<a href="#!"><img src="http://test2.eomega.org/sites/all/themes/omeganew/images/logo-inverted.png" class="img-responsive footer-logo"></a>
			</div>
			<div class="col-sm-6">
				<form class="webform-client-form webform-client-form-15421" action="/" method="post" id="webform-client-form-15421" accept-charset="UTF-8">
					<div>
						<div class="form-item webform-component webform-component-email webform-component--email form-group form-item form-item-submitted-email form-type-webform-email form-group">
							<label class="control-label" for="edit-submitted-email--2">Newsletter Sign-up</label>
							<input placeholder="you@email.com" class="form-control form-text form-email" type="email" id="edit-submitted-email--2" name="submitted[email]" size="60">
						</div>
						<input type="hidden" name="details[sid]">
						<input type="hidden" name="details[page_num]" value="1">
						<input type="hidden" name="details[page_count]" value="1">
						<input type="hidden" name="details[finished]" value="0">
						<input type="hidden" name="form_build_id" value="form-4qUZhbKON7FYMwc9m7pkRorerEGr8wY3fuMtm9YN2Qs">
						<input type="hidden" name="form_id" value="webform_client_form_15421">
						<input type="hidden" name="honeypot_time" value="1487321887|97xjRvbpB9cPO92bSrU5qm4gckWRvhiKniA3MdZ7zYs">
						<div class="user_email_address_confirm-textfield">

							<div class="form-item form-item-user-email-address-confirm form-type-textfield form-group">
								<label class="control-label" for="edit-user-email-address-confirm--2">Leave this field blank</label>
								<input autocomplete="off" class="form-control form-text" type="text" id="edit-user-email-address-confirm--2" name="user_email_address_confirm" value="" size="20" maxlength="128">
							</div>
						</div>
						<div class="form-actions">
							<button class="webform-submit button-primary btn btn-default form-submit" id="edit-webform-ajax-submit-15421" type="submit" name="op" value="Sign Up">Sign Up</button>

						</div>

					</div>

				</form>
			</div>
			<div class="col-sm-3">

				<ul class="list-inline social-links">
					<li>
						<a id="facebook-share-footer" style="cursor: pointer;">
							<span class="fa-stack fa-lg">
					<i class="fa fa-circle fa-stack-2x teal"></i>
					<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
				</span>
						</a>
					</li>
					<li>
						<a id="twitter-share-footer">
							<span class="fa-stack fa-lg">
					<i class="fa fa-circle fa-stack-2x teal"></i>
					<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
				</span>
						</a>
					</li>
					<li>
						<a href="mailto:?subject=&amp;body=Read%20more%20">
							<span class="fa-stack fa-lg">
					<i class="fa fa-circle fa-stack-2x teal"></i>
					<i class="fa fa-envelope-o fa-stack-1x fa-inverse"></i>
				</span>
						</a>
					</li>
					<li>
						<a href="#!">
							<span class="fa-stack fa-lg">
					<i class="fa fa-circle fa-stack-2x teal"></i>
					<i class="fa fa-share-alt fa-stack-1x fa-inverse"></i>
				</span>
						</a>
					</li>
				</ul>

				<script type="text/javascript">
					jQuery(document).ready(function($) {
						$("#facebook-share-footer").on('click', function() {

							link = 'http://test2.eomega.org/';
							image = '';
							title = 'OMEGA';

							FB.ui({
								method: 'share',
								href: link,
								picture: image,
								title: title
							}, function(response) {});
						});

						$("#twitter-share-footer").attr("href", "http://www.twitter.com/share?url=http://test2.eomega.org/&text=OMEGA").attr("target", "_blank");
					});

				</script>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-6">
				<ul class="list-inline footer-menu field-text">
					<li><a href="#!" class="field-text">Privacy Policy</a></li>
					<li><a href="#!" class="field-text">Terms of Use</a></li>
					<li><a href="#!" class="field-text">Contact</a></li>
				</ul>
			</div>
			<div class="col-sm-3">
				<ul class="list-inline copyright pull-right">
					<li>
						<span class="block field-text">©2015 Omega<br>Institute</span>
					</li>
					<li>
						<span class="block field-text">                            <br></span>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
</body>

</html>
