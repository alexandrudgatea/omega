<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Omega</title>
    <link rel="shortcut icon" href="">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/mediaquery.css">
    <script src="https://use.fontawesome.com/4a75309447.js"></script>
    <script src="http://use.typekit.net/obx8btq.js"></script>
    <script>
        try {
            Typekit.load({
                async: true
            });
        } catch (e) {}

    </script>
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/jquerymobile.custom.min.js"></script>
    <script src="js/bootstrapold.min.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/css_browser_selector.js"></script>
    <script src="js/smooth-scroll.js"></script>
    <script src="js/contenthub_carousel.js"></script>
    <script src="js/jquery.jcarousel.js"></script>
    <script src="js/video_carousel.js"></script>
    <script src="js/tags-carosuel.js"></script>
    <script src="js/bootstrap-tagsinput.js"></script>
</head>

<body class="logged-in">
    <div id="alert">
        <div class="alert text-center" role="alert">
            <a href="#!" class="alert-link"></a>
            <button type="button" class="close"><span aria-hidden="true">&times;</span></button>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="alert-mesasage">
                            <p>new catalog</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="nav-wrapper">
        <nav class="navbar yamm navbar-default navbar-fixed-top hidden-xs" role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainMenu" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
                    <a class="navbar-brand" href="#!"><img src="images/logo.png"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="mainMenu">
                    <div class="navbar-right">
                        <ul class="list-inline nav-top">
                            <li><a href="#!">Login</a></li>
                            <li><a href="#!">Signup</a></li>
                            <li>
                                <a href="#!"><img src="images/icons/icon_shopping_cart.png" alt="Shop"></a>
                            </li>
                            <li class="dropdown social-block">
                                <a href="#!" class="social dropdown-toggle" data-toggle="dropdown">
                                    <img src="images/icons/icon_facebook.png" class="icon" alt="facebook">
                                    <img src="images/icons/icon_twitter.png" class="icon" alt="twitter">
                                    <img src="images/icons/icon_instagram.png" class="icon" alt="instagram">
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <div class="yamm-content">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="http://www.facebook.com/eOmega.org" class="btn facebook" target="_blank">
                                                                <i class="fa fa-facebook fa-fw"></i>Like us on Facebook
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="http://twitter.com/omega_institute" class="btn twitter" target="_blank">
                                                                <i class="fa fa-twitter fa-fw"></i>Follow @omega_institute
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="http://instagram.com/omegainstitute" class="btn instagram" target="_blank">
                                                                <i class="fa fa-instagram fa-fw"></i>Like us Instagram
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="http://www.youtube.com/user/TheOmegaInstitute" class="btn youtube" target="_blank">
                                                                <i class="fa fa-youtube fa-fw"></i>Follow us on YouTube
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="https://plus.google.com/u/0/117962827078682687225/posts" class="btn googleplus" target="_blank">
                                                                <i class="fa fa-google-plus fa-fw"></i>Follow us on Google Plus
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="http://www.linkedin.com/company/omega-institute" class="btn linkedin" target="_blank">
                                                                <i class="fa fa-linkedin fa-fw"></i>Follow us on LinkedIn
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="http://www.pinterest.com/OmegaInstitute/" class="btn pinterest" target="_blank">
                                                                <i class="fa fa-pinterest fa-fw"></i>Follow us on Pinterest
                                                            </a>
                                                        </li>
                                                        <li role="separator" class="divider"></li>
                                                        <li>
                                                            <form action="" method="POST" role="form" id="navNewsletter">
                                                                <legend>Get the Latest from Omega</legend>
                                                                <div class="form-group">
                                                                    <input type="email" class="form-control" id="navNewsEmail" name="navNewsEmail" placeholder="Enter your email address">
                                                                </div>
                                                                <div class="checkbox">

                                                                    <label for="getCatalog">
																	<input type="checkbox" value="" id="getCatalog" name="getCatalog">I'd like to receive a catalog </label>
                                                                </div>
                                                                <button type="submit" class="btn btn-primary disabled">Submit</button>
                                                            </form>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#!"><img src="images/icons/icon_email.png"></a>
                            </li>
                            <li>
                                <form class="navbar-form navbar-nav" id="search">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Search">
                                    </div>
                                    <span class="btn btn-default button"><i class="fa fa-search"></i></span>
                                </form>
                            </li>
                        </ul>
                        <ul class="nav-bottom list-inline">
                            <li class="dropdown yamm-fw">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Discover</a>
                                <ul class="dropdown-menu animated">
                                    <li>
                                        <div class="yamm-content container">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <h3 class="title">Content</h3>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <ul class="list-unstyled tow-col-list sublist">
                                                                <li><a href="#!">Explore Everything</a></li>
                                                                <li><a href="#!">Workshops &amp; Events</a></li>
                                                                <li><a href="#!">Online Learning</a></li>
                                                                <li><a href="#!">Ideas</a></li>
                                                                <li><a href="#!">People</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <h3 class="title">Topics</h3>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <ul class="list-unstyled tow-col-list sublist">
                                                                <li><a href="#!">Body, Mind &amp; Spirit</a></li>
                                                                <li><a href="#!">Creative Expresions</a></li>
                                                                <li><a href="#!">Leadership &amp; Work</a></li>
                                                                <li><a href="#!">Health &amp; Healing</a></li>
                                                                <li><a href="#!">Relationships &amp; Family</a></li>
                                                                <li><a href="#!">Sustainable Living</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown yamm-fw">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Engage</a>
                                <ul class="dropdown-menu animated">
                                    <li>
                                        <div class="yamm-content container">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <ul class="list-unstyled sublist">
                                                        <li><a href="#!">Center for Sustainable Living</a></li>
                                                        <li><a href="#!">Service Week</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-sm-3">
                                                    <ul class="list-unstyled sublist">
                                                        <li><a href="#!">Women's Leadership Center</a></li>
                                                        <li><a href="#!">Yoga Service </a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-sm-3">
                                                    <ul class="list-unstyled sublist">
                                                        <li><a href="#!">Veterans, Trauma &amp; Resilience</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-sm-3">
                                                    <ul class="list-unstyled sublist">
                                                        <li><a href="#!">Mindfulness</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown yamm-fw">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Visit</a>
                                <ul class="dropdown-menu animated">
                                    <li>
                                        <div class="yamm-content container">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <h3 class="title">Our main campus in rhinebeck, NY</h3>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <ul class="list-unstyled tow-col-list sublist">
                                                                <li><a href="#!">Getaway Retreats</a></li>
                                                                <li><a href="#!">Theme Weeks</a></li>
                                                                <li><a href="#!">Wellness Center</a></li>
                                                                <li><a href="#!">Plan Your Visit</a></li>
                                                                <li><a href="#!">Campus Location</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <h3 class="title">Locations</h3>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <ul class="list-unstyled tow-col-list sublist">
                                                                <li><a href="#!">Rhinebeck, NY</a></li>
                                                                <li><a href="#!">New York City</a></li>
                                                                <li><a href="#!">California</a></li>
                                                                <li><a href="#!">Costa Rica</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown yamm-fw">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Contribute</a>
                                <ul class="dropdown-menu animated">
                                    <li>
                                        <div class="yamm-content container">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <ul class="list-unstyled sublist">
                                                        <li><a href="#!">Make a Gift</a></li>
                                                        <li><a href="#!">Donor Events</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-sm-3">
                                                    <ul class="list-unstyled sublist">
                                                        <li><a href="#!">Why Give Today</a></li>
                                                        <li><a href="#!">Stweardship Council </a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-sm-3">
                                                    <ul class="list-unstyled sublist">
                                                        <li><a href="#!">membership Benefits</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-sm-3">
                                                    <ul class="list-unstyled sublist">
                                                        <li><a href="#!">Other Ways to Give</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown yamm-fw">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">About</a>
                                <ul class="dropdown-menu animated">
                                    <li>
                                        <div class="yamm-content container">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <ul class="list-unstyled sublist">
                                                        <li><a href="#!">Mission &amp; History</a></li>
                                                        <li><a href="#!">Local &amp; Global Partners</a></li>
                                                        <li><a href="#!">Our Capmus</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-sm-3">
                                                    <ul class="list-unstyled sublist">
                                                        <li><a href="#!">Why Omega Exists</a></li>
                                                        <li><a href="#!">Working at Omega </a></li>
                                                        <li><a href="#!">Contact </a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-sm-3">
                                                    <ul class="list-unstyled sublist">
                                                        <li><a href="#!">Leadership &amp; Board</a></li>
                                                        <li><a href="#!">Media Center</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-sm-3">
                                                    <ul class="list-unstyled sublist">
                                                        <li><a href="#!">Staff</a></li>
                                                        <li><a href="#!">Financial Information</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <!--	mobile menu - visible only on 320 - 767px-->
        <nav class="navbar yamm mobile navbar-default navbar-fixed-top visible-xs" role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainMenuMobile" aria-expanded="false">
					<i class="fa fa-search"></i>
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
                    <a class="navbar-brand" href="#!"><img src="images/logo.png"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="mainMenuMobile">
                    <div class="navbar-right">
                        <form class="navbar-form navbar-nav" id="search">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Search Omega">
                            </div>
                        </form>
                        <ul class="nav-top clearfix">
                            <li class="dropdown yamm-fw">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Discover</a>
                                <div class="dropdown-menu animated">
                                    <div class="yamm-content container">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <h3 class="title">Content</h3>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <ul class="list-unstyled tow-col-list sublist">
                                                            <li><a href="#!">Explore Everything</a></li>
                                                            <li><a href="#!">Workshops &amp; Events</a></li>
                                                            <li><a href="#!">Online Learning</a></li>
                                                            <li><a href="#!">Ideas</a></li>
                                                            <li><a href="#!">People</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <h3 class="title">Topics</h3>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <ul class="list-unstyled tow-col-list sublist">
                                                            <li><a href="#!">Body, Mind &amp; Spirit</a></li>
                                                            <li><a href="#!">Creative Expresions</a></li>
                                                            <li><a href="#!">Leadership &amp; Work</a></li>
                                                            <li><a href="#!">Health &amp; Healing</a></li>
                                                            <li><a href="#!">Relationships &amp; Family</a></li>
                                                            <li><a href="#!">Sustainable Living</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="dropdown yamm-fw">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Engage</a>
                                <div class="dropdown-menu animated">
                                    <div class="yamm-content container">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <ul class="list-unstyled sublist columns">
                                                    <li><a href="#!">Center for Sustainable Living</a></li>
                                                    <li><a href="#!">Veterans, Trauma &amp; Resilience</a></li>
                                                    <li><a href="#!">Service Week</a></li>
                                                    <li><a href="#!">Women's Leadership Center</a></li>
                                                    <li><a href="#!">Mindfulness</a></li>
                                                    <li><a href="#!">Yoga Service </a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    </dvi>
                            </li>
                            <li class="dropdown yamm-fw">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Visit</a>
                                <div class="dropdown-menu animated">
                                    <div class="yamm-content container">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <h3 class="title">Our main campus in rhinebeck, NY</h3>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <ul class="list-unstyled tow-col-list sublist">
                                                            <li><a href="#!">Getaway Retreats</a></li>
                                                            <li><a href="#!">Theme Weeks</a></li>
                                                            <li><a href="#!">Wellness Center</a></li>
                                                            <li><a href="#!">Plan Your Visit</a></li>
                                                            <li><a href="#!">Campus Location</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <h3 class="title">Locations</h3>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <ul class="list-unstyled tow-col-list sublist">
                                                            <li><a href="#!">Rhinebeck, NY</a></li>
                                                            <li><a href="#!">New York City</a></li>
                                                            <li><a href="#!">California</a></li>
                                                            <li><a href="#!">Costa Rica</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="dropdown yamm-fw">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Contribute</a>
                                <div class="dropdown-menu animated">
                                    <div class="yamm-content container">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <ul class="list-unstyled sublist columns">
                                                    <li><a href="#!">Make a Gift</a></li>
                                                    <li><a href="#!">Donor Events</a></li>
                                                    <li><a href="#!">Other Ways to Give</a></li>
                                                    <li><a href="#!">Why Give Today</a></li>
                                                    <li><a href="#!">Stweardship Council </a></li>
                                                    <li><a href="#!">Membership Benefits</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="dropdown yamm-fw">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">About</a>
                                <div class="dropdown-menu animated">
                                    <div class="yamm-content container">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <ul class="list-unstyled sublist columns">
                                                    <li><a href="#!">Mission &amp; History</a></li>
                                                    <li><a href="#!">Leadership &amp; Board</a></li>
                                                    <li><a href="#!">Local &amp; Global Partners</a></li>
                                                    <li><a href="#!">Media Center</a></li>
                                                    <li><a href="#!">Contact </a></li>
                                                    <li><a href="#!">Why Omega Exists</a></li>
                                                    <li><a href="#!">Staff</a></li>
                                                    <li><a href="#!">Working at Omega </a></li>
                                                    <li><a href="#!">Financial Information</a></li>
                                                    <li><a href="#!">Our Capmus</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <ul class="nav-middle list-inline clearfix">
                            <li class="login">
                                <a href="/google">Log In</a>
                            </li>
                            <li class="register">
                                <a href="/google">Sign Up</a>
                            </li>
                            <li class="catalog">
                                <a href="#!">Request a Catalog</a>
                            </li>
                            <li class="cart">
                                <a href="#!"><img src="images/icons/icon_cart_large.png" alt="Shop"></a>
                            </li>
                            <li class="email">
                                <a href="#!"><img src="images/icons/icon_email_large.png"></a>
                            </li>
                        </ul>
                        <div class="nav-bottom">
                            <div class="social-block text-left">
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="http://www.facebook.com/eOmega.org" class="btn facebook" target="_blank">
                                            <i class="fa fa-facebook fa-fw"></i>Like us on Facebook
                                        </a>
                                    </li>
                                    <li>
                                        <a href="http://twitter.com/omega_institute" class="btn twitter" target="_blank">
                                            <i class="fa fa-twitter fa-fw"></i>Follow @omega_institute
                                        </a>
                                    </li>
                                    <li>
                                        <a href="http://instagram.com/omegainstitute" class="btn instagram" target="_blank">
                                            <i class="fa fa-instagram fa-fw"></i>Like us Instagram
                                        </a>
                                    </li>
                                    <li>
                                        <a href="http://www.youtube.com/user/TheOmegaInstitute" class="btn youtube" target="_blank">
                                            <i class="fa fa-youtube fa-fw"></i>Follow us on YouTube
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://plus.google.com/u/0/117962827078682687225/posts" class="btn googleplus" target="_blank">
                                            <i class="fa fa-google-plus fa-fw"></i>Follow us on Google Plus
                                        </a>
                                    </li>
                                    <li>
                                        <a href="http://www.linkedin.com/company/omega-institute" class="btn linkedin" target="_blank">
                                            <i class="fa fa-linkedin fa-fw"></i>Follow us on LinkedIn
                                        </a>
                                    </li>
                                    <li>
                                        <a href="http://www.pinterest.com/OmegaInstitute/" class="btn pinterest" target="_blank">
                                            <i class="fa fa-pinterest fa-fw"></i>Follow us on Pinterest
                                        </a>
                                    </li>
                                </ul>

                            </div>
                            <form action="" method="POST" role="form" id="navNewsletter">
                                <legend>Get the Latest from Omega</legend>
                                <div class="form-group">
                                    <input type="email" class="form-control" id="navNewsEmail" name="navNewsEmail" placeholder="Enter your email address">
                                </div>
                                <div class="checkbox">

                                    <label for="getCatalogMobile">
									<input type="checkbox" value="" id="getCatalogMobile" name="getCatalog">I'd like to receive a catalog </label>
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                        </div>
                    </div>
                </div>
        </nav>
        </div>
        <div id="personal-pages" class="mmenu">
            <div class="backdrop"></div>
            <ul class="list-unstyled">
                <li><a href="article.php">Article</a></li>
                <li><a href="article-live.php">Article-live</a></li>
                <li><a href="audio.php">Audio</a></li>
                <li><a href="collections.php">Collections</a></li>
                <li><a href="video.php">Video</a></li>
                <li class="separator"></li>
                <li><a href="landing-page.php">Landingpage OWLC</a></li>
                <li><a href="ocsl.php">Landinpage OCSL</a></li>
                <li><a href="otherinitiative.php">Landingpage Other</a></li>
                <li><a href="fancy-page.php">OWLC inner</a></li>
                <li><a href="page-ocsl.php">OCSL inner</a></li>
                <li><a href="page-other.php">Other inner</a></li>
                <li><a href="person.php">Person page</a></li>
                <li><a href="press-release.php">Press Release</a></li>
                <li><a href="simple-page.php">Simple page</a></li>
                <li><a href="simplepage.php">Simple page (fancy-style)</a></li>
                <li><a href="workshop.php">Workshop</a></li>
                <li class="separator"></li>
                <li><a href="tipography.php">Tipography</a></li>
                <li><a href="tiles.php">Tiles</a></li>
                <li><a href="login.php">Login</a></li>
                <li><a href="register.php">Register</a></li>
                <li><a href="reset-password.php">Reset Password</a></li>
                <li><a href="sponsors.php">Sponsors</a></li>
                <li><a href="discover.php">Discover</a></li>
            </ul>
            <style>
                #personal-pages {
                    position: fixed;
                    top: 0;
                    left: 0;
                    z-index: 9000;
                    height: 100vh;
                    transition: all 0.4s ease;
                    width: 100vw;
                    pointer-events: none;
                }
                
                #personal-pages ul {
                    width: 150px;
                    height: 100%;
                    background: #fff;
                    left: -320px;
                    position: relative;
                    transition: all 0.3s ease;
                    padding: 10px 0px;
                    overflow-x: hidden;
                    overflow-y: scroll;
                    padding-top: 50px;
                }
                
                #personal-pages ul::-webkit-scrollbar {
                    display: none;
                }
                
                #personal-pages ul li {
                    border-top: 1px solid rgba(0, 0, 0, 0.02);
                    border-bottom: 1px solid rgba(0, 0, 0, 0.02);
                    margin-top: -1px;
                    position: relative;
                    display: block;
                }
                
                #personal-pages ul li.separator {
                    height: 3px;
                    background: rgba(0, 0, 0, 0.2);
                }
                
                #personal-pages ul li a {
                    display: block;
                    line-height: 25px;
                    padding: 0 5px;
                    font-size: 12px;
                    transition: all 0.3s ease;
                    animation-delay: 0.3s;
                    pointer-events: all;
                }
                
                #personal-pages ul li a:hover {
                    background: #2AD0CE;
                    color: #fff;
                }
                
                #personal-pages.open ul {
                    left: 0;
                    box-shadow: 5px 0 5px 0px rgba(0, 0, 0, 0.5);
                }
                
                #personal-pages .backdrop {
                    position: absolute;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                    background: rgba(0, 0, 0, 0.5);
                    opacity: 0;
                    transition: all 0.2s ease;
                    pointer-events: none;
                }
                
                #personal-pages.open .backdrop {
                    opacity: 1;
                    animation-delay: 300ms;
                }
                
                body.blur {
                    overflow: hidden;
                    padding-right: 17px;
                }
                
                body.blur>* {
                    -webkit-filter: blur(10px);
                }
                
                body.blur #personal-pages {
                    -webkit-filter: blur(0px);
                }

            </style>
            <script>
                var windowWidth = $(window).width();
                $(".navbar-brand").on("click", function() {
                    $("#personal-pages").toggleClass("open");
                    $("body").toggleClass("blur");
                });
                $("#personal-pages").on("click", function() {
                    $("#personal-pages").removeClass("open");
                    $("body").removeClass("blur");
                });

            </script>
        </div>
