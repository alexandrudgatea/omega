<?php include ("templates/header.php"); ?>
<section id="block-system-main" class="block block-system clearfix" style="min-height: 558px;">


	<div class="container">
		<div class="view view-search view-id-search view-display-id-search_page container view-dom-id-fd4f584b3d008994090033b6b3adb7f6 everything">
			<div class="searchZone">
				<i class="fa fa-search" aria-hidden="true"></i> <input type="text" form="views-exposed-form-search-search-page" value="" name="title" placeholder="Search"></div>
		</div>
		<div class="view-header">
			1 - 10 of 4292 Results </div>

		<div class="view-filters">
			<form action="/search" method="get" id="views-exposed-form-search-search-page" accept-charset="UTF-8">
				<div>
					<div class="row">
						<div class="col-sm-12 lp-tabs">
							<ul class="nav nav-tabs" role="tablist">
								<li role="presentation" class=" active ">
									<a href="/search"> Everything </a>
								</li>
								<li role="presentation" class="">
									<a href="/search?type[]=workshop">Workshop &amp; Events</a>
								</li>
								<li role="presentation" class="">
									<a href="/search?type[]=online_workshop">Online Learning</a>
								</li>
								<li role="presentation" class="">
									<a href="/search?type[1]=article&amp;type[2]=video">Ideas</a>
								</li>
								<li role="presentation" class="">
									<a href="/search?type[]=person">People</a>
								</li>
								<li role="presentation" class="">
									<a href="/search?type[]=page">Information</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="hidden">
						<div class="form-item form-item-type form-type-select form-group"><select multiple="multiple" name="type[]" class="form-control form-select" id="edit-type" size="6"><option value="article">Article</option><option value="online_workshop">Online Workshop</option><option value="page">Page</option><option value="person">Person</option><option value="video">Video</option><option value="workshop">Workshop</option></select></div>
					</div>
				</div>
			</form>
		</div>

		<div class="view-content">
			<ul>

				<li class="search-tile">
					<div class="tile-image">
						<img src="http://test2.eomega.org/sites/all/themes/omeganew/images/panel_idea.jpg" class="img-responsive">
					</div>
					<div class="tile-content">
						<div class="content-type">
							<span class="maybeShow">Online Learning</span>
						</div>
						<h2 class="title interractive-text"><a href="online-workshops/loving-what-is"> Loving What Is </a></h2>
						<span class="tile-text block">
					</span>

						<div class="bottom-info">
							<div class="featured">
								Jun 02 - Jun 02 2017 | Online Workshop </div>
						</div>
					</div>
				</li>


				<li class="search-tile">
					<div class="tile-image">
						<img src="http://test2.eomega.org/sites/all/themes/omeganew/images/panel_idea.jpg" class="img-responsive">
					</div>
					<div class="tile-content">
						<div class="content-type">
							<span class="maybeShow">Online Learning</span>
						</div>
						<h2 class="title interractive-text"><a href="online-workshops/being-fearless-live-online"> Being Fearless Live Online </a></h2>
						<span class="tile-text block">
			LIVE STREAM REGISTRATION INFORMATION COMING SOON

As we look to understand our way forward, it is difficult to find common ground when positions have become so oppositional and hardened. As our ability to hear each other is compressed into the 140 characters of a tweet, and news has become spin—how do we find ways to enter profound and necessary dialogue to address the urgent questions before us?

This critical conference, guided by an extraordinary group of courageous and insightful teachers, thinkers, and community leaders, brings together innovative thought, deeply felt activism, and contemplative wisdom as a foundation to navigate the times in which we live. Through stories from the front lines of change and panel discussions, along with interactive sessions, engaging conversati...        </span>

						<div class="bottom-info">
							<div class="featured">
								Oct 13 - Oct 13 2017 | Online Workshop </div>
						</div>
					</div>
				</li>


				<li class="search-tile">
					<div class="tile-image">
						<img src="http://styles/search_tile_focus__260x250_/http/test2.eomega.org/sites/default/files/images/headshots/bennett_jacalyn_17_web.jpg?itok=C3IsAT1f" class="img-responsive">
					</div>
					<div class="tile-content">
						<div class="content-type">
							<span class="maybeShow">Teacher</span>
						</div>
						<h2 class="title interractive-text"><a href="workshops/teachers/jacalyn-bennett"> Jacalyn Bennett </a></h2>
						<span class="tile-text block">
					</span>

						<div class="bottom-info">
							<div class="featured">

							</div>
						</div>
					</div>
				</li>
				<li class="search-tile">
					<div class="tile-image">
						<img src="http://styles/search_tile_focus__260x250_/http/test2.eomega.org/sites/default/files/remove_this_image_20.png?itok=YeNCFwm7" class="img-responsive">
					</div>
					<div class="tile-content">
						<div class="content-type">
							<span class="maybeShow">Information</span>
						</div>
						<h2 class="title interractive-text"><a href="board-of-directors"> Board of Directors </a></h2>
						<span class="tile-text block">

	David Orlinsky
	Chair
	After spending the first 15 years of his career as a practicing attorney, David Orlinsky cofounded NetJets, Inc., which was the largest private aviation company in the world before being acquired...
	Read More



	&nbsp;



	Jacalyn Bennett
	Jacalyn Bennett, a member of Omega's Board of Directors, is a trained artist and the founding president of Bennett and Company, a fashion brand that embraces Gandhian principles, ecology, human rights, and social consciousness. She...
	Read More



	&nbsp;



	Katherine Collins
	Katherine Collins is author of&nbsp;The Nature of Investing&nbsp;and a member of Omega's board of directors. As founder and chief executive officer of Honeybee Capital, she strives to reintegrate...
	Read More
	&nbsp;



...        </span>

						<div class="bottom-info">
							<div class="featured">

							</div>
						</div>
					</div>
				</li>


				<li class="search-tile">
					<div class="tile-image">
						<img src="http://styles/search_tile_focus__260x250_/http/test2.eomega.org/sites/default/files/remove_this_image_13.png?itok=wdM2kNPC" class="img-responsive">
					</div>
					<div class="tile-content">
						<div class="content-type">
							<span class="maybeShow">Information</span>
						</div>
						<h2 class="title interractive-text"><a href="mission-history-values"> Mission, History &amp; Values </a></h2>
						<span class="tile-text block">
					</span>

						<div class="bottom-info">
							<div class="featured">

							</div>
						</div>
					</div>
				</li>


				<li class="search-tile">
					<div class="tile-image">
						<img src="http://styles/search_tile_focus__260x250_/http/test2.eomega.org/sites/default/files/remove_this_image_8.png?itok=4oixoubE" class="img-responsive">
					</div>
					<div class="tile-content">
						<div class="content-type">
							<span class="maybeShow">Information</span>
						</div>
						<h2 class="title interractive-text"><a href="about"> About </a></h2>
						<span class="tile-text block">

	Omega is a nonprofit, mission-driven, and donor-supported educational organization. For more than 40 years we’ve been a pioneer in holistic studies – helping people and organizations integrate personal growth and social change, moving beyond ‘the way it is’ toward ‘the way it can be’.



	Considered a premiere travel destination in New York’s Hudson Valley, Omega has seen more than one million people come through our doors to grow, learn, and find a greater sense of purpose. Annually, more than 23,000 people attend Omega’s 350+ programs in-person, and close to 2 million people participate in our offering online.&nbsp;



	More than simply a place, Omega is a global community that awakens the best in the human spirit and cultivates the extraordinary potential that exists in us...        </span>

						<div class="bottom-info">
							<div class="featured">

							</div>
						</div>
					</div>
				</li>


				<li class="search-tile">
					<div class="tile-image">
						<img src="http://styles/search_tile_focus__260x250_/http/test2.eomega.org/sites/default/files/remove_this_image_4.png?itok=Hrhhk_3H" class="img-responsive">
					</div>
					<div class="tile-content">
						<div class="content-type">
							<span class="maybeShow">Information</span>
						</div>
						<h2 class="title interractive-text"><a href="our-commitment-to-nonprofit-retreats"> Our Commitment to Nonprofit Retreats </a></h2>
						<span class="tile-text block">

	Our peer nonprofits are working hard—daily—to improve the well-being of communities and the lives of countless people. A shared vision of a more peaceful, just, and sustainable world calls us to contribute what we can in support of one another's efforts. This is an essential part of Omega’s mission to awaken the best in the human spirit and cultivate the extraordinary potential in us all.
	&nbsp;
	To aligned changemakers, Omega offers space for centering, opportunities for connection, practices for resilience, and platforms to amplify our voices together. Working together, we develop mutually supportive networks of leaders, strengthen our communities, engage the whole range of perspectives we need, and create hope for and progress toward the future we envision.

		</span>

						<div class="bottom-info">
							<div class="featured">

							</div>
						</div>
					</div>
				</li>


				<li class="search-tile">
					<div class="tile-image">
						<img src="http://styles/search_tile_focus__260x250_/http/test2.eomega.org/sites/default/files/gy20160601_0804_copy.jpg?itok=p9Cf8wqO" class="img-responsive">
					</div>
					<div class="tile-content">
						<div class="content-type">
							<span class="maybeShow">Information</span>
						</div>
						<h2 class="title interractive-text"><a href="what-are-omega-nonprofit-retreats"> What Are Omega Nonprofit Retreats </a></h2>
						<span class="tile-text block">

	Nonprofit Retreats at Omega began in 2005.* The goal is to support peer organizations actively working toward needed changes in the world and strengthen networks of change leaders connected to Omega and each other.
	&nbsp;
	For more than a dozen years, organizations—large and small—have brought their teams of staff, board members, volunteers and constituents to Omega to recharge and reflect, strategize and plan, network and collaborate, so they can return to their efforts rejuvenated, with greater connection and increased clarity.
	&nbsp;
	When multiple nonprofits are on campus at the same time, Omega provides opportunities to gather with and learn from other participating organizations. Each invited organization creates its own self-led working retreat, using the facilities and ...        </span>

						<div class="bottom-info">
							<div class="featured">

							</div>
						</div>
					</div>
				</li>


				<li class="search-tile">
					<div class="tile-image">
						<img src="http://styles/search_tile_focus__260x250_/http/test2.eomega.org/sites/default/files/remove_this_image_12.png?itok=vlF45jyv" class="img-responsive">
					</div>
					<div class="tile-content">
						<div class="content-type">
							<span class="maybeShow">Information</span>
						</div>
						<h2 class="title interractive-text"><a href="our-commitment-to-scholarships"> Our Commitment to Scholarships </a></h2>
						<span class="tile-text block">

	Omega believes that the benefits of a holistic education should be available to everyone. We strive to bring many voices, perspectives, backgrounds, identities, and life experiences to Omega. The Omega Scholarship Program is one of the ways we demonstrate our commitment to building a holistic learning community, increasing our responsiveness to barriers that exist, and creating greater access to the educational opportunities offered by Omega.

		</span>

						<div class="bottom-info">
							<div class="featured">

							</div>
						</div>
					</div>
				</li>


				<li class="search-tile">
					<div class="tile-image">
						<img src="http://styles/search_tile_focus__260x250_/http/test2.eomega.org/sites/default/files/gy101015_573_copy.jpg?itok=QZAXM079" class="img-responsive">
					</div>
					<div class="tile-content">
						<div class="content-type">
							<span class="maybeShow">Information</span>
						</div>
						<h2 class="title interractive-text"><a href="what-is-the-omega-scholarship-program"> What is the Omega Scholarship Program? </a></h2>
						<span class="tile-text block">

	Each year, hundreds of people are supported by full and partial scholarships to share their perspectives and experience with the Omega learning community, participating in a wide range of workshops, retreats, and conferences. Omega is grateful for the support of generous individuals, foundations, and organizations, [currently directs to: https://www.eomega.org/donate ] enabling us to grow the size and impact of the Omega Scholarship Program.&nbsp;



	Scholarship eligibility, criteria, and application deadlines vary, so please review each scholarship description [currently directs to: https://www.eomega.org/omega-scholarships/scholarship-opportunities ] to find the one that best fits your needs.

		</span>

						<div class="bottom-info">
							<div class="featured">

							</div>
						</div>
					</div>
				</li>

			</ul>
		</div>

		<div class="text-center">
			<ul class="pagination">
				<li class="active"><span>1</span></li>
				<li><a title="Go to page 2" href="/search?&amp;title=&amp;page=1">2</a></li>
				<li><a title="Go to page 3" href="/search?&amp;title=&amp;page=2">3</a></li>
				<li><a title="Go to page 4" href="/search?&amp;title=&amp;page=3">4</a></li>
				<li><a title="Go to page 5" href="/search?&amp;title=&amp;page=4">5</a></li>
				<li><a title="Go to page 6" href="/search?&amp;title=&amp;page=5">6</a></li>
				<li><a title="Go to page 7" href="/search?&amp;title=&amp;page=6">7</a></li>
				<li><a title="Go to page 8" href="/search?&amp;title=&amp;page=7">8</a></li>
				<li><a title="Go to page 9" href="/search?&amp;title=&amp;page=8">9</a></li>
				<li class="pager-ellipsis disabled"><span>…</span></li>
				<li class="next"><a title="Go to next page" href="/search?&amp;title=&amp;page=1">next ›</a></li>
				<li class="pager-last"><a title="Go to last page" href="/search?&amp;title=&amp;page=429">last »</a></li>
			</ul>
		</div>

	</div>
</section>
<?php include ("templates/footer.php"); ?>
