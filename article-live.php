<?php include ("templates/header.php"); ?>

<section id="block-system-main" class="block block-system clearfix">


  <div id="hero" class="article background-image" style="background: #2AD0CE url('http://test2.eomega.org/sites/all/themes/omeganew/images/hero_article.jpg')">
	<div class="container full-height">
		<div class="row">
			<div class="col-sm-6 idea-bg transparent">
				<div class="article-info field-text">
					<h4 class="article-category"></h4>

					<h1 class="article-title">Maximize Impact With Matching Gifts</h1>
											<span class="article-subtitle"></span>
																								</div>
			</div>
		</div>
	</div>
</div>
	<div id="mainContent" class="article">
		<div class="container">
			<div class="row">
				<div class="col-sm-3 sidebar">
											<ul class="article-info list-unstyled">
							<li>
							<span class="article-author block">
															</span>
								<span class="article-date block">Mar 14, 2017</span>
															</li>

						</ul>

					<!--                        <ul class="article-info list-unstyled">-->
<!--                            <li class="article-share">-->
<!--                                --><!--                                <a href="#!" class="btn cta icon-button rounded-corners add-fav"><i-->
<!--                                        class="fa fa-bookmark icon"></i>Add to favorites</a>-->
<!--                            </li>-->
<!--                        </ul>-->
						<div class="future-events tiles-grid hidden-xs">
													</div>
									</div>
				<div class="col-sm-9 no-padding">
					<div class="article-body">

													<span class="article-text">
								<p>
	Michael Murphy attended his first Omega workshop—a yoga retreat with Beryl Bender Birch—in the early 1980s. About a decade later, he and his wife began thinking about moving from their home in New York City. “I immediately thought of getting land near Omega,” he said. “Luckily, we found a 5-acre plot right down the road!”<br>
	&nbsp;<br>
	Michael has worked for more than twenty years as the director of analytics for American Express. He is also a yoga teacher and a student of contemplative practice. “Omega’s deep and rich programs and community have been tremendous resources in my life. Having the energy and the vitality of the campus right next door serves as a vital well-spring of value,” he said.<br>
	&nbsp;<br>
	Michael feels philanthropy is an important way to support organizations in line with his values, he says, “because I know they depend on donations to sustain their operations and add resources for future development.” He said he is “fortunate and proud” to work for a company that matches employee charitable gifts. “By literally doubling my donation, it makes my gift have twice the impact.”
</p>

<p>
	<strong>To find out if your employer matches donations, just check with your human resources department or call us at 845.266.4444, ext. 405.</strong>
</p>
							</span>






						<!-- Sponsors and Partners -->

					</div>
				</div>
			</div>
		</div>
	</div>

</section>

<?php include ("templates/footer.php"); ?>
