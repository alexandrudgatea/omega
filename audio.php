<?php include ("templates/header.php"); ?>
	<div id="hero" class="audio background-image" style="background: url('/images/hero_audio.jpg')">
		<div class="container full-height">
			<div class="row">
				<div class="col-sm-6 special-bg transparent">
					<div class="article-info field-text">
						<h4 class="article-category ">Omega Women’s Leadership Center</h4>
						<h1 class="article-title ">Descended From Dreamers</h1> <span class="article-subtitle">A Poem by Li-Young Lee</span>
						<div class="audio-player">
							<span class="audio-info-text">Featured on PBS News Hour</span>
							<audio id="audio-player" src="media/demo.mp3" type="audio/mp3" controls="controls"></audio>
						</div>
						<div id="audio-timers">
							<span id="duration" class="pull-left"></span>
							<span id="timeleft" class="pull-right"></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="mainContent" class="audio">
		<div class="container">
			<div class="row">
				<div class="col-sm-3 sidebar">
					<ul class="article-info list-unstyled">
						<li> <span class="article-author block">Featuring Li-Young Lee</span>
							<span class="article-date block">May 03, 2015</span>
						</li>
						<li class="article-share">
							<ul class="list-inline social-links">
								<li>
									<a href="#!">
										<span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x teal"></i>
									<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
								</span>
									</a>
								</li>
								<li>
									<a href="#!"> <span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x teal"></i>
									<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
								</span> </a>
								</li>
								<li>
									<a href="#!"> <span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x teal"></i>
									<i class="fa fa-envelope-o fa-stack-1x fa-inverse"></i>
								</span> </a>
								</li>
								<li>
									<a href="#!"> <span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x teal"></i>
									<i class="fa fa-share-alt fa-stack-1x fa-inverse"></i>
								</span> </a>
								</li>
							</ul> <a href="#!" class="btn cta icon-button rounded-corners"><i class="fa fa-bookmark icon"></i>See Another</a> </li>
					</ul>
					<ul class="future-events list-unstyled">
						<li>
							<div class="event-type">Workshop</div>
							<h3 class="event-title">Growing and Sustaining A Yoga Service Orginzation</h3> <span class="event-participants">Ania Nussbaum, Grenadine Wright, Rene Zenllenger, and more</span> <span class="event-date">Jun 12 – Jun 18 2016</span> </li>
						<li>
							<div class="event-type">Conference</div>
							<h3 class="event-title">Transformational Cleaning for the Body and Soul</h3> <span class="event-participants">Ania Nussbaum and Grenadine Wright</span> <span class="event-date">Feb 28 – Mar 06 2016</span> </li>
						<li>
							<div class="event-type">Workshop</div>
							<h3 class="event-title">Collaborating with Nature</h3> <span class="event-participants">Peter Parker and Grenadine Wright</span> <span class="event-date">Jun 12 – Jun 18 2016</span> </li>
					</ul>
				</div>
				<div class="col-sm-9 no-padding">
					<div class="article-body clearfix">
						<div class="quote-section pull-right">
							<div class="quote-icon">&lsquo;&lsquo; </div>
							<h2 class="quote">You must have chaos within you to give birth to a dancing star.</h2> <span class="quote-author block">Friedrich Nietzsche</span> <span class="quote-author-profession block">Philosopher, cultural critic, and poet</span>
							<ul class="list-inline social-links">
								<li>
									<a href="#!"> <span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x teal"></i>
									<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
								</span> </a>
								</li>
								<li>
									<a href="#!"> <span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x teal"></i>
									<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
								</span> </a>
								</li>
							</ul>
						</div> <span class="article-text">
						Even if there were a day of observance set aside for the appreciation of friends, it seems like one day would not be enough to convey the importance and distinction that friends bring to our lives. They are our sounding boards, our support systems when family fails us or simply
						becomes too challenging, and they provide invaluable perspective and reflection that helps us make sense of our. 
						</span>
						<div class="discover-more">
							<h1 class="section-title">Discover More</h1>
							<div class="tiles-grid">
								<div class="row">
									<div class="col-sm-6">
										<div class="panel panel-default panel-idea tile">
											<div class="favourite">
												<div class="checkbox">
													<input type="checkbox" value="" id="favourite1">
													<label for="favourite1"></label>
												</div>
											</div>
											<div class="panel-heading" style="background: url('images/panel_idea.jpg')"> </div>
											<div class="panel-body panel-audio-video">
												<div class="content-type"> <a href="#!"><i class="fa fa-play-circle-o"></i></a> <span>Audio</span> </div>
												<div class="title">
													<h1>Bobby McFerrin: Circle Songs from Omega</h1> </div>
												<div class="person">
													<ul class="by list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
													<ul class="featuring list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
												</div>
												<div class="duration-date"> <em class="duration">13 minute watch</em> <em class="date-release">13th of September, 2016</em> </div>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="panel panel-default panel-idea tile">
											<div class="favourite">
												<div class="checkbox">
													<input type="checkbox" value="" id="favourite1">
													<label for="favourite1"></label>
												</div>
											</div>
											<div class="panel-heading" style="background: url('images/panel_idea.jpg')"> </div>
											<div class="panel-body panel-audio-video">
												<div class="content-type"> <a href="#!"><i class="fa fa-play-circle-o"></i></a> <span>Audio</span> </div>
												<div class="title">
													<h1>Bobby McFerrin: Circle Songs from Omega</h1> </div>
												<div class="person">
													<ul class="by list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
													<ul class="featuring list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
												</div>
												<div class="duration-date"> <em class="duration">13 minute watch</em> <em class="date-release">13th of September, 2016</em> </div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="panel panel-default panel-idea tile">
											<div class="favourite">
												<div class="checkbox">
													<input type="checkbox" value="" id="favourite1">
													<label for="favourite1"></label>
												</div>
											</div>
											<div class="panel-heading" style="background: url('images/panel_idea.jpg')"> </div>
											<div class="panel-body panel-audio-video">
												<div class="content-type"> <a href="#!"><i class="fa fa-play-circle-o"></i></a> <span>Audio</span> </div>
												<div class="title">
													<h1>Bobby McFerrin: Circle Songs from Omega</h1> </div>
												<div class="person">
													<ul class="by list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
													<ul class="featuring list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
												</div>
												<div class="duration-date"> <em class="duration">13 minute watch</em> <em class="date-release">13th of September, 2016</em> </div>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="panel panel-default panel-idea tile">
											<div class="favourite">
												<div class="checkbox">
													<input type="checkbox" value="" id="favourite1">
													<label for="favourite1"></label>
												</div>
											</div>
											<div class="panel-heading" style="background: url('images/panel_idea.jpg')"> </div>
											<div class="panel-body panel-audio-video">
												<div class="content-type"> <a href="#!"><i class="fa fa-play-circle-o"></i></a> <span>Audio</span> </div>
												<div class="title">
													<h1>Bobby McFerrin: Circle Songs from Omega</h1> </div>
												<div class="person">
													<ul class="by list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
													<ul class="featuring list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
												</div>
												<div class="duration-date"> <em class="duration">13 minute watch</em> <em class="date-release">13th of September, 2016</em> </div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="panel panel-default panel-idea tile">
											<div class="favourite">
												<div class="checkbox">
													<input type="checkbox" value="" id="favourite1">
													<label for="favourite1"></label>
												</div>
											</div>
											<div class="panel-heading" style="background: url('images/panel_idea.jpg')"> </div>
											<div class="panel-body panel-audio-video">
												<div class="content-type"> <a href="#!"><i class="fa fa-play-circle-o"></i></a> <span>Audio</span> </div>
												<div class="title">
													<h1>Bobby McFerrin: Circle Songs from Omega</h1> </div>
												<div class="person">
													<ul class="by list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
													<ul class="featuring list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
												</div>
												<div class="duration-date"> <em class="duration">13 minute watch</em> <em class="date-release">13th of September, 2016</em> </div>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="panel panel-default panel-idea tile">
											<div class="favourite">
												<div class="checkbox">
													<input type="checkbox" value="" id="favourite1">
													<label for="favourite1"></label>
												</div>
											</div>
											<div class="panel-heading" style="background: url('images/panel_idea.jpg')"> </div>
											<div class="panel-body panel-audio-video">
												<div class="content-type"> <a href="#!"><i class="fa fa-play-circle-o"></i></a> <span>Audio</span> </div>
												<div class="title">
													<h1>Bobby McFerrin: Circle Songs from Omega</h1> </div>
												<div class="person">
													<ul class="by list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
													<ul class="featuring list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
												</div>
												<div class="duration-date"> <em class="duration">13 minute watch</em> <em class="date-release">13th of September, 2016</em> </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>	

	<div id="footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<a href="#!"><img src="http://test2.eomega.org/sites/all/themes/omeganew/images/logo-inverted.png" class="img-responsive footer-logo"></a>
				</div>
				<div class="col-sm-6">
					<form class="webform-client-form webform-client-form-15421" action="/" method="post" id="webform-client-form-15421" accept-charset="UTF-8">
						<div>
							<div class="form-item webform-component webform-component-email webform-component--email form-group form-item form-item-submitted-email form-type-webform-email form-group">
								<label class="control-label" for="edit-submitted-email--2">Newsletter Sign-up</label>
								<input placeholder="you@email.com" class="form-control form-text form-email" type="email" id="edit-submitted-email--2" name="submitted[email]" size="60">
							</div>
							<input type="hidden" name="details[sid]">
							<input type="hidden" name="details[page_num]" value="1">
							<input type="hidden" name="details[page_count]" value="1">
							<input type="hidden" name="details[finished]" value="0">
							<input type="hidden" name="form_build_id" value="form-4qUZhbKON7FYMwc9m7pkRorerEGr8wY3fuMtm9YN2Qs">
							<input type="hidden" name="form_id" value="webform_client_form_15421">
							<input type="hidden" name="honeypot_time" value="1487321887|97xjRvbpB9cPO92bSrU5qm4gckWRvhiKniA3MdZ7zYs">
							<div class="user_email_address_confirm-textfield">

								<div class="form-item form-item-user-email-address-confirm form-type-textfield form-group">
									<label class="control-label" for="edit-user-email-address-confirm--2">Leave this field blank</label>
									<input autocomplete="off" class="form-control form-text" type="text" id="edit-user-email-address-confirm--2" name="user_email_address_confirm" value="" size="20" maxlength="128">
								</div>
							</div>
							<div class="form-actions">
								<button class="webform-submit button-primary btn btn-default form-submit" id="edit-webform-ajax-submit-15421" type="submit" name="op" value="Sign Up">Sign Up</button>

							</div>

						</div>

					</form>
				</div>
				<div class="col-sm-3">

					<ul class="list-inline social-links">
						<li>
							<a id="facebook-share-footer" style="cursor: pointer;">
								<span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x teal"></i>
                    <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                </span>
							</a>
						</li>
						<li>
							<a id="twitter-share-footer">
								<span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x teal"></i>
                    <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                </span>
							</a>
						</li>
						<li>
							<a href="mailto:?subject=&amp;body=Read%20more%20">
								<span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x teal"></i>
                    <i class="fa fa-envelope-o fa-stack-1x fa-inverse"></i>
                </span>
							</a>
						</li>
						<li>
							<a href="#!">
								<span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x teal"></i>
                    <i class="fa fa-share-alt fa-stack-1x fa-inverse"></i>
                </span>
							</a>
						</li>
					</ul>

					<script type="text/javascript">
						jQuery(document).ready(function ($) {
							$("#facebook-share-footer").on('click', function () {

								link = 'http://test2.eomega.org/';
								image = '';
								title = 'OMEGA';

								FB.ui({
									method: 'share',
									href: link,
									picture: image,
									title: title
								}, function (response) {});
							});

							$("#twitter-share-footer").attr("href", "http://www.twitter.com/share?url=http://test2.eomega.org/&text=OMEGA").attr("target", "_blank");
						});
					</script>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6">
					<ul class="list-inline footer-menu field-text">
						<li><a href="#!" class="field-text">Privacy Policy</a></li>
						<li><a href="#!" class="field-text">Terms of Use</a></li>
						<li><a href="#!" class="field-text">Contact</a></li>
					</ul>
				</div>
				<div class="col-sm-3">
					<ul class="list-inline copyright pull-right">
						<li>
							<span class="block field-text">©2015 Omega<br>Institute</span>
						</li>
						<li>
							<span class="block field-text">                            <br></span>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<script src="js/jquery-2.1.4.min.js"></script>
	<script src="js/jquerymobile.custom.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/mediaelement-and-player.min.js"></script>
	<script src="js/audioplayer.js"></script>
	<script src="js/custom.js"></script>
	<script src="js/smooth-scroll.js"></script>
	<script>
		$(document).ready(function () {
			$('#audio-player').mediaelementplayer({
				alwaysShowControls: true,
				features: ['playpause', 'progress'],
				audioVolume: 'horizontal'
			});
		});
	</script>
</body>

</html>