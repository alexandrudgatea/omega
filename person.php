<?php include ("templates/header.php"); ?>

	<div id="mainContent" class="person">
		<div class="person-bio">
			<div class="container">
				<div class="row">
					<div class="col-xs-4">
						<div class="person-image" style="background-image: url('images/person_pema.jpg')"></div>
						<div class="person-website">
							<h1 class="visible-xs">Pema Chödrön</h1>
							<a href="" target="_blank">www.pemachodronfoundation.org</a>
						</div>
					</div>
					<div class="col-xs-8">
						<div class="bio">
							<h1 class="hidden-xs">Pema Chödrön</h1>
							<p>A leading exponent of teachings on meditation and how they apply to everyday life, Pema Chödrön is widely known for her insightful, down-to-earth interpretation of Tibetan Buddhism for Western audiences. </p>

							<p>Chödrön is the resident teacher at Gampo Abbey in Cape Breton, Nova Scotia, the first Tibetan monastery for Westerners. An American Buddhist nun, she began studying Buddhism in the early 1970s, working closely with the renowned Chögyam Trungpa Rinpoche of the Shambhala Buddhist tradition until his death in 1987. She is currently a student of Dzigar Kongtrul Rinpoche. From years of study and monastic training, she addresses complex issues with a clarity that bespeaks the fruits of her practice.</p>

							<p>Chödrön is interested in helping establish Tibetan Buddhist monastacism in the West, as well as continuing her work with Western Buddhists of all traditions, sharing ideas and teachings. She is the author of numerous books and audiobooks, including When Things Fall Apart; The Places That Scare You; The Wisdom of No Escape; Getting Unstuck; Start Where You Are; The Pema Chödrön Audio Collection; Comfortable With Uncertainty; No Time to Lose; Always Maintain a Joyful Mind (lojong teachings); Practicing Peace in Times of War; Living Beautifully With Uncertainty and Change; and How To Meditate.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tiles-grid hidden-xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h1 class="section-title interractive-text">Pema Chödrön at Omega</h1>
					</div>
				</div>
				<div class="tiles-row row">
					<div class="col-sm-12 headline-col">
						<div class="row">
							<div class="col-sm-12 col-md-6">
								<h2 class="headline-title">Workshops &amp; Events</h2>
							</div>
							<div class="col-sm-12 col-md-6">
								<a href="#!" class="btn see-all pull-right">See all</a>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="row">
							<div class="col-sm-4">
								<div class="panel panel-default panel-workshop tile">
									<div class="favourite">
										<div class="checkbox">
											<input type="checkbox" value="" id="favourite4">
											<label for="favourite4"></label>
										</div>
									</div>
									<div class="panel-body">
										<div class="content-type">
											<span>Workshop</span>
										</div>
										<div class="title">
											<h1>Collaborating With Nature</h1>
										</div>
										<div class="person">
											<ul class="list-unstyled list-inline person-images">
												<li>
													<img src="images/person.jpg">
												</li>
												<li> <img src="images/person.jpg"></li>
											</ul>
											<ul class="list-unstyled list-inline person-names">
												<li>Thomas Robertson</li>
												<li>Marry Anderson</li>
											</ul>
										</div>
										<div class="date-location">
											<ul class="list-inline">
												<li><em>Feb 28 - Mar 06 2016</em></li>
												<li><em>Rhinebeck, NY</em></li>
											</ul>
										</div>
									</div>
									<div class="panel-footer">
										<a href="#!" class="expand-button">
											<i class="fa fa-plus"></i>
										</a>
										<div class="expanded-tile-content animated fadeInUp">
											<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
											<a href="#!" class="btn white-transparent-button">Learn More</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="panel panel-default panel-workshop tile">
									<div class="favourite">
										<div class="checkbox">
											<input type="checkbox" value="" id="favourite5">
											<label for="favourite5"></label>
										</div>
									</div>
									<div class="panel-body">
										<div class="content-type">
											<span>Workshop</span>
										</div>
										<div class="title">
											<h1>Collaborating With Nature</h1>
										</div>
										<div class="person">
											<ul class="list-unstyled list-inline person-images">
												<li>
													<img src="images/person.jpg">
												</li>
												<li> <img src="images/person.jpg"></li>
											</ul>
											<ul class="list-unstyled list-inline person-names">
												<li>Thomas Robertson</li>
												<li>Marry Anderson</li>
											</ul>
										</div>
										<div class="date-location">
											<ul class="list-inline">
												<li><em>Feb 28 - Mar 06 2016</em></li>
												<li><em>Rhinebeck, NY</em></li>
											</ul>
										</div>
									</div>
									<div class="panel-footer">
										<a href="#!" class="expand-button">
											<i class="fa fa-plus"></i>
										</a>
										<div class="expanded-tile-content animated fadeInUp">
											<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
											<a href="#!" class="btn white-transparent-button">Learn More</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="panel panel-default panel-workshop tile">
									<div class="favourite">
										<div class="checkbox">
											<input type="checkbox" value="" id="favourite6">
											<label for="favourite6"></label>
										</div>
									</div>
									<div class="panel-body">
										<div class="content-type">
											<span>Workshop</span>
										</div>
										<div class="title">
											<h1>Collaborating With Nature</h1>
										</div>
										<div class="person">
											<ul class="list-unstyled list-inline person-images">
												<li>
													<img src="images/person.jpg">
												</li>
												<li> <img src="images/person.jpg"></li>
											</ul>
											<ul class="list-unstyled list-inline person-names">
												<li>Thomas Robertson</li>
												<li>Marry Anderson</li>
											</ul>
										</div>
										<div class="date-location">
											<ul class="list-inline">
												<li><em>Feb 28 - Mar 06 2016</em></li>
												<li><em>Rhinebeck, NY</em></li>
											</ul>
										</div>
									</div>
									<div class="panel-footer">
										<a href="#!" class="expand-button">
											<i class="fa fa-plus"></i>
										</a>
										<div class="expanded-tile-content animated fadeInUp">
											<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
											<a href="#!" class="btn white-transparent-button">Learn More</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tiles-row row">
					<div class="col-sm-12 headline-col">
						<div class="row">
							<div class="col-sm-12 col-md-6">
								<h2 class="headline-title">Ideas</h2>
							</div>
							<div class="col-sm-12 col-md-6">
								<!-- <a href="#!" class="btn see-all pull-right">See all</a>-->
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="row">
							<div class="col-sm-4">
								<div class="panel panel-default panel-idea tile">
									<div class="favourite">
										<div class="checkbox">
											<input type="checkbox" value="" id="favourite1">
											<label for="favourite1"></label>
										</div>
									</div>
									<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
									</div>
									<div class="panel-body panel-article">
										<div class="content-type">
											<span>Article</span>
										</div>
										<div class="title">
											<h1>Bobby McFerrin: Circle Songs from Omega</h1>
										</div>
										<div class="person">
											<ul class="by list-unstyled list-inline">
												<li>Thomas Robertson</li>
												<li>Marry Anderson</li>
											</ul>
											<span class="featuring">Featuring:</span>
											<ul class="featuring list-unstyled list-inline">
												<li>Thomas Robertson</li>
												<li>Marry Anderson</li>
											</ul>
										</div>
										<div class="duration-date">
											<em class="duration">13 minute watch</em>
											<em class="date-release">13th of September, 2016</em>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="panel panel-default panel-idea  tile">
									<div class="favourite">
										<div class="checkbox">
											<input type="checkbox" value="" id="favourite2">
											<label for="favourite2"></label>
										</div>
									</div>
									<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
									</div>
									<div class="panel-body panel-audio-video">
										<div class="content-type">
											<a href="#!"><i class="fa fa-play-circle-o"></i></a>
											<span>Video</span>
										</div>
										<div class="title">
											<h1>Bobby McFerrin: Circle Songs from Omega</h1>
										</div>
										<div class="person">
											<ul class="by list-unstyled list-inline">
												<li>Thomas Robertson</li>
												<li>Marry Anderson</li>
											</ul>
											<ul class="featuring list-unstyled list-inline">
												<li>Thomas Robertson</li>
												<li>Marry Anderson</li>
											</ul>
										</div>
										<div class="duration-date">
											<em class="duration">13 minute watch</em>
											<em class="date-release">13th of September, 2016</em>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		<div class="tiles-grid visible-xs">
			<div class="row">
				<div class="col-sm-12">
					<h1 class="section-title interractive-text">Pema Chödrön at Omega</h1>
				</div>
			</div>
			<div class="related">
				<h3 class="related-tiles">Related Workshops</h3>
				<div class="row">
					<div class="col-sm-6">
						<div class="panel panel-default tile  panel-workshop">
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite4">
									<label for="favourite4"></label>
								</div>
							</div>
							<div class="panel-body">
								<div class="content-type">
									<span>Workshop</span>
								</div>
								<div class="title">
									<h1>Collaborating With Nature</h1>
								</div>
								<div class="person">
									<ul class="list-unstyled list-inline person-images">
										<li>
											<img src="images/person.jpg">
										</li>
										<li> <img src="images/person.jpg"></li>
									</ul>
									<ul class="list-unstyled list-inline person-names">
										<li>Thomas Robertson</li>
										<li>Marry Anderson</li>
									</ul>
								</div>
								<div class="date-location">
									<ul class="list-inline">
										<li><em>Feb 28 - Mar 06 2016</em></li>
										<li><em>Rhinebeck, NY</em></li>
									</ul>
								</div>
							</div>
							<div class="panel-footer">
								<a href="#!" class="expand-button">
									<i class="fa fa-plus"></i>
								</a>
								<div class="expanded-tile-content animated fadeInUp">
									<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
									<a href="#!" class="btn white-transparent-button">Learn More</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="panel panel-default tile small panel-workshop">
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite4">
									<label for="favourite4"></label>
								</div>
							</div>
							<div class="panel-body">
								<div class="content-type">
									<span>Workshop</span>
								</div>
								<div class="title">
									<h1>Collaborating With Nature</h1>
								</div>
								<div class="person">
									<ul class="list-unstyled list-inline person-images">
										<li>
											<img src="images/person.jpg">
										</li>
										<li> <img src="images/person.jpg"></li>
									</ul>
									<ul class="list-unstyled list-inline person-names">
										<li>Thomas Robertson</li>
										<li>Marry Anderson</li>
									</ul>
								</div>
								<div class="date-location">
									<ul class="list-inline">
										<li><em>Feb 28 - Mar 06 2016</em></li>
										<li><em>Rhinebeck, NY</em></li>
									</ul>
								</div>
							</div>
							<div class="panel-footer">
								<a href="#!" class="expand-button">
									<i class="fa fa-plus"></i>
								</a>
								<div class="expanded-tile-content animated fadeInUp">
									<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
									<a href="#!" class="btn white-transparent-button">Learn More</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="panel panel-default tile small panel-workshop">
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite4">
									<label for="favourite4"></label>
								</div>
							</div>
							<div class="panel-body">
								<div class="content-type">
									<span>Workshop</span>
								</div>
								<div class="title">
									<h1>Collaborating With Nature</h1>
								</div>
								<div class="person">
									<ul class="list-unstyled list-inline person-images">
										<li>
											<img src="images/person.jpg">
										</li>
										<li> <img src="images/person.jpg"></li>
									</ul>
									<ul class="list-unstyled list-inline person-names">
										<li>Thomas Robertson</li>
										<li>Marry Anderson</li>
									</ul>
								</div>
								<div class="date-location">
									<ul class="list-inline">
										<li><em>Feb 28 - Mar 06 2016</em></li>
										<li><em>Rhinebeck, NY</em></li>
									</ul>
								</div>
							</div>
							<div class="panel-footer">
								<a href="#!" class="expand-button">
									<i class="fa fa-plus"></i>
								</a>
								<div class="expanded-tile-content animated fadeInUp">
									<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
									<a href="#!" class="btn white-transparent-button">Learn More</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="panel panel-default tile small panel-workshop">
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite4">
									<label for="favourite4"></label>
								</div>
							</div>
							<div class="panel-body">
								<div class="content-type">
									<span>Workshop</span>
								</div>
								<div class="title">
									<h1>Collaborating With Nature</h1>
								</div>
								<div class="person">
									<ul class="list-unstyled list-inline person-images">
										<li>
											<img src="images/person.jpg">
										</li>
										<li> <img src="images/person.jpg"></li>
									</ul>
									<ul class="list-unstyled list-inline person-names">
										<li>Thomas Robertson</li>
										<li>Marry Anderson</li>
									</ul>
								</div>
								<div class="date-location">
									<ul class="list-inline">
										<li><em>Feb 28 - Mar 06 2016</em></li>
										<li><em>Rhinebeck, NY</em></li>
									</ul>
								</div>
							</div>
							<div class="panel-footer">
								<a href="#!" class="expand-button">
									<i class="fa fa-plus"></i>
								</a>
								<div class="expanded-tile-content animated fadeInUp">
									<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
									<a href="#!" class="btn white-transparent-button">Learn More</a>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
			<div class="related">
				<h3 class="related-tiles">Related Ideas</h3>
				<div class="row">
					<div class="col-sm-6">
						<div class="panel panel-default panel-idea tile">
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite1">
									<label for="favourite1"></label>
								</div>
							</div>
							<div class="panel-heading" style="background: url('images/panel_idea.jpg')">

							</div>
							<div class="panel-body panel-article">
								<div class="content-type">
									<span>Article</span>
								</div>
								<div class="title">
									<h1>Bobby McFerrin: Circle Songs from Omega</h1>
								</div>
								<div class="person">
									<ul class="by list-unstyled list-inline">
										<li>Thomas Robertson</li>
										<li>Marry Anderson</li>
									</ul>
									<span class="featuring">Featuring:</span>
									<ul class="featuring list-unstyled list-inline">
										<li>Thomas Robertson</li>
										<li>Marry Anderson</li>
									</ul>
								</div>
								<div class="duration-date">
									<em class="duration">13 minute watch</em>
									<em class="date-release">13th of September, 2016</em>
								</div>
							</div>

						</div>
					</div>
					<div class="col-sm-6">
						<div class="panel panel-default panel-idea tile small">
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite1">
									<label for="favourite1"></label>
								</div>
							</div>
							<div class="panel-heading" style="background: url('images/panel_idea.jpg')">

							</div>
							<div class="panel-body panel-article">
								<div class="content-type">
									<span>Article</span>
								</div>
								<div class="title">
									<h1>Bobby McFerrin: Circle Songs from Omega</h1>
								</div>
								<div class="person">
									<ul class="by list-unstyled list-inline">
										<li>Thomas Robertson</li>
										<li>Marry Anderson</li>
									</ul>
									<span class="featuring">Featuring:</span>
									<ul class="featuring list-unstyled list-inline">
										<li>Thomas Robertson</li>
										<li>Marry Anderson</li>
									</ul>
								</div>
								<div class="duration-date">
									<em class="duration">13 minute watch</em>
									<em class="date-release">13th of September, 2016</em>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="panel panel-default panel-idea tile small">
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite1">
									<label for="favourite1"></label>
								</div>
							</div>
							<div class="panel-heading" style="background: url('images/panel_idea.jpg')">

							</div>
							<div class="panel-body panel-article">
								<div class="content-type">
									<span>Article</span>
								</div>
								<div class="title">
									<h1>Bobby McFerrin: Circle Songs from Omega</h1>
								</div>
								<div class="person">
									<ul class="by list-unstyled list-inline">
										<li>Thomas Robertson</li>
										<li>Marry Anderson</li>
									</ul>
									<span class="featuring">Featuring:</span>
									<ul class="featuring list-unstyled list-inline">
										<li>Thomas Robertson</li>
										<li>Marry Anderson</li>
									</ul>
								</div>
								<div class="duration-date">
									<em class="duration">13 minute watch</em>
									<em class="date-release">13th of September, 2016</em>
								</div>
							</div>

						</div>
					</div>
					<div class="col-sm-6">
						<div class="panel panel-default panel-idea tile small">
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite1">
									<label for="favourite1"></label>
								</div>
							</div>
							<div class="panel-heading" style="background: url('images/panel_idea.jpg')">

							</div>
							<div class="panel-body panel-article">
								<div class="content-type">
									<span>Article</span>
								</div>
								<div class="title">
									<h1>Bobby McFerrin: Circle Songs from Omega</h1>
								</div>
								<div class="person">
									<ul class="by list-unstyled list-inline">
										<li>Thomas Robertson</li>
										<li>Marry Anderson</li>
									</ul>
									<span class="featuring">Featuring:</span>
									<ul class="featuring list-unstyled list-inline">
										<li>Thomas Robertson</li>
										<li>Marry Anderson</li>
									</ul>
								</div>
								<div class="duration-date">
									<em class="duration">13 minute watch</em>
									<em class="date-release">13th of September, 2016</em>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
	</div>

	<?php include ("templates/footer.php"); ?>