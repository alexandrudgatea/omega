jQuery(document).ready(function ($) {
	$(function () {
		var jcarousel = $('#videoCarousel .video-carousel');
		jcarousel
			.jcarousel({
				wrap: 'circular',
				visible: $('#videoCarousel .video-carousel li').length
			});

		var direction = 'next';
		var windowWidth = window.innerWidth;
		var videoWidth = $('#videoCarousel .video-carousel').width();

		$('#videoCarousel .jcarousel-control-prev')
			.jcarouselControl({
				target: '-=1'
			});

		$('#videoCarousel .jcarousel-control-next')
			.jcarouselControl({
				target: '+=1'
			});

		jcarousel.on('jcarousel:animate', function (event, carousel) {
			$('.video-carousel .item').removeClass('active');
			var first = jcarousel.jcarousel('first');
			var CenterElement = first.next();
			CenterElement.addClass('active');
		});
	});
});
