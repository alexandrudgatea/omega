jQuery(document).ready(function ($) {
	$(window).bind('hashchange', function (e) {
		activateTab();
	});
	activateTab();

	//	======================================
	//  toggle search input
	//	======================================

	$("#search .button").on("click", function () {
		$("#search input").toggleClass("active");
	}); // ===== END =====

	//	======================================
	// 	allow operate mouse clicks inside megamenu without collapsing it
	//	======================================

	$(document).on('click', '.yamm .dropdown-menu', function (e) {
		e.stopPropagation()
	});

	$(document).on('click', '.yamm.mobile', function (e) {
		e.stopPropagation()
	}); // ===== END =====


	//	======================================
	// 	set min-height to page so footer stays to bottom in case of no content
	//	======================================

	var pageNavHeight = $('nav').outerHeight(true);
	var pageHeroHeight = $('#hero').outerHeight(true);
	var pageFooterHeight = $("#footer").outerHeight(true);
	var viewportHeight = $(window).height();
	var minpageHeight = viewportHeight - (pageNavHeight + pageFooterHeight);

	$('#block-system-main').css("min-height", minpageHeight + "px");
	// ===== END =====


	//	======================================
	// 	open dropdown on tablet
	//	======================================

	var windowWidth = $(window).width();
	if (windowWidth > 991) {
		//open megamenu for nav-top on li hover
		$('.nav-top .dropdown').hover(function () {
			$(this).addClass('open');
		}, function () {
			$(this).removeClass('open');
		});
		// ===== END =====

		//	======================================
		// 	open megamenu for nav-bottom on li hover and add animations
		//	======================================

		$('.nav-bottom .dropdown').hover(function () {
			$(this).addClass('open');
			//			$(".dropdown-menu").addClass("fadeInUp");
		}, function () {
			$(this).removeClass('open');
			//			$(".dropdown-menu").removeClass("fadeInUp");
		});
	} // ===== END =====

	//	======================================
	// 	check if social block newsletter input is filled
	//	======================================

	$("#navNewsEmail").on("change", function () {
		var value = $.trim($("#navNewsEmail").val());

		if (value.length > 0) {
			$("#navNewsletter button").removeClass("disabled");
		}
	}); // ===== END =====

	//	======================================
	// 	set tile height equal to width
	//	======================================

	$(".tiles-grid .tile").each(function () {
		var tileWidth = $(this).width();
		$(this).height(tileWidth);
	}); // ===== END =====

	//	======================================
	// 	set tile height equal to width on landingpage tab switch
	//	======================================

	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		$(".tiles-grid .tile").each(function () {
			var tileWidth = $(this).width();
			$(this).height(tileWidth);
		});
	});

	$(window).on("resize", function () {
		$(".tiles-grid .tile").each(function () {
			var tileWidth = $(this).width();
			$(this).height(tileWidth);
		});
	}); // ===== END =====

	//	======================================
	// 	set interval for  hero carousel
	//	======================================

	$('#homeCarousel').carousel({
		interval: false
	}); // ===== END =====

	//	======================================
	// 	set interval for  accomodation carousel - workshop-page
	//	======================================

	$('div[id*="unit-carousel"]').carousel({
		interval: false
	}); // ===== END =====

	//	======================================
	// 	set interval for  homepage quptes carousel
	//	======================================

	$('#quoteCarousel').carousel({
		interval: false
	}); // ===== END =====

	//	======================================
	// 	spin icon on quote caruselitem change
	//	======================================

	$("#quoteCarousel .cta").on("click", function () {
		$("#quoteCarousel .cta .glyphicon").addClass("fa-spin");
		setTimeout(function () {
			$("#quoteCarousel .cta .glyphicon").removeClass("fa-spin");
		}, 500);
	}); // ===== END =====

	//	======================================
	// 	open / close tile
	//	======================================

	$(document).on("click", ".tile .expand-button", function () {
		var tileHeight = $(this).closest(".panel").outerHeight();
		var thisFooter = $(this).closest(".panel").find(".panel-footer");

		thisFooter.toggleClass("open");
		if (thisFooter.hasClass("open")) {
			thisFooter.height(tileHeight + 20);
		} else {
			thisFooter.css("height", "0");
		}
	});
	// ===== END =====

	//	======================================
	// 	stop videoplayback in videopage carousel if carousel slides
	//	======================================

	$("#videoCarousel .carousel-control").on("click", function () {
		$("#videoCarousel .item.active iframe").attr("src", $("#videoCarousel .item.active iframe").attr("src"));
	}); // ===== END =====

	//	======================================
	// 	hide video overlay on play/pause video
	//	======================================

	$('video-frame .video').on("click", function () {
		$(".video-overlay").toggleClass("hidden");
	}); // ===== END =====

	//	======================================
	// 	add tooltips to tiles and change the text on click + open alert
	//	======================================
	$(".panel .favourite").attr("data-original-title", "Save to your favorites").tooltip();

	$('.panel .favourite').on("click", function () {
		$("#tile-alert").addClass("open");

	});

	$(document).on("click", "body .tile .fav-icon", function () {

		var tileTitle = $(this).parents(".tile").find("h1 a").html(),
			thisTile = $(this).parents(".tile"),
			thisNode = $(this).parents(".favourite").parent().attr("class");

		if ($("body").hasClass("logged-in")) {

			$("#tile-alert").addClass("open").find(".tile-name").html(tileTitle);

			$("." + thisNode).each(function () {
				$(this).find(".fav-icon").addClass("checked");
				$(this).find(".favourite").attr("data-original-title", "Remove from your favorites");
			});

			thisTile.find(".favourite").attr("data-original-title", "Remove from your favorites").tooltip('show');

			setTimeout(function () {
				$("body.logged-in #tile-alert").removeClass("open");
			}, 10000);

		}
	});

	$(document).on("click", "body .tile .fav-icon.checked", function () {
		var thisNode = $(this).parents(".favourite").parent().attr("class");

		$("." + thisNode).each(function () {
			$(this).find(".fav-icon").removeClass("checked");
			$(this).find(".favourite").attr("data-original-title", "Save to your favorites").tooltip();
		});

		$("body.logged-in #tile-alert").addClass("open");

		setTimeout(function () {
			$("body.logged-in #tile-alert").removeClass("open");
		}, 3000);
	});

	$("#tile-alert .close").on("click", function () {
		$(this).parents("#tile-alert").removeClass("open");
	}); // ===== END =====

	$("#alert .close").on("click", function () {
		$(this).parents("#alert").addClass("hidden");
	}); // ===== END =====

	//	======================================
	// 	change checkbox label
	//	======================================

	$(document).on('click', "[type='checkbox']", function (e) {
		var checked = $(this).is(':checked');

		if (checked) {
			$(this).parent().addClass("labelchecked");
		} else {
			$(this).parent().removeClass("labelchecked");
		}
	}); // ===== END =====


	//	======================================
	// 	change radio label
	//	======================================

	$("input:radio:checked").parents('label').addClass("labelchecked");

	$(document).on('click', "[type='radio']", function (e) {
		$('input:radio[name=' + $(this).attr('name') + ']').parent().removeClass('labelchecked');
		$(this).parent().addClass('labelchecked');
	}); // ===== END =====

	//	======================================
	// 	open dropdown on hover in landingpage menus
	//	======================================

	$('.page-navbar .dropdown').hover(function () {
			$(this).addClass('open');
		},
		function () {
			$(this).removeClass('open');

		});

	$('.page-navbar .dropdown-submenu').hover(function () {
			$(this).addClass('open');
		},
		function () {
			$(this).removeClass('open');
		}); // ===== END =====


	//	======================================
	// 	activate side-menu scrollspy
	//	======================================

	$('body').scrollspy({
		target: '.side-menu',
		offset: 70
	}); // ===== END =====

	//	======================================
	// 	initialize smoothscroll plugin
	//	======================================

	smoothScroll.init();
	// ===== END =====


	//	======================================
	//	set sidebar same height as content on fancy-page and worskshop page
	//	======================================

	if (windowWidth > 767) {
		$('.sidebar').height($(".page-body").parent().outerHeight());
	} // ===== END =====


	//	======================================
	//	sidemenu fallback if not enough content to activate scrollspy hover
	//	======================================

	$(".side-menu ul li a").on("click", function () {
		$(".side-menu ul li").removeClass("active");
		$(this).parent().addClass("active");

	}); // ===== END =====


	//	======================================
	//	set the affixed menu same width as it's parent - needed when it becomes positioned fixed
	//	======================================

	var wsAffixWidth = $(".workshop-page .sidebar").width();
	$(".workshop-page #sideMenu").width(wsAffixWidth);

	var fpAffixWidth = $(".fancy-page .sidebar").width();
	$(".fancy-page .sidebar .side-menu").width(fpAffixWidth);
	// ===== END =====

	//	======================================
	//	mobile menu js
	//	======================================

	if (windowWidth < 768) {
		$('.navbar.mobile .navbar-toggle').on('click', function () {
			setTimeout(function () {
				if ($("#mainMenuMobile").hasClass("in")) {
					$("body").css("overflow", "hidden");
				} else {
					$("body").css("overflow", "visible");
				}
			}, 500);
		});

		$(".nav-tabs li").on("click", function () {
			$(this).parents("ul").toggleClass("open");
		});

		$(".side-menu .mobile-toggle").on("click", function () {
			$(this).parent().toggleClass("open");
		});

		$(".side-menu a").on("click", function () {
			$(this).parents(".side-menu").removeClass("open");
		});


		$(".workshop-page #sideMenu .mobile-sidenav-toggle").on("click", function () {
			$(".workshop-page #sideMenu").toggleClass("open");
		});
		$(".workshop-page #sideMenu .side-menu a").on("click", function () {
			$(".workshop-page #sideMenu").removeClass("open");
		});

		//		$(".page-navbar .page-menu li.dropdown").addClass("open");
		//		$(".page-navbar .page-menu li.dropdown a").removeAttr("data-toggle");
		// ===== END =====

		//	======================================
		//	workshop page sidemenu affix
		//	======================================

		$('.workshop-page #sideMenu').affix({
			offset: {
				top: ($('.workshop-page .person.mobile').outerHeight(true) + $('#hero').outerHeight(true)) + 170,
			}
		}); // ===== END =====

		//	======================================
		//	workshop-page check if there are more than 4 teachers and show button to see more
		//	======================================

		var teachersNumber = $('.workshop-page .teachers-grid li').length;
		if (teachersNumber > 4) {
			$('.workshop-page .teachers-grid').addClass("four-plus");
			$('#wsTeachers .see-more').removeClass("hidden");
		}

		if ($("body").hasClass("logged-in")) {
			var loggedmenuOffset = $("#admin-menu").height();
			$("nav.navbar").css("top", loggedmenuOffset);
		}
	} // ===== END =====

	//	======================================
	//	activate affix on sidemenus
	//	======================================

	$('#sideMenu').affix({
		offset: {
			top: ($('.navbar.yamm').outerHeight(true) + $('#mainContent .navbar').outerHeight(true) + $('#hero').outerHeight(true)) + 40,
			bottom: $('#footer').outerHeight(true) + 40
		}
	}); // ===== END =====


	//	======================================
	//	append footer newsletter form name
	//	======================================

	//	$("#footer .webform-client-form").prepend('<legend>Newsletter Sign-up</legend>')
	$('.page-navbar .navbar-collapse').on('show.bs.collapse', function (e) {
		// hide open menus
		$('.page-navbar .navbar-collapse').each(function () {
			if ($(this).hasClass('in')) {
				$(this).collapse('toggle');
			}
		});
	}); // ===== END =====

	//	======================================
	//	lower font size in workshop hero it it's too long
	//	======================================

	var wspTitle = $("#hero.workshop-page .page-title");
	if (wspTitle.height() > 150) {
		wspTitle.css("font-size", "30px");
	} // ===== END =====

	//	workshop page member fav
	$("#sideMenu .member-fav").on("click", function () {
		$(this).toggleClass("favorite");
	}); // ===== END =====

	//	======================================
	//	carousel swipe functionality
	//	======================================

	$(".carousel").swiperight(function () {
		$(".carousel").carousel('prev');
	});
	$(".carousel").swipeleft(function () {
		$(".carousel").carousel('next');
	}); // ===== END =====

	//	======================================
	//	tuition accordion scroll to content
	//	======================================

	$("#feesAccordion").on("shown.bs.collapse", function () {
		var currentAccordion = $(this).find('.collapse.in').prev('.panel-heading');

		$('html, body').animate({
			scrollTop: $(currentAccordion).offset().top - 10
		}, 500);
	}); // ===== END =====




	$(window).on("resize", function () {
		var windowWidth = $(window).width();
		if (windowWidth > 767) {
			//	set sidebar same height as content on fancy-page and worskshop page
			$('.sidebar').height($(".page-body").parent().outerHeight());
		} else {
			$('.sidebar').css("height", "auto");
		}

		//		if (windowWidth < 768) {
		//			$(".page-navbar .page-menu li.dropdown").addClass("open");
		//			$(".page-navbar .page-menu li.dropdown a").removeAttr("data-toggle");
		//		} else {
		//			$(".page-navbar .page-menu li.dropdown").removeClass("open");
		//			$(".page-navbar .page-menu li.dropdown a").attr("data-toggle", "dropdown");
		//		}

		if (windowWidth < 768) {
			if ($("body").hasClass("logged-in")) {
				var loggedmenuOffset = $("#admin-menu").height();
				$("nav.navbar.yamm").css("top", loggedmenuOffset);
			}
		}
		var wsAffixWidth = $(".workshop-page .sidebar").width();
		$(".workshop-page #sideMenu").width(wsAffixWidth);

		var fpAffixWidth = $(".fancy-page .sidebar").width();
		$(".fancy-page .sidebar .side-menu").width(fpAffixWidth);
	});


	//	======================================
	//	make some tiles smaller on desktop by adding a class to remove some elements
	//	======================================

	$(".discover-more .related").each(function () {
		$(this).find(".tile").each(function (index) {
			if (index != 0) {
				$(this).addClass('small');
			}
		});
	});
	// ===== END =====

	//    STICKY CLASS ACTION
	function sticky_relocate() {
		var window_top = $(window).scrollTop();
		var div_top = $('#sticky-anchor').offset().top;
		if (window_top > div_top) {
			$('.sticky').addClass('stick');
			$('#sticky-anchor').height($('.sticky').outerHeight());
		} else {
			$('.sticky').removeClass('stick');
			$('#sticky-anchor').height(0);
		}
	}

	$(function () {
		if ($('.sticky').length !== 0) {
			$(".sticky").width($(".sticky").parent().width());
			$(window).on("resize", function () {
				$(".sticky").width($(".sticky").parent().width());
			});
			$(window).scroll(sticky_relocate);
			sticky_relocate();
		}
	});

	// END STICKY CLASS ACTION



	//
	// Activate TAB if found and called in #anchor
	//
	function activateTab() {
		if (windowWidth > 767) {
			var anchor = window.location.hash || false;
			if (anchor) {
				anchor = anchor.replace("#", "");
				targeterElement = $('.main-container').find("[aria-controls='" + anchor + "']") || false;
				if (targeterElement) {
					targeterElement.click();
				}
			}
		}
	}

	// END ACTIVATE TAB
	//	======================================
	//	highlight landingpage filter options on click
	//	======================================



	$(".filterModal .panel .suboption").on("click", function () {
		$(this).toggleClass("active");
		if ($(this).hasClass("all")) {
			$(".filterModal .panel .in .suboption").addClass("active");
		}

		$(this).parents(".panel").find(".suboption").each(function () {
			if (!$(this).hasClass("active")) {
				$(".filterModal .panel .suboption.all").removeClass("active");
			}
		});
	});

	$(".filterModal .reset-btn").on("click", function () {
		$(".filterModal .panel .suboption").removeClass("active");
	});

	//	======================================
	//	tags input - keep focus after pressing TAB
	//	======================================
	//
	//	$('.bootstrap-tagsinput').keydown(function (e) {
	//		if (e.which == 9) {
	//			$(".bootstrap-tagsinput input").focus();
	//			e.preventDefault();
	//		}
	//	});

	//	$('.bootstrap-tagsinput input').tagsinput({
	//		confirmKeys: [13, 9]
	//	});


	//	set the width of sticky object same as it's parent
	$(".sticky").width($(".sticky").parent().width());
	$(window).on("resize", function () {
		$(".sticky").width($(".sticky").parent().width());
	});

	// handle workshop page resize
	$(window).on("resize", function () {
		if (typeof $('.workshop-page #sideMenu .mobile-sidenav-toggle') !== 'undefined') {
			$(".workshop-page #sideMenu .mobile-sidenav-toggle").on("click", function () {
				$(".workshop-page #sideMenu").toggleClass("open");
			});
		}
	});

});
