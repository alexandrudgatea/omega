var audio = document.getElementById("audio-player");

audio.onloadeddata = function() {
       var timeleft = document.getElementById('timeleft'),
        duration = parseInt( audio.duration ),
        timeLeft = duration,
        s, m;
    
    s = timeLeft % 60;
    m = Math.floor( timeLeft / 60 ) % 60;
    
    s = s < 10 ? "0"+s : s;
    m = m < 10 ? "0"+m : m;
    
    timeleft.innerHTML = m+":"+s; 
	
};

// Countup
audio.addEventListener("timeupdate", function() {
    var timeline = document.getElementById('duration');
    var s = parseInt(audio.currentTime % 60);
    var m = parseInt((audio.currentTime / 60) % 60);
    if (s < 10) {
        timeline.innerHTML = "0"+m + ':0' + s;
    }
    else {
        timeline.innerHTML = "0"+m + ':' + s;
    }
}, false);