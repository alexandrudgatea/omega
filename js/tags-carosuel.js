(function ($) {
	$(function () {
		/*
		Carousel initialization
		*/
		$('.tags-carousel')
			.jcarousel({
				animation: 'slow',
				transitions: true,
				wrap: 'circular'
			});

		/*
		 Prev control initialization
		 */
		$('.tags-carousel-control-prev')
			.on('jcarouselcontrol:active', function () {
				$(this).removeClass('inactive');
			})
			.on('jcarouselcontrol:inactive', function () {
				$(this).addClass('inactive');
			})
			.jcarouselControl({
				// Options go here
				target: '-=1'
			});

		/*
		 Next control initialization
		 */
		$('.tags-carousel-control-next')
			.on('jcarouselcontrol:active', function () {
				$(this).removeClass('inactive');
			})
			.on('jcarouselcontrol:inactive', function () {
				$(this).addClass('inactive');
			})
			.jcarouselControl({
				// Options go here
				target: '+=1'
			});

		$(".tags-carousel").swiperight(function () {
			$(this).find(".tags-carousel-control-prev").jcarouselControl({
				target: '-=1'
			});
		});
		$(".tags-carousel").swipeleft(function () {
			$(this).find(".tags-carousel-control-next").jcarouselControl({
				target: '+=1'
			});
		});




	});
})(jQuery);
