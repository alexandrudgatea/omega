<?php include ("templates/header.php"); ?>
<div id="block-system-main">
	<div id="mainContent" class="landingpage owlc">
	<!--	this is the landingpage nav-->
	<nav class="navbar navbar-default page-navbar" role="navigation">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".page-menu">
					<span class="visible-xs mobile-menu-toggle"> Explore the OWLC <i class="fa fa-angle-down"></i> </span>
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".mobile-join">
					<span class="visible-xs mobile-menu"> Join</span>
				</button>

		</div>
		<div class="container-fluid">
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse page-menu">
				<ul class="nav navbar-nav navbar-right">
					<li><span>About the OWLC</span></li>
					<!--page-nav dropdown begin-->
					<li class="dropdown">
						<a href="http://google.com" class="dropdown-toggle" data-toggle="dropdown">Programs</a>
						<ul class="dropdown-menu">
							<li><a href="http://google.com">2016 Workshops</a></li>
							<li><a href="http://google.com">Leadership in Sustainable Education Award</a></li>
							<li><span>OCSL Past Programs</span></li>

							<!-- in case necessary this is markup for lvl2 dropdown-->
							<!--
								<li class="dropdown-submenu">
									<a class="submenu-drop" tabindex="-1" href="#">New dropdown </a>
									<ul class="dropdown-menu">
										<li><a href="#">2nd level dropdown</a></li>
										<li><a href="#">2nd level dropdown</a></li>
										<li><a href="#">2nd level dropdown</a></li>
									</ul>
								</li>
-->
						</ul>
					</li>
					<!--page-nav dropdown end-->
					<li><a href="#">Resources</a></li>
					<li><a href="#">Support the OWLC</a></li>
				</ul>
			</div>
			<div class="collapse navbar-collapse mobile-join">
				<ul class="nav navbar-nav navbar-right visible-xs">
					<li class="text-center">
						<div class="facebook-like">
							<a href="#" class="btn" target="_blank"><i class="fa fa-facebook"></i> Like OWLC on Facebook</a>
						</div>
					</li>
					<li>
						<div class="lp-not-logged">
							<div class="container">
								<div class="row">
									<div class="col-sm-12 center-block text-center">
										<form class="form-inline">
											<div class="form-group">
												<label for="owlcSignUp">Get OWLC Updates: </label>
												<input type="email" class="form-control" id="owlcSignUp" placeholder="you@email.com">
											</div>
											<button type="submit" class="btn btn-default">Sign Up</button>
										</form>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 text-center">
										<a href="#" class="log-in">
												Already Registered? Login
												<span class="fa-stack fa-lg">
													<i class="fa fa-circle fa-stack-1x"></i>
													<i class="fa fa-angle-right fa-stack-1x fa-inverse"></i>
												</span>
											</a>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="lp-logged">
							<div class="container">
								<div class="row">
									<div class="col-sm-12 center-block text-center">
										<form class="form-inline">
											<div class="form-group">
												<div class="checkbox">
													<label for="owlcUpdates">
															<input type="checkbox" value="" id="owlcUpdates">Get OWLC Updates
														</label>
												</div>
											</div>
										</form>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 text-center">
										<a href="#" class="view-profile interractive-text">
												View your profile
												<span class="fa-stack fa-lg">
													<i class="fa fa-circle fa-stack-1x "></i>
													<i class="fa fa-angle-right fa-stack-1x fa-inverse"></i>
												</span>
											</a>
									</div>
								</div>
							</div>
						</div>
					</li>

				</ul>
			</div>
		</div>
		<!-- /.navbar-collapse -->
	</nav>

	<div id="hero" class="landingpage background-image" style="background: url('images/hero_owlc.jpg')">
		<div class="container full-height">
			<div class="row">
				<div class="col-sm-12 text-center">
					<div class="hero-content field-text">
						<h1 class="initiative-title">Omega Women’s Leadership Center</h1>
						<h3 class="tagline">Nurturing relationships, healthy families and communities.</h3>
						<a href="#" class="btn" target="_blank"><i class="fa fa-facebook"></i> Like OWLC on Facebook</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--	===== not logged content =====-->
	<div class="lp-not-logged">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 center-block text-center">
					<form class="form-inline">
						<div class="form-group">
							<label for="owlcSignUp">Get OWLC Updates: </label>
							<input type="email" class="form-control" id="owlcSignUp" placeholder="you@email.com">
						</div>
						<button type="submit" class="btn btn-default">Sign Up</button>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 text-center">
					<a href="#" class="log-in">
						Already Registered? Login
						<span class="fa-stack fa-lg">
							<i class="fa fa-circle fa-stack-1x"></i>
							<i class="fa fa-angle-right fa-stack-1x fa-inverse"></i>
						</span>
					</a>
				</div>
			</div>
		</div>
	</div>

	<!--	===== logged in content =====-->
	<div class="lp-logged">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 center-block text-center">
					<form class="form-inline">
						<div class="form-group">
							<div class="checkbox">
								<label for="owlcUpdates">
										<input type="checkbox" value="" id="owlcUpdates">Get OWLC Updates
									</label>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 text-center">
					<a href="#" class="view-profile interractive-text">
						View your profile
						<span class="fa-stack fa-lg">
							<i class="fa fa-circle fa-stack-1x "></i>
							<i class="fa fa-angle-right fa-stack-1x fa-inverse"></i>
						</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<!--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
	<!--===== video carosuel =====-->
	<div id="videoCarousel">
		<div class="video-carousel">
			<ul class="list-inline clearfix">
				<li class="item">
					<div class="tile-frame">
						<div class="panel panel-default panel-idea tile">
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite1">
									<label for="favourite1"></label>
								</div>
							</div>
							<div class="panel-heading" style="background: url('images/panel_idea3.jpg')">

							</div>
							<div class="panel-body panel-audio-video">
								<div class="content-type">
									<a href="#"><i class="fa fa-play-circle-o"></i></a>
									<span>Video</span>
								</div>
								<div class="title">
									<h1>Learning to Let Go</h1>
								</div>
								<div class="person">
									<ul class="featuring list-unstyled list-inline">
										<li>Bobby McFerrin</li>
										<li class="duration">00:13:12</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="item active">
					<div class="tile-frame">
						<div class="panel panel-default panel-idea tile">
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite2">
									<label for="favourite2"></label>
								</div>
							</div>
							<div class="panel-heading" style="background: url('images/panel_idea.jpg')">

							</div>
							<div class="panel-body panel-audio-video">
								<div class="content-type">
									<a href="#"><i class="fa fa-play-circle-o"></i></a>
									<span>Video</span>
								</div>
								<div class="title">
									<h1>Learning to Let Go</h1>
								</div>
								<div class="person">
									<ul class="featuring list-unstyled list-inline">
										<li>Bobby McFerrin</li>
										<li class="duration">00:13:12</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="item">
					<div class="tile-frame">
						<div class="panel panel-default panel-idea tile">
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite3">
									<label for="favourite3"></label>
								</div>
							</div>
							<div class="panel-heading" style="background: url('images/panel_idea2.jpg')">

							</div>
							<div class="panel-body panel-audio-video">
								<div class="content-type">
									<a href="#"><i class="fa fa-play-circle-o"></i></a>
									<span>Video</span>
								</div>
								<div class="title">
									<h1>Learning to Let Go</h1>
								</div>
								<div class="person">
									<ul class="featuring list-unstyled list-inline">
										<li>Bobby McFerrin</li>
										<li class="duration">00:13:12</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="item">
					<div class="tile-frame">
						<div class="panel panel-default panel-idea tile">
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite4">
									<label for="favourite4"></label>
								</div>
							</div>
							<div class="panel-heading" style="background: url('images/panel_idea3.jpg')">

							</div>
							<div class="panel-body panel-audio-video">
								<div class="content-type">
									<a href="#"><i class="fa fa-play-circle-o"></i></a>
									<span>Video</span>
								</div>
								<div class="title">
									<h1>Learning to Let Go</h1>
								</div>
								<div class="person">
									<ul class="featuring list-unstyled list-inline">
										<li>Bobby McFerrin</li>
										<li class="duration">00:13:12</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="item">
					<div class="tile-frame">
						<div class="panel panel-default tile  panel-workshop">
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite4">
									<label for="favourite4"></label>
								</div>
							</div>
							<div class="panel-body">
								<div class="content-type">
									<span>Workshop</span>
								</div>
								<div class="title">
									<h1>Collaborating With Nature</h1>
								</div>
								<div class="person">
									<ul class="list-unstyled list-inline person-images">
										<li>
											<img src="images/person.jpg">
										</li>
										<li> <img src="images/person.jpg"></li>
									</ul>
									<ul class="list-unstyled list-inline person-names">
										<li>Thomas Robertson</li>
										<li>Marry Anderson</li>
									</ul>
								</div>
								<div class="date-location">
									<ul class="list-inline">
										<li><em>Feb 28 - Mar 06 2016</em></li>
										<li><em>Rhinebeck, NY</em></li>
									</ul>
								</div>
							</div>
							<div class="panel-footer">
								<a href="#" class="expand-button">
									<i class="fa fa-plus"></i>
								</a>
								<div class="expanded-tile-content animated fadeInUp">
									<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
									<a href="#" class="btn white-transparent-button">Learn More</a>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="item">
					<div class="tile-frame">
						<div class="panel panel-default tile  panel-learn">
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite7">
									<label for="favourite7"></label>
								</div>
							</div>
							<div class="panel-body">
								<div class="content-type">
									<span>E-course</span>
								</div>
								<div class="title">
									<h1>Growing &amp; Sustaining A Yoga Service Organization</h1>
								</div>
								<div class="person">
									<ul class="list-unstyled list-inline person-images">
										<li>
											<img src="images/person.jpg">
										</li>
										<li> <img src="images/person.jpg"></li>
										<li> <img src="images/person.jpg"></li>
									</ul>
									<ul class="list-unstyled list-inline person-names">
										<li>Thomas Robertson</li>
										<li>Marry Anderson</li>
										<li>Marry Anderson</li>
									</ul>
								</div>
							</div>
							<div class="panel-footer">
								<a href="#" class="expand-button">
									<i class="fa fa-plus"></i>
								</a>
								<div class="expanded-tile-content animated fadeInUp">
									<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
									<a href="#" class="btn white-transparent-button">Learn More</a>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="item">
					<div class="tile-frame">
						<div class="panel panel-default tile  panel-collection">
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite10">
									<label for="favourite10"></label>
								</div>
							</div>
							<div class="panel-body">
								<div class="content-type">
									<span>Playlist</span>
								</div>
								<div class="title">
									<h1>Pema Chodron's Words of Wisdom</h1>
								</div>
								<div class="image-tiles">
									<ul class="list-inline">
										<li style="background:url('images/workshop_events.jpg');"></li>
										<li style="background:url('images/person.jpg');"></li>
										<li style="background:url('images/online_learning.jpg');"></li>
										<li style="background:url('images/idea.jpg');"></li>
									</ul>
								</div>
								<div class="person">
									<ul class="list-unstyled list-inline person-names">
										<li>Thomas Robertson</li>
										<li>Marry Anderson</li>
										<li>Marry Anderson</li>
									</ul>
								</div>
							</div>
							<div class="panel-footer">
								<a href="#" class="expand-button">
									<i class="fa fa-plus"></i>
								</a>
								<div class="expanded-tile-content animated fadeInUp">
									<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
									<a href="#" class="btn white-transparent-button">Learn More</a>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="item">
					<div class="tile-frame">
						<div class="panel panel-default tile  panel-category" style="background: url('images/workshop_events.jpg')">
							<div class="panel-body">
								<div class="category">
									<h1><a href="#">Workshops &amp; Events</a></h1>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="item">
					<div class="tile-frame">
						<div class="panel panel-default tile  panel-general-purpose">
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite13">
									<label for="favourite13"></label>
								</div>
							</div>
							<div class="panel-heading" style="background: url('images/panel_idea.jpg')">

							</div>
							<div class="panel-body panel-article">
								<div class="content-type">
									<span>Take Action</span>
								</div>
								<div class="title">
									<h1>Learning to let go</h1>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="item">
					<div class="tile-frame">
						<div class="panel panel-default tile  panel-person">
							<div class="panel-body clearfix">
								<div class="person-image">
									<img src="images/person.jpg">
								</div>
								<div class="person-bio">
									<span class="block field-text person-ocupation">										Teacher
									</span>
									<span class="block field-text person-name">
										Armando Tolken
									</span>
									<span class="block field-text person-info">
										Armando is a great guy and knows so much about life, happiness, and where to get an amazing lobster roll in Santa Fe, he also …
									</span>
								</div>
							</div>

						</div>
					</div>
				</li>
			</ul>
		</div>
		<div class="jcarousel-control-prev left carousel-control"></div>
		<div class="jcarousel-control-next right carousel-control"></div>
	</div>
	<!--===TAB CONTENT===-->
	<div role="tabpanel">
		<!-- Nav tabs -->
		<div class="container">
			<!--tabs-->
			<div class="row">
				<div class="col-sm-12 lp-tabs">
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active">
							<a href="#owlcEverything" aria-controls="owlcEverything" role="tab" data-toggle="tab">Everything</a>
						</li>
						<li role="presentation">
							<a href="#owlcVideo" aria-controls="owlcVideo" role="tab" data-toggle="tab">Video Library</a>
						</li>
						<li role="presentation">
							<a href="#owlcAction" aria-controls="owlcAction" role="tab" data-toggle="tab">OWLC in Action</a>
						</li>
					</ul>
					<button type="button" class="btn filter-btn mobile-filter-btn visible-xs" data-toggle="modal" data-target=".filterModal"><i class="fa fa-filter"></i> Filter By Topics </button>
				</div>


			</div>
			<!--filter list-->
			<div class="row">
				<div class="col-sm-12 lp-filter">
					<form action="" method="POST" class="form-inline" role="form" id="owlcSearchForm">
						<div class="form-group">
							<input type="text" class="form-control" id="owlcSearch" name="owlcSearch" placeholder="Search OWLC Workshops, Video, Articles and More..." data-role="tagsinput">
						</div>
						<button type="submit" class="btn">Search</button>

						<div class="form-group date">
							<button type="button" class="btn dates-popover"><i class="fa fa-calendar-o"></i> All dates</button>
							<div class="allDates hidden">
								<div class="form-inline">
									<input type="text" class="form-control" name="" value="<?php print $start; ?>" placeholder="Starting">
									<label for="">to</label>
									<input type="text" class="form-control" name="" value="<?php print $end; ?>" placeholder="Until">
								</div>
							</div>
						</div>

						<button type="button" class="btn filter-btn" data-toggle="modal" data-target=".filterModal"><i class="fa fa-filter"></i> Filter By Topics </button>
					</form>

					<form action="" method="POST" role="form" id="owlcFilterForm">
						<!-- Modal -->
						<div class="modal fade owlc filterModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										<h4 class="modal-title" id="myModalLabel">Learning Pathways</h4>
									</div>
									<div class="modal-body text-left">
										<div class="panel-group" id="filterOptionsAccordion" role="tablist" aria-multiselectable="true">
											<div class="panel panel-default">
												<div class="panel-heading" role="tab" id="option1">
													<h4 class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#filterOptionsAccordion" href="#suboption1" aria-expanded="false" aria-controls="suboption1">
														  Featured
														</a>
													</h4>
												</div>
												<div id="suboption1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="option1">
													<div class="panel-body">
														<a href="#!" class="suboption all">All</a>
														<ul class="suboptions-list list-unstyled">
															<li><a href="#!" class="suboption">Meditation</a></li>
															<li><a href="#" class="suboption">Mind &amp; Psychology</a></li>
															<li><a href="#!" class="suboption">Spirituality</a></li>
															<li><a href="#!" class="suboption">Personal Growth</a></li>
															<li><a href="#!" class="suboption">Intuition</a></li>
															<li><a href="#!" class="suboption">Yoga</a></li>
															<li><a href="#!" class="suboption">Mindfulness</a></li>
															<li><a href="#!" class="suboption">Qigong &amp; Tai Chi</a></li>
														</ul>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading" role="tab" id="option2">
													<h4 class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#filterOptionsAccordion" href="#suboption2" aria-expanded="false" aria-controls="suboption2">
														  Health &amp; Healing
														</a>
													</h4>
												</div>
												<div id="suboption2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="option2">
													<div class="panel-body">
														<a href="#!" class="suboption all">All</a>
														<ul class="suboptions-list list-unstyled">
															<li><a href="#!" class="suboption">Meditation</a></li>
															<li><a href="#" class="suboption">Mind &amp; Psychology</a></li>
															<li><a href="#!" class="suboption">Spirituality</a></li>
															<li><a href="#!" class="suboption">Personal Growth</a></li>
															<li><a href="#!" class="suboption">Intuition</a></li>
															<li><a href="#!" class="suboption">Yoga</a></li>
															<li><a href="#!" class="suboption">Mindfulness</a></li>
															<li><a href="#!" class="suboption">Qigong &amp; Tai Chi</a></li>
														</ul>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading" role="tab" id="option3">
													<h4 class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#filterOptionsAccordion" href="#suboption3" aria-expanded="false" aria-controls="suboption3">
														  Creative Expresion
														</a>
													</h4>
												</div>
												<div id="suboption3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="option1">
													<div class="panel-body">
														<a href="#!" class="suboption all">All</a>
														<ul class="suboptions-list list-unstyled">
															<li><a href="#!" class="suboption">Meditation</a></li>
															<li><a href="#" class="suboption">Mind &amp; Psychology</a></li>
															<li><a href="#!" class="suboption">Spirituality</a></li>
															<li><a href="#!" class="suboption">Personal Growth</a></li>
															<li><a href="#!" class="suboption">Intuition</a></li>
															<li><a href="#!" class="suboption">Yoga</a></li>
															<li><a href="#!" class="suboption">Mindfulness</a></li>
															<li><a href="#!" class="suboption">Qigong &amp; Tai Chi</a></li>
														</ul>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading" role="tab" id="option4">
													<h4 class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#filterOptionsAccordion" href="#suboption4" aria-expanded="true" aria-controls="suboption4">
														  Relationship &amp; Family
														</a>
													</h4>
												</div>
												<div id="suboption4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="option4">
													<div class="panel-body">

													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading" role="tab" id="option5">
													<h4 class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#filterOptionsAccordion" href="#suboption5" aria-expanded="true" aria-controls="suboption5">
														  Leadership &amp; Work
														</a>
													</h4>
												</div>
												<div id="suboption5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="option5">
													<div class="panel-body">

													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading" role="tab" id="option6">
													<h4 class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#filterOptionsAccordion" href="#suboption6" aria-expanded="true" aria-controls="suboption6">
														  Sustainable Living
														</a>
													</h4>
												</div>
												<div id="suboption6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="option6">
													<div class="panel-body">

													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="submit" class="btn">Submit</button>
										<button type="button" class="btn reset-btn">Reset</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<!--tab contents-->
			<!--fiecare tabpanel de mai jos are un ID. te rog sa le referentiezi bine cu nav-tabs de mai sus, sa fei la fel la href si la aria controls-->
			<div class="row">
				<div class="col-sm-12">
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="owlcEverything">
							<div class="filter-results text-center interractive-text">
								<h2 class="interactive-text">Body Mind &amp; Spirit/ Yoga</h2>
								<p class="mb20">See all the ways we are cultivating a more compassionate, connected, balanced world.</p>

								<div class="tags-carousel-wrapper">
									<!-- Carousel -->
									<div class="jcarousel tags-carousel" data-jcarousel="true">
										<ul>
											<li><a href="#!">abc</a></li>
											<li><a href="#!">Yoga</a></li>
											<li><a href="#!">Omega</a></li>
											<li><a href="#!">Tag</a></li>
											<li><a href="#!">Body Mind </a></li>
											<li><a href="#!">Spirit</a></li>
											<li><a href="#!">Body Spirit</a></li>
											<li><a href="#!">Mind</a></li>
											<li><a href="#!">Spirit</a></li>
											<li><a href="#!">Body Mind &amp; Spirit</a></li>
											<li><a href="#!">Yoga</a></li>
											<li><a href="#!">Omega</a></li>
											<li><a href="#!">Tag</a></li>
											<li><a href="#!">Body Mind </a></li>
											<li><a href="#!">Spirit</a></li>
											<li><a href="#!">Body Spirit</a></li>
											<li><a href="#!">Mind</a></li>
											<li><a href="#!">Spirit</a></li>
										</ul>

									</div>
									<!-- Prev/next controls -->
									<a href="#" class="tags-carousel-control-prev" data-jcarouselcontrol="true"><i class="fa fa-angle-left"></i></a>
									<a href="#" class="tags-carousel-control-next inactive" data-jcarouselcontrol="true"><i class="fa fa-angle-right"></i></a>
								</div>
								<script>
									//	======================================
									//	mobile set tags-carousel width
									//	======================================
									setTimeout(function() {
										if (windowWidth < 768) {
											$(".tags-carousel").each(function(index) {
												var totalWidth = 0;
												$(this).find("li").each(function() {
													totalWidth += Math.ceil($(this).outerWidth(true));
												});
												$(this).find("ul").width(totalWidth);
											});
										}
									}, 500);
								</script>

								<!--clear filters button-->
								<button class="btn clear-filters mt20">Clear filters <i class="fa fa-times"></i></button>

							</div>
							<div class="tiles-grid">
								<div class="tiles-row row">
									<div class="col-sm-12 headline-col">
										<div class="row">
											<div class="col-sm-12 col-md-6">
												<h2 class="headline-title">Featured</h2>
											</div>
											<div class="col-sm-12 col-md-6">
												<a href="#" class="btn see-all pull-right">See all</a>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="row">
											<div class="col-sm-4">
												<div class="panel panel-default panel-idea tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite2">
															<label for="favourite2"></label>
														</div>
													</div>
													<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
													</div>
													<div class="panel-body panel-audio-video">
														<div class="content-type">
															<a href="#"><i class="fa fa-play-circle-o"></i></a>
															<span>Video</span>
														</div>
														<div class="title">
															<h1>Bobby McFerrin: Circle Songs from Omega</h1>
														</div>
														<div class="person">
															<ul class="by list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
															<ul class="featuring list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="duration-date">
															<em class="duration">13 minute watch</em>
															<em class="date-release">13th of September, 2016</em>
														</div>
													</div>
													<div class="panel-footer">
														<a href="#" class="expand-button">
															<i class="fa fa-plus"></i>
														</a>
														<div class="expanded-tile-content animated fadeInUp">
															<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
															<a href="#" class="btn white-transparent-button">Learn More</a>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="panel panel-default panel-collection tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite10">
															<label for="favourite10"></label>
														</div>
													</div>
													<div class="panel-body">
														<div class="content-type">
															<span>Playlist</span>
														</div>
														<div class="title">
															<h1>Pema Chodron's Words of Wisdom</h1>
														</div>
														<div class="image-tiles">
															<ul class="list-inline">
																<li style="background:url('images/workshop_events.jpg');"></li>
																<li style="background:url('images/person.jpg');"></li>
																<li style="background:url('images/online_learning.jpg');"></li>
																<li style="background:url('images/idea.jpg');"></li>
															</ul>
														</div>
														<div class="person">
															<ul class="list-unstyled list-inline person-names">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
													</div>
													<div class="panel-footer">
														<a href="#" class="expand-button">
															<i class="fa fa-plus"></i>
														</a>
														<div class="expanded-tile-content animated fadeInUp">
															<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
															<a href="#" class="btn white-transparent-button">Learn More</a>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="panel panel-default panel-learn tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite7">
															<label for="favourite7"></label>
														</div>
													</div>
													<div class="panel-body">
														<div class="content-type">
															<span>E-course</span>
														</div>
														<div class="title">
															<h1>Growing &amp; Sustaining A Yoga Service Organization</h1>
														</div>
														<div class="person">

														</div>
													</div>
													<div class="panel-footer">
														<a href="#" class="expand-button">
															<i class="fa fa-plus"></i>
														</a>
														<div class="expanded-tile-content animated fadeInUp">
															<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
															<a href="#" class="btn white-transparent-button">Learn More</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tiles-row row">
									<div class="col-sm-12 headline-col">
										<div class="row">
											<div class="col-sm-12 col-md-6">
												<h2 class="headline-title">Workshops &amp; Events</h2>
											</div>
											<div class="col-sm-12 col-md-6">
												<a href="#" class="btn see-all pull-right">See all</a>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="row">
											<div class="col-sm-4">
												<div class="panel panel-default panel-workshop tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite4">
															<label for="favourite4"></label>
														</div>
													</div>
													<div class="panel-body">
														<div class="content-type">
															<span>Workshop</span>
														</div>
														<div class="title">
															<h1>Collaborating With Nature</h1>
														</div>
														<div class="person">
															<ul class="list-unstyled list-inline person-images">
																<li>
																	<img src="images/person.jpg">
																</li>
																<li> <img src="images/person.jpg"></li>
															</ul>
															<ul class="list-unstyled list-inline person-names">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="date-location">
															<ul class="list-inline">
																<li><em>Feb 28 - Mar 06 2016</em></li>
																<li><em>Rhinebeck, NY</em></li>
															</ul>
														</div>
													</div>
													<div class="panel-footer">
														<a href="#" class="expand-button">
															<i class="fa fa-plus"></i>
														</a>
														<div class="expanded-tile-content animated fadeInUp">
															<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
															<a href="#" class="btn white-transparent-button">Learn More</a>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="panel panel-default panel-workshop tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite5">
															<label for="favourite5"></label>
														</div>
													</div>
													<div class="panel-body">
														<div class="content-type">
															<span>Workshop</span>
														</div>
														<div class="title">
															<h1>Collaborating With Nature</h1>
														</div>
														<div class="person">
															<ul class="list-unstyled list-inline person-images">
																<li>
																	<img src="images/person.jpg">
																</li>
																<li> <img src="images/person.jpg"></li>
															</ul>
															<ul class="list-unstyled list-inline person-names">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="date-location">
															<ul class="list-inline">
																<li><em>Feb 28 - Mar 06 2016</em></li>
																<li><em>Rhinebeck, NY</em></li>
															</ul>
														</div>
													</div>
													<div class="panel-footer">
														<a href="#" class="expand-button">
															<i class="fa fa-plus"></i>
														</a>
														<div class="expanded-tile-content animated fadeInUp">
															<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
															<a href="#" class="btn white-transparent-button">Learn More</a>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="panel panel-default panel-workshop tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite6">
															<label for="favourite6"></label>
														</div>
													</div>
													<div class="panel-body">
														<div class="content-type">
															<span>Workshop</span>
														</div>
														<div class="title">
															<h1>Collaborating With Nature</h1>
														</div>
														<div class="person">
															<ul class="list-unstyled list-inline person-images">
																<li>
																	<img src="images/person.jpg">
																</li>
																<li> <img src="images/person.jpg"></li>
															</ul>
															<ul class="list-unstyled list-inline person-names">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="date-location">
															<ul class="list-inline">
																<li><em>Feb 28 - Mar 06 2016</em></li>
																<li><em>Rhinebeck, NY</em></li>
															</ul>
														</div>
													</div>
													<div class="panel-footer">
														<a href="#" class="expand-button">
															<i class="fa fa-plus"></i>
														</a>
														<div class="expanded-tile-content animated fadeInUp">
															<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
															<a href="#" class="btn white-transparent-button">Learn More</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tiles-row row">
									<div class="col-sm-12 headline-col">
										<div class="row">
											<div class="col-sm-12 col-md-6">
												<h2 class="headline-title">Ideas</h2>
											</div>
											<div class="col-sm-12 col-md-6">
												<a href="#" class="btn see-all pull-right">See all</a>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="row">
											<div class="col-sm-4">
												<div class="panel panel-default panel-idea tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite1">
															<label for="favourite1"></label>
														</div>
													</div>
													<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
													</div>
													<div class="panel-body panel-article">
														<div class="content-type">
															<span>Article</span>
														</div>
														<div class="title">
															<h1>Bobby McFerrin: Circle Songs from Omega</h1>
														</div>
														<div class="person">
															<ul class="by list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
															<span class="featuring">Featuring:</span>
															<ul class="featuring list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="duration-date">
															<em class="duration">13 minute watch</em>
															<em class="date-release">13th of September, 2016</em>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="panel panel-default panel-idea  tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite2">
															<label for="favourite2"></label>
														</div>
													</div>
													<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
													</div>
													<div class="panel-body panel-audio-video">
														<div class="content-type">
															<a href="#"><i class="fa fa-play-circle-o"></i></a>
															<span>Video</span>
														</div>
														<div class="title">
															<h1>Bobby McFerrin: Circle Songs from Omega</h1>
														</div>
														<div class="person">
															<ul class="by list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
															<ul class="featuring list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="duration-date">
															<em class="duration">13 minute watch</em>
															<em class="date-release">13th of September, 2016</em>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="panel panel-default panel-idea tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite3">
															<label for="favourite3"></label>
														</div>
													</div>
													<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
													</div>
													<div class="panel-body panel-press">
														<div class="content-type">
															<span>Press Release</span>
														</div>
														<div class="title">
															<h1>Bobby McFerrin: Circle Songs from Omega</h1>
														</div>
														<div class="person">
															<ul class="by list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
															<span class="featuring">Featuring:</span>
															<ul class="featuring list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="duration-date">
															<em class="duration">13 minute watch</em>
															<em class="date-release">13<sup>th</sup> of September, 2016</em>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tiles-row row">
									<div class="col-sm-12 headline-col">
										<div class="row">
											<div class="col-sm-12 col-md-6">
												<h2 class="headline-title">People</h2>
											</div>
											<div class="col-sm-12 col-md-6">
												<a href="#" class="btn see-all pull-right">See all</a>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="row">
											<div class="col-sm-6">
												<div class="panel panel-default tile  panel-person">
													<div class="panel-body clearfix">
														<div class="person-image">
															<img src="images/person.jpg">
														</div>
														<div class="person-bio">
															<span class="block field-text person-ocupation">Teacher</span>
															<span class="block field-text person-name">Armando Tolken</span>
															<span class="block field-text person-info">Armando is a great guy and knows so much about life, happiness, and where to get an amazing lobster roll in Santa Fe, he also …</span>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="panel panel-default tile  panel-person">
													<div class="panel-body clearfix">
														<div class="person-image">
															<img src="images/person.jpg">
														</div>
														<div class="person-bio">
															<span class="block field-text person-ocupation">Teacher</span>
															<span class="block field-text person-name">Armando Tolken</span>
															<span class="block field-text person-info">Armando is a great guy and knows so much about life, happiness, and where to get an amazing lobster roll in Santa Fe, he also …</span>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="panel panel-default tile  panel-person">
													<div class="panel-body clearfix">
														<div class="person-image">
															<img src="images/person.jpg">
														</div>
														<div class="person-bio">
															<span class="block field-text person-ocupation">Teacher</span>
															<span class="block field-text person-name">Armando Tolken</span>
															<span class="block field-text person-info">Armando is a great guy and knows so much about life, happiness, and where to get an amazing lobster roll in Santa Fe, he also …</span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="owlcVideo">

							<div class="tiles-grid">
								<div class="tiles-row row">
									<div class="col-sm-12 headline-col">
										<div class="row">
											<div class="col-sm-12 col-md-6">
												<h2 class="headline-title">Workshops &amp; Events</h2>
											</div>
											<div class="col-sm-12 col-md-6">
												<a href="#" class="btn see-all pull-right">See all</a>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="row">
											<div class="col-sm-4">
												<div class="panel panel-default panel-workshop tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite4">
															<label for="favourite4"></label>
														</div>
													</div>
													<div class="panel-body">
														<div class="content-type">
															<span>Workshop</span>
														</div>
														<div class="title">
															<h1>Collaborating With Nature</h1>
														</div>
														<div class="person">
															<ul class="list-unstyled list-inline person-images">
																<li>
																	<img src="images/person.jpg">
																</li>
																<li> <img src="images/person.jpg"></li>
															</ul>
															<ul class="list-unstyled list-inline person-names">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="date-location">
															<ul class="list-inline">
																<li><em>Feb 28 - Mar 06 2016</em></li>
																<li><em>Rhinebeck, NY</em></li>
															</ul>
														</div>
													</div>
													<div class="panel-footer">
														<a href="#" class="expand-button">
															<i class="fa fa-plus"></i>
														</a>
														<div class="expanded-tile-content animated fadeInUp">
															<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
															<a href="#" class="btn white-transparent-button">Learn More</a>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="panel panel-default panel-workshop tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite5">
															<label for="favourite5"></label>
														</div>
													</div>
													<div class="panel-body">
														<div class="content-type">
															<span>Workshop</span>
														</div>
														<div class="title">
															<h1>Collaborating With Nature</h1>
														</div>
														<div class="person">
															<ul class="list-unstyled list-inline person-images">
																<li>
																	<img src="images/person.jpg">
																</li>
																<li> <img src="images/person.jpg"></li>
															</ul>
															<ul class="list-unstyled list-inline person-names">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="date-location">
															<ul class="list-inline">
																<li><em>Feb 28 - Mar 06 2016</em></li>
																<li><em>Rhinebeck, NY</em></li>
															</ul>
														</div>
													</div>
													<div class="panel-footer">
														<a href="#" class="expand-button">
															<i class="fa fa-plus"></i>
														</a>
														<div class="expanded-tile-content animated fadeInUp">
															<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
															<a href="#" class="btn white-transparent-button">Learn More</a>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="panel panel-default panel-workshop tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite6">
															<label for="favourite6"></label>
														</div>
													</div>
													<div class="panel-body">
														<div class="content-type">
															<span>Workshop</span>
														</div>
														<div class="title">
															<h1>Collaborating With Nature</h1>
														</div>
														<div class="person">
															<ul class="list-unstyled list-inline person-images">
																<li>
																	<img src="images/person.jpg">
																</li>
																<li> <img src="images/person.jpg"></li>
															</ul>
															<ul class="list-unstyled list-inline person-names">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="date-location">
															<ul class="list-inline">
																<li><em>Feb 28 - Mar 06 2016</em></li>
																<li><em>Rhinebeck, NY</em></li>
															</ul>
														</div>
													</div>
													<div class="panel-footer">
														<a href="#" class="expand-button">
															<i class="fa fa-plus"></i>
														</a>
														<div class="expanded-tile-content animated fadeInUp">
															<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
															<a href="#" class="btn white-transparent-button">Learn More</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tiles-row row">
									<div class="col-sm-12 headline-col">
										<div class="row">
											<div class="col-sm-12 col-md-6">
												<h2 class="headline-title">Ideas</h2>
											</div>
											<div class="col-sm-12 col-md-6">
												<a href="#" class="btn see-all pull-right">See all</a>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="row">
											<div class="col-sm-4">
												<div class="panel panel-default panel-idea tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite1">
															<label for="favourite1"></label>
														</div>
													</div>
													<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
													</div>
													<div class="panel-body panel-article">
														<div class="content-type">
															<span>Article</span>
														</div>
														<div class="title">
															<h1>Bobby McFerrin: Circle Songs from Omega</h1>
														</div>
														<div class="person">
															<ul class="by list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
															<span class="featuring">Featuring:</span>
															<ul class="featuring list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="duration-date">
															<em class="duration">13 minute watch</em>
															<em class="date-release">13th of September, 2016</em>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="panel panel-default panel-idea  tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite2">
															<label for="favourite2"></label>
														</div>
													</div>
													<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
													</div>
													<div class="panel-body panel-audio-video">
														<div class="content-type">
															<a href="#"><i class="fa fa-play-circle-o"></i></a>
															<span>Video</span>
														</div>
														<div class="title">
															<h1>Bobby McFerrin: Circle Songs from Omega</h1>
														</div>
														<div class="person">
															<ul class="by list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
															<ul class="featuring list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="duration-date">
															<em class="duration">13 minute watch</em>
															<em class="date-release">13th of September, 2016</em>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="panel panel-default panel-idea tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite3">
															<label for="favourite3"></label>
														</div>
													</div>
													<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
													</div>
													<div class="panel-body panel-press">
														<div class="content-type">
															<span>Press Release</span>
														</div>
														<div class="title">
															<h1>Bobby McFerrin: Circle Songs from Omega</h1>
														</div>
														<div class="person">
															<ul class="by list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
															<span class="featuring">Featuring:</span>
															<ul class="featuring list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="duration-date">
															<em class="duration">13 minute watch</em>
															<em class="date-release">13<sup>th</sup> of September, 2016</em>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="owlcAction">
							<div class="tiles-grid">
								<div class="tiles-row row">
									<div class="col-sm-12 headline-col">
										<div class="row">
											<div class="col-sm-12 col-md-6">
												<h2 class="headline-title">Ideas</h2>
											</div>
											<div class="col-sm-12 col-md-6">
												<a href="#" class="btn see-all pull-right">See all</a>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="row">
											<div class="col-sm-4">
												<div class="panel panel-default panel-idea tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite1">
															<label for="favourite1"></label>
														</div>
													</div>
													<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
													</div>
													<div class="panel-body panel-article">
														<div class="content-type">
															<span>Article</span>
														</div>
														<div class="title">
															<h1>Bobby McFerrin: Circle Songs from Omega</h1>
														</div>
														<div class="person">
															<ul class="by list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
															<span class="featuring">Featuring:</span>
															<ul class="featuring list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="duration-date">
															<em class="duration">13 minute watch</em>
															<em class="date-release">13th of September, 2016</em>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="panel panel-default panel-idea  tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite2">
															<label for="favourite2"></label>
														</div>
													</div>
													<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
													</div>
													<div class="panel-body panel-audio-video">
														<div class="content-type">
															<a href="#"><i class="fa fa-play-circle-o"></i></a>
															<span>Video</span>
														</div>
														<div class="title">
															<h1>Bobby McFerrin: Circle Songs from Omega</h1>
														</div>
														<div class="person">
															<ul class="by list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
															<ul class="featuring list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="duration-date">
															<em class="duration">13 minute watch</em>
															<em class="date-release">13th of September, 2016</em>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="panel panel-default panel-idea tile">
													<div class="favourite">
														<div class="checkbox">
															<input type="checkbox" value="" id="favourite3">
															<label for="favourite3"></label>
														</div>
													</div>
													<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
													</div>
													<div class="panel-body panel-press">
														<div class="content-type">
															<span>Press Release</span>
														</div>
														<div class="title">
															<h1>Bobby McFerrin: Circle Songs from Omega</h1>
														</div>
														<div class="person">
															<ul class="by list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
															<span class="featuring">Featuring:</span>
															<ul class="featuring list-unstyled list-inline">
																<li>Thomas Robertson</li>
																<li>Marry Anderson</li>
															</ul>
														</div>
														<div class="duration-date">
															<em class="duration">13 minute watch</em>
															<em class="date-release">13<sup>th</sup> of September, 2016</em>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->

		<!--OUR SPONSORS-->
		<div class="sponsors">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h1 class="section-title-big interractive-text text-center bottom-bar-orange">OWLC Sponsors</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 text-center">
						<ul class="sponsor-list list-inline">
							<li>
								<a href="#" target="_blank"><img src="images/sponsor1.png" alt="Sponsor 1"></a>
							</li>
							<li>
								<a href="#" target="_blank"><img src="images/sponsor2.png"></a>
							</li>
							<li>
								<a href="#" target="_blank"><img src="images/sponsor3.png"></a>
							</li>
							<li>
								<a href="#" target="_blank"><img src="images/sponsor4.png"></a>
							</li>
							<li>
								<a href="#" target="_blank"><img src="images/sponsor5.png"></a>
							</li>
							<li>
								<a href="#" target="_blank"><img src="images/sponsor6.png"></a>
							</li>
							<li>
								<a href="#" target="_blank"><img src="images/sponsor7.png"></a>
							</li>
							<li>
								<a href="#" target="_blank"><img src="images/sponsor8.png"></a>
							</li>
							<li>
								<a href="#" target="_blank"><img src="images/sponsor9.png"></a>
							</li>
							<li>
								<a href="#" target="_blank"><img src="images/sponsor10.png"></a>
							</li>
						</ul>
						<a href="#" class='interractive-text text-center see-more'>+ See more</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<script>
	jQuery(document).ready(function ($) {

		/* for specific browser url for landing-page make corresponding tab open */
		if ($('.lp-tabs .nav-tabs').find('li a[href="' + window.location.hash + '"]').length != 0) {
			$('.lp-tabs .nav-tabs li.active').removeClass('active');
			$('.lp-tabs .nav-tabs li a[href="' + window.location.hash + '"]').parent().addClass('active');

			$('.tab-content .tab-pane').removeClass('active');
			$('.tab-content #' + window.location.hash.split('#')[1]).addClass('active');
		}

		/* change url according to tab id */
		$(".lp-tabs .nav-tabs li a").on("click", function () {
			window.location.hash = this.hash;
		});

	});
</script>
<?php include ("templates/footer.php"); ?>
