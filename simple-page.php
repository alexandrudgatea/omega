	<?php include ("templates/header.php"); ?>


	<div id="mainContent" class="simple-page">
		<div id="hero" class="simple-page background-image" style="background: url('/images/hero_owlc.jpg')"></div>
		<div id="pageContent">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 no-padding">
						<div class="page-body">
							<div class="page-header text-center">
								<h1>Mission &amp; History and a very very long title on this simple page so show Omega Women Leadership Center</h1>
								<ul class="list-inline social-links text-center">
									<li>
										<a href="#!">
											<span class="fa-stack fa-lg">
											<i class="fa fa-circle fa-stack-2x teal"></i>
											<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
										</span>
										</a>
									</li>
									<li>
										<a href="#!">
											<span class="fa-stack fa-lg">
											<i class="fa fa-circle fa-stack-2x teal"></i>
											<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
										</span>
										</a>
									</li>
									<li>
										<a href="#!">
											<span class="fa-stack fa-lg">
											<i class="fa fa-circle fa-stack-2x teal"></i>
											<i class="fa fa-envelope-o fa-stack-1x fa-inverse"></i>
										</span>
										</a>
									</li>
									<li>
										<a href="#!">
											<span class="fa-stack fa-lg">
											<i class="fa fa-circle fa-stack-2x teal"></i>
											<i class="fa fa-share-alt fa-stack-1x fa-inverse"></i>
										</span>
										</a>
									</li>
								</ul>
							</div>
							<div class="page-text clearfix">
								<h2><strong>Omega is a place to explore the extraordinary potential that exists in all of us as individuals and together as a human family.</strong></h2>
								<p>
									Omega was founded on the holistic worldview that the well-being of each of us is deeply connected to the well-being of all living things. Since 1977, we have offered diverse and innovative educational experiences that inspire an integrated approach to personal and social change. Omega, a nonprofit organization, continues to be at the forefront of human development.
								</p>
								<p>
									Each year, more than 23,000 people attend workshops and educational programs delivered by hundreds of teachers, artists, healers, and thinkers on the leading edge of their disciplines. With special attention to our key initiatives in sustainability, women’s leadership, veterans care, and service, we bring awareness to issues that must be addressed in order for our society to heal and flourish.
								</p>
								<blockquote>
									We nurture dialogues on the integration of modern medicine and natural healing; design programs that connect science, spirituality, and creativity; and lay the groundwork for new traditions and lifestyles.
								</blockquote>
								<p>
									ACCOUNTABILITY We expect each of us to do what we say we will do, meet commitments, and be dependable and responsible.
								</p>
								<p>
									HOLISM We honor the mind, body, heart, and spirit in each individual, knowing the need to balance and blend all these elements. In our programming, we encourage authenticity as a means to build trust and as essential to the growth and development of the individual and the well-being of others and the world we share.
								</p>
								<p>
									INTEGRITY In business and in relationships, we conduct ourselves with honesty, fairness, truth, candor, and respect. We treat others as we ourselves would want to be treated. We focus on the collective good.
								</p>

								<p>
									SERVICE We value the practice of service and what it teaches us about ourselves, and our relation to others. We are attuned to and care about our participants’ needs and expectations. We treat each other with similar grace.
								</p>
								<p>
									SIMPLICITY We strive for clear, direct, and unambiguous communication.We seek true, underlying meaning, and employ spiritual guidance in that quest. In this way, we work to make sense of the complexities of life.
								</p>
								<p>
									SUSTAINABILITY We consider the impact of our actions. We advocate for fairness in the treatment of all species, make decisions for the common good, and encourage activism as a means to social justice. Our facilities are grounded in the awareness of our relationship to the environment. We endeavor to have our work in the world be regenerative and self-sustaining.
								</p>
								<p>
									TEAMWORK We work together, inclusively, collaboratively, with energy, intention, and commitment. We keep each other informed, share what we are thinking and doing, and expect the same in return.
								</p>
								<p>
									WELCOMING We invite people to find space here to feel safe, create community, feel at home, and find nourishment. Our environment is nurturing, relaxing, stimulating, and inspiring. are attuned to and care about our participants’ needs and expectations. We treat each other with similar grace.
								</p>
								<h2>Omega's History</h2>
								<p>
									Founded in 1977 by Stephan Rechtschaffen, MD, and Elizabeth Lesser, Omega Institute for Holistic Studies was inspired by scholar and Eastern meditation teacher, Pir Vilayat Inayat Khan. Together, they envisioned a dynamic "university of life" designed to foster personal growth and social change. The name "Omega" came from the teachings of Pierre Teilhard de Chardin, a renowned 20th-century philosopher, who used the term "Omega Point" to describe the peak of unity and integration toward which all life is evolving.
								</p>
								<p>
									In 1981, Omega expanded from rented facilities in New York and Vermont to our current Rhinebeck home on the former grounds of Camp Boiberik, a popular Yiddish camp. Over the years, we have lovingly restored the campus while maintaining its simplicity as it has grown to 250 acres and more than 100 buildings, including the Sanctuary, the Ram Dass Library, and the new Omega Center for Sustainable Living.
								</p>
								<h2>A Bright Future</h2>
								<p>
									Today, Omega is the nation's most trusted source for wellness and personal growth, welcoming more than 23,000 people to our workshops, conferences, and retreats in Rhinebeck and at exceptional locations around the world.
								</p>
								<p>
									A nonprofit organization, Omega has consistently been at the forefront of human development. From nurturing early dialogues on the integration of modern medicine and natural healing; to designing programs that connect science, spirituality, and creativity; to laying the groundwork for new traditions and lifestyles, Omega continues to be a place where people from all walks of life come for inspiration, restoration, and new ideas. Our mission guides us to help people find health, happiness, and community while living gently on the Earth.
								</p>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		<?php include ("templates/footer.php"); ?>

