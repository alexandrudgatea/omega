<?php include ("templates/header.php"); ?>
	<div id="hero" class="article background-image" style="background: url('/images/hero_article.jpg')">
		<div class="container full-height">
			<div class="row">
				<div class="col-sm-6 special-bg transparent">
					<div class="article-info field-text">
						<h4 class="article-category ">Omega Women’s Leadership Center</h4>
						<h1 class="article-title ">5 Ways to Appreciate Your Friends</h1>
						<span class="article-subtitle ">An interview with Ania Nussbaum</span>
					</div>

				</div>
			</div>
		</div>
	</div>
	<div id="mainContent" class="article">
		<div class="container">
			<div class="row">
				<div class="col-sm-3 sidebar">
					<ul class="article-info list-unstyled">
						<li>
							<span class="article-author block">By Ania Nussbaum</span>
							<span class="article-date block">May 03, 2015</span>
						</li>
						<li class="article-share">
							<ul class="list-inline social-links">
								<li>
									<a href="#!">
										<span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x teal"></i>
									<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
								</span>
									</a>
								</li>
								<li>
									<a href="#!">
										<span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x teal"></i>
									<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
								</span>
									</a>
								</li>
								<li>
									<a href="#!">
										<span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x teal"></i>
									<i class="fa fa-envelope-o fa-stack-1x fa-inverse"></i>
								</span>
									</a>
								</li>
								<li>
									<a href="#!">
										<span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x teal"></i>
									<i class="fa fa-share-alt fa-stack-1x fa-inverse"></i>
								</span>
									</a>
								</li>
							</ul>
							<a href="#!" class="btn cta icon-button rounded-corners"><i class="fa fa-bookmark icon"></i>See Another</a>
						</li>
					</ul>
					<ul class="future-events list-unstyled">
						<li>
							<div class="event-type">Workshop</div>
							<h3 class="event-title">Growing and Sustaining A Yoga Service Orginzation</h3>
							<span class="event-participants">Ania Nussbaum, Grenadine Wright, Rene Zenllenger, and more</span>
							<span class="event-date">Jun 12 – Jun 18 2016</span>
						</li>
						<li>
							<div class="event-type">Conference</div>
							<h3 class="event-title">Transformational Cleaning for the Body and Soul</h3>
							<span class="event-participants">Ania Nussbaum and Grenadine Wright</span>
							<span class="event-date">Feb 28 – Mar 06 2016</span>
						</li>
						<li>
							<div class="event-type">Workshop</div>
							<h3 class="event-title">Collaborating with Nature</h3>
							<span class="event-participants">Peter Parker and Grenadine Wright</span>
							<span class="event-date">Jun 12 – Jun 18 2016</span>
						</li>
					</ul>
				</div>
				<div class="col-sm-9 no-padding">
					<div class="article-body">
						<span class="article-intro interractive-text bottom-bar-left-teal">
						Holidays like Valentine’s Day, are marked by the practice of showing love and appreciation for those we are romantically involved with. Other holidays, like Thanksgiving and Christmas, are traditionally characterized by sharing time. Other holidays, like Thanksgiving and Christmas, are traditionally characterized by sharing time characterized.
						</span>
						<div class="quote-section">
							<div class="quote-icon">&lsquo;&lsquo; </div>
							<h2 class="quote">You must have chaos within you to give birth to a dancing star.</h2>
							<span class="quote-author block">Friedrich Nietzsche</span>
							<span class="quote-author-profession block">Philosopher, cultural critic, and poet</span>
							<ul class="list-inline social-links">
								<li>
									<a href="#!">
										<span class="fa-stack fa-lg">
											<i class="fa fa-circle fa-stack-2x teal"></i>
											<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
										</span>
									</a>
								</li>
								<li>
									<a href="#!">
										<span class="fa-stack fa-lg">
											<i class="fa fa-circle fa-stack-2x teal"></i>
											<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
										</span>
									</a>
								</li>
							</ul>
						</div>
						<span class="article-text">
							<h1>Heading 1</h1>
				<h2>Heading 2</h2>
				<h3>Heading 3</h3>
				<h4>Heading 4</h4>
				</br>
				<p>This is a paragraph Nostrud noster sunt in esse an qui lorem tamen varias consequat do fabulas anim enim pariatur illum, do do quae admodum sed ullamco ut sunt quibusdam aut aut fugiat minim ipsum vidisse, fugiat occaecat nescius te sed varias ingeniis. Cillum mentitum te praetermissum de ex dolor ullamco quibusdam.</p>
				<em class="serif">This is the italic font. To give it the serif font just add class <code>serif</code> to it</em>

				<p><a href="http://">This is a link</a> The link &lt;a href="http:// [...]"><em>link here</em> &lt; /a &gt; tag is wrapped inside a &lt; p &gt; to inherit it's size</p>
				</br>
				<p><strong><em>Below is a list:</em></strong></p>
				<ul>
					<li>
						<p>This is a paragraph item</p>
					</li>
					<li>
						<p><a href="http://">This is a link item</a></p>
					</li>
					<li><em class="serif">This is an italic text with serif font</em></li>
				</ul>
				<hr></hr>
				<p class="font-300">This paragraph has <code>font-weight: 400;</code></p>
				<p class="font-400">This paragraph has <code>font-weight: 400;</code></p>
				<p class="font-700">This paragraph has <code>font-weight: 700;</code></p>
				<hr></hr>
<div>
				<p>
					Appellat aut laborum, quid quamquam exercitation, magna exquisitaque mandaremus veniam cupidatat et iis dolor consequat ab nam summis eram elit expetendis, probant sint qui singulis concursionibus, ut se concursionibus an laboris sint vidisse cupidatat. Quid senserit voluptatibus, dolore in do magna nescius, se eiusmod sempiternum, non ubi adipisicing. Esse hic eiusmod. Quamquam sed labore, laborum ex incurreret, ab iis concursionibus in de mentitum coniunctione. Cupidatat est aliqua ingeniis se lorem adipisicing fabulas dolore offendit. Malis vidisse appellat, do senserit adipisicing hic ubi dolor quamquam, doctrina duis multos ne quorum. Voluptate ita possumus, te quem vidisse. Minim laborum ad aliqua sint ubi anim possumus distinguantur, tamen occaecat incurreret aut vidisse summis probant ingeniis aut nulla hic ab enim officia ita ea quo efflorescere, ab amet laborum praesentibus, legam si sed multos laborum.
				</p>


				<p class="text-left"><img src="images/initiative3.jpg"></p>
				<p class="text-center"><img src="images/initiative3.jpg"></p>
				<p class="text-right"><img src="images/initiative3.jpg"></p>
				<p>Appellat aut laborum, quid quamquam exercitation, magna exquisitaque mandaremus veniam cupidatat et iis dolor consequat ab nam summis eram elit expetendis, probant sint qui singulis concursionibus, ut se concursionibus an laboris sint vidisse cupidatat. Quid senserit voluptatibus, dolore in do magna nescius, se eiusmod sempiternum, non ubi adipisicing. Esse hic eiusmod. Quamquam sed labore, laborum ex incurreret, ab iis concursionibus in de mentitum coniunctione. Cupidatat est aliqua ingeniis se lorem adipisicing fabulas dolore offendit. Malis vidisse appellat, do senserit adipisicing hic ubi dolor quamquam, doctrina duis multos ne quorum. Voluptate ita possumus, te quem vidisse. Minim laborum ad aliqua sint ubi anim possumus distinguantur, tamen occaecat incurreret aut vidisse summis probant ingeniis aut nulla hic ab enim officia ita ea quo efflorescere, ab amet laborum praesentibus, legam si sed multos laborum.</p>
				<p><strong>Left floated image</strong></p>
				<img src="images/panel_idea.jpg" class="left-align">
				<p>Appellat aut laborum, quid quamquam exercitation, magna exquisitaque mandaremus veniam cupidatat et iis dolor consequat ab nam summis eram elit expetendis, probant sint qui singulis concursionibus, ut se concursionibus an laboris sint vidisse cupidatat. Quid senserit voluptatibus, dolore in do magna nescius, se eiusmod sempiternum, non ubi adipisicing. Esse hic eiusmod. Quamquam sed labore, laborum ex incurreret, ab iis concursionibus in de mentitum coniunctione. Cupidatat est aliqua ingeniis se lorem adipisicing fabulas dolore offendit. Malis vidisse appellat, do senserit adipisicing hic ubi dolor quamquam, doctrina duis multos ne quorum. Voluptate ita possumus, te quem vidisse. Minim laborum ad aliqua sint ubi anim possumus distinguantur, tamen occaecat incurreret aut vidisse summis probant ingeniis aut nulla hic ab enim officia ita ea quo efflorescere, ab amet laborum praesentibus, legam si sed multos laborum.</p>
				<p>Appellat aut laborum, quid quamquam exercitation, magna exquisitaque mandaremus veniam cupidatat et iis dolor consequat ab nam summis eram elit expetendis, probant sint qui singulis concursionibus, ut se concursionibus an laboris sint vidisse cupidatat. Quid senserit voluptatibus, dolore in do magna nescius, se eiusmod sempiternum, non ubi adipisicing. Esse hic eiusmod. Quamquam sed labore, laborum ex incurreret, ab iis concursionibus in de mentitum coniunctione. Cupidatat est aliqua ingeniis se lorem adipisicing fabulas dolore offendit. Malis vidisse appellat, do senserit adipisicing hic ubi dolor quamquam, doctrina duis multos ne quorum. Voluptate ita possumus, te quem vidisse. Minim laborum ad aliqua sint ubi anim possumus distinguantur, tamen occaecat incurreret aut vidisse summis probant ingeniis aut nulla hic ab enim officia ita ea quo efflorescere, ab amet laborum praesentibus, legam si sed multos laborum.</p>
				<p><strong>Right floated image</strong></p>
				<img src="images/person.jpg" class="right-align">
				<p>Appellat aut laborum, quid quamquam exercitation, magna exquisitaque mandaremus veniam cupidatat et iis dolor consequat ab nam summis eram elit expetendis, probant sint qui singulis concursionibus, ut se concursionibus an laboris sint vidisse cupidatat. Quid senserit voluptatibus, dolore in do magna nescius, se eiusmod sempiternum, non ubi adipisicing. Esse hic eiusmod. Quamquam sed labore, laborum ex incurreret, ab iis concursionibus in de mentitum coniunctione. Cupidatat est aliqua ingeniis se lorem adipisicing fabulas dolore offendit. Malis vidisse appellat, do senserit adipisicing hic ubi dolor quamquam, doctrina duis multos ne quorum. Voluptate ita possumus, te quem vidisse. Minim laborum ad aliqua sint ubi anim possumus distinguantur, tamen occaecat incurreret aut vidisse summis probant ingeniis aut nulla hic ab enim officia ita ea quo efflorescere, ab amet laborum praesentibus, legam si sed multos laborum.</p>
				<p>Appellat aut laborum, quid quamquam exercitation, magna exquisitaque mandaremus veniam cupidatat et iis dolor consequat ab nam summis eram elit expetendis, probant sint qui singulis concursionibus, ut se concursionibus an laboris sint vidisse cupidatat. Quid senserit voluptatibus, dolore in do magna nescius, se eiusmod sempiternum, non ubi adipisicing. Esse hic eiusmod. Quamquam sed labore, laborum ex incurreret, ab iis concursionibus in de mentitum coniunctione. Cupidatat est aliqua ingeniis se lorem adipisicing fabulas dolore offendit. Malis vidisse appellat, do senserit adipisicing hic ubi dolor quamquam, doctrina duis multos ne quorum. Voluptate ita possumus, te quem vidisse. Minim laborum ad aliqua sint ubi anim possumus distinguantur, tamen occaecat incurreret aut vidisse summis probant ingeniis aut nulla hic ab enim officia ita ea quo efflorescere, ab amet laborum praesentibus, legam si sed multos laborum.</p>
				<p>Appellat aut laborum, quid quamquam exercitation, magna exquisitaque mandaremus veniam cupidatat et iis dolor consequat ab nam summis eram elit expetendis, probant sint qui singulis concursionibus, ut se concursionibus an laboris sint vidisse cupidatat. Quid senserit voluptatibus, dolore in do magna nescius, se eiusmod sempiternum, non ubi adipisicing. Esse hic eiusmod. Quamquam sed labore, laborum ex incurreret, ab iis concursionibus in de mentitum coniunctione. Cupidatat est aliqua ingeniis se lorem adipisicing fabulas dolore offendit. Malis vidisse appellat, do senserit adipisicing hic ubi dolor quamquam, doctrina duis multos ne quorum. Voluptate ita possumus, te quem vidisse. Minim laborum ad aliqua sint ubi anim possumus distinguantur, tamen occaecat incurreret aut vidisse summis probant ingeniis aut nulla hic ab enim officia ita ea quo efflorescere, ab amet laborum praesentibus, legam si sed multos laborum.</p>
				<p><strong>Full-width(90% actually) image</strong></p>
				<img src="images/initiative1.jpg" class="full-width">
				<p>Appellat aut laborum, quid quamquam exercitation, magna exquisitaque mandaremus veniam cupidatat et iis dolor consequat ab nam summis eram elit expetendis, probant sint qui singulis concursionibus, ut se concursionibus an laboris sint vidisse cupidatat. Quid senserit voluptatibus, dolore in do magna nescius, se eiusmod sempiternum, non ubi adipisicing. Esse hic eiusmod. Quamquam sed labore, laborum ex incurreret, ab iis concursionibus in de mentitum coniunctione. Cupidatat est aliqua ingeniis se lorem adipisicing fabulas dolore offendit. Malis vidisse appellat, do senserit adipisicing hic ubi dolor quamquam, doctrina duis multos ne quorum. Voluptate ita possumus, te quem vidisse. Minim laborum ad aliqua sint ubi anim possumus distinguantur, tamen occaecat incurreret aut vidisse summis probant ingeniis aut nulla hic ab enim officia ita ea quo efflorescere, ab amet laborum praesentibus, legam si sed multos laborum.</p>
						Even if there were a day of observance set aside for the appreciation of friends, it seems like one day would not be enough to convey the importance and distinction that friends bring to our lives. They are our sounding boards, our support systems when family fails us or simply
						becomes too challenging, and they provide invaluable perspective and reflection that helps us make sense of our world. In essence, as an old friend used to say, “friends are our chosen family.” Barring the naming of a holiday like “Friend’s Day,” we can choose to show appreciation for our friends daily. But other than picking up the tab every so
						often or buying a gift, we often come up blank in showing true appreciation that feels authentic, natural, and representative of our deepest consideration of these people we call friends. You may have already cultivated a language, or mode of expression, that effectively expresses and conveys the value and importance of your friends. However, the rest of us need some guidance and creative direction to spread our warmth, consideration, and affection to our friends.
						<br><br>Encouragement<br><br>
						Right now, each one of your friends is facing a challenge of some sort. Some may be coping with adversity, others may be considering a radical life shift or a minor life improvement. Get involved in their process, consider their
						difficulties, fears, and blocks, and do everything you can to encourage growth, risk-taking, and investigation, while providing a loyal sense of support through tough, or just challenging times. Use the Mail Between texts, email, and various forms of social media, there really is no practical reason to ever send anything through the post, right? Wrong. Our mailboxes have become a dumping ground for bills and unwanted solicitations. So when something unusual arrives in our unexpected and remarkable. Send a just a note telling your friend that Such a gesture will, no doubt, bring light to an ordinary day.
							<img src="images/initiative1.jpg" class="right-align">
						<br><br>Make Time<br><br>
						When asked, people tend to tell one another how incredibly busy they are. Instead of observing the obstacles that are keeping us from spending time and energy on our relationships, put aside a bit of time for a friend  that demonstrates both devotion and intention to hang out or just experience one another. This could be done in person or over the phone. Either way, make sure the time is spent with togetherness in mind, and all forms of distraction eliminated.
						</span>
						
						<div class="carousel-section">
							<div class="dark-bg field-text">
								<div class="text-block">
									<h1 class="field-text section-title">The Photographic Visions of Douglas Beasley</h1>
									<span class="author block">By Briana Boles</span>
									<span class="date block">May 03, 2016</span>
									<span class="description"> Arts Week teacher Douglas Beasley is founder and director of Vision Quest Photo Workshops. As an artist, he believes that we always have the opportunity to transform an emotional  experience into a visual one.</span>
								</div>

								<div id="articleCarousel" class="carousel slide carousel-fade" data-ride="carousel">
									<ol class="carousel-indicators">
										<li data-target="#articleCarousel" data-slide-to="0" class="active"> </li>
										<li data-target="#articleCarousel" data-slide-to="1"> </li>
										<li data-target="#articleCarousel" data-slide-to="2"> </li>
									</ol>
									<div class="carousel-inner">
										<div class="item active">
											<img src="images/hero.jpeg" alt="First slide" />
										</div>
										<div class="item">
											<img src="images/hero2.jpeg" alt="Second slide" />
										</div>
										<div class="item">
											<img src="images/hero3.jpeg" alt="Third slide" />
										</div>
									</div>
									<a class="left carousel-control" href="#articleCarousel" data-slide="prev">

									</a>
									<a class="right carousel-control" href="#articleCarousel" data-slide="next">

									</a>
								</div>
								<div class="text-block">
									<span class="description">
								Douglas describes his own journey as one through religion toward spirituality, and he credits
photography as helping him become more authentic in his own vision. 
								<br><br>
								“My work as a photographer is about seeking the sacred,” he says, “looking to find the divine in everything. My photography is about recognizing the spirit that inhabits all things, animate and inanimate, big and small.”
								</span>
								</div>
							</div>
						</div>
						<span class="article-text">
						<br>Share Friends<br><br>
						What better way to demonstrate your faith and enthusiasm for a friend than introducing them to another friend, or even set of friends. If you truly enjoy and cherish a friendship, you will want to share that friend with others and endorse their qualities and character. Like you would suggest a fantastic
vacation or a tremendous experience, making introductions between friends reveals your love and enthusiasm for all involved. Set up a lunch date, a trip to a museum, or even a ski trip and make sure your eagerness is known to all on your invite list. Even if it doesn’t yield a lasting connection between friends, your intention will resonate with all.

						<br><br>Unexpected Gifts<br><br>
						No matter what friends may tell you, everyone loves a gift, and unexpected gifts can be best because they are devoid of expectation and prediction. Surprise a friend with a simple gift that expresses a heartfelt gesture of friendship. This could be simply picking up the tab at a meal, purchasing
something you know they would love but would never buy for themselves, or (if you are crafty) actually making something for them and presenting it with pride and affection.
						</span>

						<div class="discover-more">
							<h1 class="section-title">Discover More</h1>
							<div class="tiles-grid">
								<div class="row">
									<div class="col-sm-6">
										<div class="panel panel-default panel-idea tile">
											<div class="favourite">
												<div class="checkbox">
													<input type="checkbox" value="" id="favourite1">
													<label for="favourite1"></label>
												</div>
											</div>
											<div class="panel-heading" style="background: url('images/panel_idea.jpg')">

											</div>
											<div class="panel-body panel-article">
												<div class="content-type">
													<span>Article</span>
												</div>
												<div class="title">
													<h1>Bobby McFerrin: Circle Songs from Omega</h1>
												</div>
												<div class="person">
													<ul class="by list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
													<span class="featuring">Featuring:</span>
													<ul class="featuring list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
												</div>
												<div class="duration-date">
													<em class="duration">13 minute watch</em>
													<em class="date-release">13th of September, 2016</em>
												</div>
											</div>

										</div>
									</div>
									<div class="col-sm-6">
										<div class="panel panel-default panel-idea tile">
											<div class="favourite">
												<div class="checkbox">
													<input type="checkbox" value="" id="favourite1">
													<label for="favourite1"></label>
												</div>
											</div>
											<div class="panel-heading" style="background: url('images/panel_idea.jpg')">

											</div>
											<div class="panel-body panel-article">
												<div class="content-type">
													<span>Article</span>
												</div>
												<div class="title">
													<h1>Bobby McFerrin: Circle Songs from Omega</h1>
												</div>
												<div class="person">
													<ul class="by list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
													<span class="featuring">Featuring:</span>
													<ul class="featuring list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
												</div>
												<div class="duration-date">
													<em class="duration">13 minute watch</em>
													<em class="date-release">13th of September, 2016</em>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="panel panel-default panel-idea tile">
											<div class="favourite">
												<div class="checkbox">
													<input type="checkbox" value="" id="favourite1">
													<label for="favourite1"></label>
												</div>
											</div>
											<div class="panel-heading" style="background: url('images/panel_idea.jpg')">

											</div>
											<div class="panel-body panel-article">
												<div class="content-type">
													<span>Article</span>
												</div>
												<div class="title">
													<h1>Bobby McFerrin: Circle Songs from Omega</h1>
												</div>
												<div class="person">
													<ul class="by list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
													<span class="featuring">Featuring:</span>
													<ul class="featuring list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
												</div>
												<div class="duration-date">
													<em class="duration">13 minute watch</em>
													<em class="date-release">13th of September, 2016</em>
												</div>
											</div>

										</div>
									</div>
									<div class="col-sm-6">
										<div class="panel panel-default panel-idea tile">
											<div class="favourite">
												<div class="checkbox">
													<input type="checkbox" value="" id="favourite1">
													<label for="favourite1"></label>
												</div>
											</div>
											<div class="panel-heading" style="background: url('images/panel_idea.jpg')">

											</div>
											<div class="panel-body panel-article">
												<div class="content-type">
													<span>Article</span>
												</div>
												<div class="title">
													<h1>Bobby McFerrin: Circle Songs from Omega</h1>
												</div>
												<div class="person">
													<ul class="by list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
													<span class="featuring">Featuring:</span>
													<ul class="featuring list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
												</div>
												<div class="duration-date">
													<em class="duration">13 minute watch</em>
													<em class="date-release">13th of September, 2016</em>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="panel panel-default panel-idea tile">
											<div class="favourite">
												<div class="checkbox">
													<input type="checkbox" value="" id="favourite1">
													<label for="favourite1"></label>
												</div>
											</div>
											<div class="panel-heading" style="background: url('images/panel_idea.jpg')">

											</div>
											<div class="panel-body panel-article">
												<div class="content-type">
													<span>Article</span>
												</div>
												<div class="title">
													<h1>Bobby McFerrin: Circle Songs from Omega</h1>
												</div>
												<div class="person">
													<ul class="by list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
													<span class="featuring">Featuring:</span>
													<ul class="featuring list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
												</div>
												<div class="duration-date">
													<em class="duration">13 minute watch</em>
													<em class="date-release">13th of September, 2016</em>
												</div>
											</div>

										</div>
									</div>
									<div class="col-sm-6">
										<div class="panel panel-default panel-idea tile">
											<div class="favourite">
												<div class="checkbox">
													<input type="checkbox" value="" id="favourite1">
													<label for="favourite1"></label>
												</div>
											</div>
											<div class="panel-heading" style="background: url('images/panel_idea.jpg')">

											</div>
											<div class="panel-body panel-article">
												<div class="content-type">
													<span>Article</span>
												</div>
												<div class="title">
													<h1>Bobby McFerrin: Circle Songs from Omega</h1>
												</div>
												<div class="person">
													<ul class="by list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
													<span class="featuring">Featuring:</span>
													<ul class="featuring list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
												</div>
												<div class="duration-date">
													<em class="duration">13 minute watch</em>
													<em class="date-release">13th of September, 2016</em>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include ("templates/footer.php"); ?>