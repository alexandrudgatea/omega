<?php include ("templates/header.php"); ?>

	<div id="mainContent" class="workshop-page">
		<div id="hero" class="workshop-page background-image" style="background: url('/images/hero_workshop.jpg')">
			<div class="container full-height">
				<div class="row">
					<div class="col-sm-6 onlinelearn-bg transparent">
						<div class="page-info field-text">
							<h4 class="page-category">Omega Women’s Leadership Center</h4>
							<h1 class="page-title">Jivamukti Yoga Teacher Training Course</h1>
							<div class="date-location">
								<span>Feb 28 - Mar 06 2016</span>
								<span>Rhinebeck, NY</span>
							</div>
							<form class="commerce-add-to-cart commerce-cart-add-to-cart-form-727" action="/workshop-registration/727/13950" method="post" id="commerce-cart-add-to-cart-form-727" accept-charset="UTF-8"><div><input type="hidden" name="product_id" value="727">
<input type="hidden" name="form_build_id" value="form-6qBNdop7pBf06IkhKarhw_ApwlnXOOCLU9ajrwscb4Y">
<input type="hidden" name="form_token" value="xubgYLBQl5AS4GDsQwv3YmbQHpdOT9_WwjZRl_fYkq0">
<input type="hidden" name="form_id" value="commerce_cart_add_to_cart_form_727">
<em>Unlimited slots available</em><div id="edit-line-item-fields" class="form-wrapper form-group"></div><div class="form-item form-item-quantity form-type-select form-group"> <label class="control-label" for="edit-quantity">Quantity</label>
<select class="form-control form-select" id="edit-quantity" name="quantity"><option value="1" selected="selected">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select></div><button type="submit" id="edit-submit" name="op" value="Add to cart" class="btn btn-success form-submit">Add to cart</button>
</div></form>
							<div class="person">
								<p class="short-workshop-info">A 300-plus hour residential program with:</p>
								<ul class="list-unstyled list-inline person-images">
									<!--1st person-->
									<li>
										<div class="image" style="background: url('/images/campus.jpg')"></div>
										<p><a href="#wsTeachers">Peter Parker</a></p>
									</li>

									<!--2nd person-->
									<li>
										<div class="image" style="background: url('/images/person.jpg')"></div>
										<p>Grenadine Wright</p>
									</li>

									<!--3rd person-->
									<li>
										<div class="image" style="background: url('/images/person_pema.jpg')"></div>
										<p>Rene Zenllenger</p>
									</li>

									<!-- +more then 4 person-->
									<li>
										<a href="#!" class="more field-text block text-center full-height"><i class="fa fa-plus"></i></a>
										<p>And More</p>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="pageContent">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="person mobile-only visible-xs">
							<p class="short-workshop-info">A 300-plus hour residential program with:</p>
							<ul class="list-unstyled list-inline person-images">
								<!--1st person-->
								<li>
									<div class="image" style="background: url('/images/campus.jpg')"></div>
									<p><a href="#wsTeachers">Peter Parker</a></p>
								</li>

								<!--2nd person-->
								<li>
									<div class="image" style="background: url('/images/person.jpg')"></div>
									<p>Grenadine Wright</p>
								</li>

								<!--3rd person-->
								<li>
									<div class="image" style="background: url('/images/person_pema.jpg')"></div>
									<p>Rene Zenllenger</p>
								</li>

								<!-- +more then 4 person-->
								<li>
									<a href="#!" class="more field-text block text-center full-height"><i class="fa fa-plus"></i></a>
									<p>And More</p>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3 sidebar clearfix">
						<ul class="list-unstyled" id="sideMenu" data-spy="affix">
							<li class="visible-xs mobile-sidenav-toggle">Jump tp section</li>
							<li class="side-menu">
								<ul class="nav nav-stacked list-unstyled" role="tablist">
									<li><a data-scroll href="#wsDescription">Description</a></li>
									<li><a data-scroll href="#wsMoreInformation">More Information</a></li>
									<li><a data-scroll href="#wsSchedule">Schedule</a></li>
									<li><a data-scroll href="#wsTeachers">Teachers</a></li>
									<li><a data-scroll href="#wsTuition">Tuition &amp; Fees</a></li>
									<li><a data-scroll href="#wsContinuingEducation">Continuing Education</a></li>
									<li><a data-scroll href="#wsScholarships">Scholarships</a></li>
									<li><a data-scroll href="#wsSponsors">Sponsors</a></li>
									<li><a data-scroll href="#wsVisitingOmega">Visiting Omega</a></li>
									<li><a data-scroll href="#wsDiscoverMore">Discover More</a></li>
								</ul>
							</li>
							<li class="register">
								<form class="commerce-add-to-cart commerce-cart-add-to-cart-form-727" action="/workshop-registration/727/13950" method="post" id="commerce-cart-add-to-cart-form-727" accept-charset="UTF-8"><div><input type="hidden" name="product_id" value="727">
<input type="hidden" name="form_build_id" value="form-6qBNdop7pBf06IkhKarhw_ApwlnXOOCLU9ajrwscb4Y">
<input type="hidden" name="form_token" value="xubgYLBQl5AS4GDsQwv3YmbQHpdOT9_WwjZRl_fYkq0">
<input type="hidden" name="form_id" value="commerce_cart_add_to_cart_form_727">
<em>Unlimited slots available</em><div id="edit-line-item-fields" class="form-wrapper form-group"></div><div class="form-item form-item-quantity form-type-select form-group"> <label class="control-label" for="edit-quantity">Quantity</label>
<select class="form-control form-select" id="edit-quantity" name="quantity"><option value="1" selected="selected">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select></div><button type="submit" id="edit-submit" name="op" value="Add to cart" class="btn btn-success form-submit">Add to cart</button>
</div></form>
							</li>
							<li class="visible-xs member-fav"><i class="fa fa-bookmark-o"></i></li>
							<li class="page-share">
								<ul class="list-inline social-links">
									<li>
										<a href="#!">
											<span class="fa-stack fa-lg">
												<i class="fa fa-circle fa-stack-2x"></i>
												<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
											</span>
										</a>
									</li>
									<li>
										<a href="#!">
											<span class="fa-stack fa-lg">
												<i class="fa fa-circle fa-stack-2x"></i>
												<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
											</span>
										</a>
									</li>
									<li>
										<a href="#!">
											<span class="fa-stack fa-lg">
												<i class="fa fa-circle fa-stack-2x"></i>
												<i class="fa fa-envelope-o fa-stack-1x fa-inverse"></i>
											</span>
										</a>
									</li>
									<li>
										<a href="#!">
											<span class="fa-stack fa-lg">
												<i class="fa fa-circle fa-stack-2x"></i>
												<i class="fa fa-share-alt fa-stack-1x fa-inverse"></i>
											</span>
										</a>
									</li>
								</ul>
								<a href="#!" class="btn cta icon-button rounded-corners add-fav"><i class="fa fa-bookmark icon"></i>Add to favorites</a>
							</li>
						</ul>
					</div>
					<div class="col-sm-9 no-padding">
						<div class="page-body">
							<div class="page-text">
								<div id="wsDescription" class="ws-section">
									<div class="workshop-intro">
										Guide the body and soul into spiritual freedom, physical strength, peace of mind, better health, and selfrealization.
									</div>
									<p>
										Jivamukti Yoga, developed by Sharon Gannon and David Life, offers the contemporary yogi a creative approach to living and being in the world today. Grounded in the ethical practices and ancient scriptures of yoga, Jivamukti—which means “living liberated” or “soul liberation” in Sanskrit —is a practical philosophy that teaches how spiritual values can help us work with the challenges of everyday life with more joy and compassion. Join cofounders Sharon Gannon and David Life, along with Emma Henry and Giselle Mari, for a monthlong teacher training in Jivamukti Yoga—the only Jivamukti training in the world confidence to become a certified Jivamukti Yoga teacher.
									</p>
									<div class="carousel-section">
										<div class="dark-bg field-text">
											<div class="text-block">
												<h1 class="field-text section-title">The Photographic Visions of Douglas Beasley</h1>
												<span class="author block">By Briana Boles</span>
												<span class="date block">May 03, 2016</span>
												<span class="description"> Arts Week teacher Douglas Beasley is founder and director of Vision Quest Photo Workshops. As an artist, he believes that we always have the opportunity to transform an emotional  experience into a visual one.</span>
											</div>

											<div id="articleCarousel" class="carousel slide carousel-fade" data-ride="carousel">
												<ol class="carousel-indicators">
													<li data-target="#articleCarousel" data-slide-to="0" class="active"> </li>
													<li data-target="#articleCarousel" data-slide-to="1"> </li>
													<li data-target="#articleCarousel" data-slide-to="2"> </li>
												</ol>
												<div class="carousel-inner">
													<div class="item active">
														<img src="images/hero.jpeg" alt="First slide" />
													</div>
													<div class="item">
														<img src="images/hero2.jpeg" alt="Second slide" />
													</div>
													<div class="item">
														<img src="images/hero3.jpeg" alt="Third slide" />
													</div>
												</div>
												<a class="left carousel-control" href="#articleCarousel" data-slide="prev">

												</a>
												<a class="right carousel-control" href="#articleCarousel" data-slide="next">

												</a>
											</div>
											<div class="text-block">
												<span class="description">
								Douglas describes his own journey as one through religion toward spirituality, and he credits
photography as helping him become more authentic in his own vision. 
								<br><br>
								“My work as a photographer is about seeking the sacred,” he says, “looking to find the divine in everything. My photography is about recognizing the spirit that inhabits all things, animate and inanimate, big and small.”
								</span>
											</div>
										</div>
									</div>
								</div>
								<div id="wsMoreInformation" class="ws-section">
									<h1 class="section-title">More Information</h1>
									<p>Required Forms</p>
									<ul class="list-unstyled">
										<li><a href="#!">Jivamukti Yoga® Teachers Code of Professional Standards (Ethical Guidelines)</a></li>
										<li><a href="#!">Jivamukti Yoga® Teacher Certification Agreement</a></li>
										<li><a href="#!">Medical Information Form</a></li>
										<li><a href="#!">Personal History Form</a></li>
									</ul>

									<p>Resources</p>
									<ul class="list-unstyled">
										<li><a href="#!">Video: Yoga and Mindfullness</a></li>
										<li><a href="#!">Online Course: Proin Gravida Dolor Sit Amet Lacus Accumsan</a></li>
										<li><a href="#!">Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</a></li>
									</ul>
								</div>
								<div id="wsSchedule" class="ws-section">
									<h1 class="section-title">Schedule</h1>
									<ul class="schedule list-unstyled">
										<li class="schedule-day">
											<div class="day">
												Friday <span class="date">Mar 28</span>
											</div>
											<div class="schedule-row row">
												<div class="time-interval col-xs-12 col-sm-5">4:00 p.m. – 6:30 p.m.</div>
												<div class="schedule-task col-xs-12 col-sm-7">Check In (rooms available after 5:00 p.m.)</div>
											</div>
											<div class="schedule-row row">
												<div class="time-interval col-xs-12 col-sm-5">8:00 p.m. – 12:30 a.m.</div>
												<div class="schedule-task col-xs-12 col-sm-7">Workshop</div>
											</div>
										</li>
										<li class="schedule-day">
											<div class="day">
												Saturday <span class="date">Mar 29</span>
											</div>
											<div class="schedule-row row">
												<div class="time-interval col-xs-12 col-sm-5">9:00 a.m. – Noon</div>
												<div class="schedule-task col-xs-12 col-sm-7">Workshop</div>
											</div>
											<div class="schedule-row row">
												<div class="time-interval col-xs-12 col-sm-5">2:30 p.m. – 5:00 p.m.</div>
												<div class="schedule-task col-xs-12 col-sm-7">Workshop</div>
											</div>
											<div class="schedule-row row">
												<div class="time-interval col-xs-12 col-sm-5">8:00 p.m. – 12:30 a.m. (Sunday)</div>
												<div class="schedule-task col-xs-12 col-sm-7">Workshop</div>
											</div>
										</li>
										<li class="schedule-day">
											<div class="day">
												Sunday <span class="date">Mar 30</span>
											</div>
											<div class="schedule-row row">
												<div class="time-interval col-xs-12 col-sm-5">9:00 a.m. – 12:30 p.m.</div>
												<div class="schedule-task col-xs-12 col-sm-7">Workshop</div>
											</div>
											<div class="schedule-row row">
												<div class="time-interval col-xs-12 col-sm-5">12:30 p.m. – 1:30 p.m.</div>
												<div class="schedule-task col-xs-12 col-sm-7">Check Out</div>
											</div>
										</li>
									</ul>
								</div>
								<div id="wsTeachers" class="ws-section">
									<h1 class="section-title">Meet the Performers</h1>
									<ul class="list-inline list-unstyled teachers-grid">
										<li>
											<div class="teacher-image" style="background: url('/images/person.jpg')">
												<a href="http://google.com"></a>
											</div>
											<div class="teacher-name"><a href="#!">Peter Parker</a></div>
											<div class="teacher-bio">Armando has been working in the world of meditation and spirituality for 25 years…</div>
										</li>
										<li>
											<div class="teacher-image" style="background: url('/images/person.jpg')"></div>
											<div class="teacher-name">Peter Parker</div>
											<div class="teacher-bio">Armando has been working in the world of meditation and spirituality for 25 years…</div>
										</li>
										<li>
											<div class="teacher-image" style="background: url('/images/person.jpg')"></div>
											<div class="teacher-name">Peter Parker</div>
											<div class="teacher-bio">Armando has been working in the world of meditation and spirituality for 25 years…</div>
										</li>
										<li>
											<div class="teacher-image" style="background: url('/images/person.jpg')"></div>
											<div class="teacher-name">Peter Parker</div>
											<div class="teacher-bio">Armando has been working in the world of meditation and spirituality for 25 years…</div>
										</li>
										<li>
											<div class="teacher-image" style="background: url('/images/person.jpg')"></div>
											<div class="teacher-name">Peter Parker</div>
											<div class="teacher-bio">Armando has been working in the world of meditation and spirituality for 25 years…</div>
										</li>
										<li>
											<div class="teacher-image" style="background: url('/images/person.jpg')"></div>
											<div class="teacher-name">Peter Parker</div>
											<div class="teacher-bio">Armando has been working in the world of meditation and spirituality for 25 years…</div>
										</li>
										<li>
											<div class="teacher-image" style="background: url('/images/person.jpg')"></div>
											<div class="teacher-name">Peter Parker</div>
											<div class="teacher-bio">Armando has been working in the world of meditation and spirituality for 25 years…</div>
										</li>
									</ul>
									<a href="#!" class="hidden see-more text-center block">See More +</a>
								</div>
								<div id="wsTuition" class="ws-section">
									<div class="row">
										<div class="col-xs-6">
											<h1 class="section-title">Tuition &amp; Fees</h1>
										</div>
										<div class="col-xs-6 text-right">
											<form class="commerce-add-to-cart commerce-cart-add-to-cart-form-727" action="/workshop-registration/727/13950" method="post" id="commerce-cart-add-to-cart-form-727" accept-charset="UTF-8"><div><input type="hidden" name="product_id" value="727">
<input type="hidden" name="form_build_id" value="form-6qBNdop7pBf06IkhKarhw_ApwlnXOOCLU9ajrwscb4Y">
<input type="hidden" name="form_token" value="xubgYLBQl5AS4GDsQwv3YmbQHpdOT9_WwjZRl_fYkq0">
<input type="hidden" name="form_id" value="commerce_cart_add_to_cart_form_727">
<em>Unlimited slots available</em><div id="edit-line-item-fields" class="form-wrapper form-group"></div><div class="form-item form-item-quantity form-type-select form-group"> <label class="control-label" for="edit-quantity">Quantity</label>
<select class="form-control form-select" id="edit-quantity" name="quantity"><option value="1" selected="selected">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select></div><button type="submit" id="edit-submit" name="op" value="Add to cart" class="btn btn-success form-submit">Add to cart</button>
</div></form>
										</div>
									</div>
									<div class="panel-group" id="feesAccordion" role="tablist" aria-multiselectable="true">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="option1">
												<h4 class="panel-title">
													<a class="collapsed" role="button" data-toggle="collapse" data-parent="#feesAccordion" href="#suboption1" aria-expanded="false" aria-controls="suboption1">
													  Tuition
													</a>
												 </h4>
											</div>
											<div id="suboption1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="option1">
												<div class="panel-body">
													<p>Early Bird: Use promo code JYTD until Dec 21 to receive a $200 discount.</p>
													<table class="table">
														<tbody>
															<tr>
																<td>
																	<a href="#!">
																		<p>Member <i class="fa fa-info-circle"></i></p>
																	</a>
																</td>
																<td>$6,000</td>
															</tr>
															<tr>
																<td>
																	<a href="#!">
																		<p>Nonmember <i class="fa fa-info-circle"></i></p>
																	</a>
																</td>
																<td>$6,650</td>
															</tr>
														</tbody>
													</table>

												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="option2">
												<h4 class="panel-title">
													<a class="collapsed" role="button" data-toggle="collapse" data-parent="#feesAccordion" href="#suboption2" aria-expanded="false" aria-controls="suboption2">
													  Stay With Us
													</a>
												</h4>
											</div>
											<div id="suboption2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="option2">
												<div class="panel-body">
													<p>
														From deluxe cabins to camping sites with shared baths, we have accommodations to suit every budget. Price includes lodgings; three delicious, healthy meals per day, and an array of activities to enrich your stay with us.
													</p>
													<table class="table table-striped">
														<thead>
															<tr>
																<th>TYPE</th>
																<th>Rate - 26 Nights</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>
																	<a href="#!" data-toggle="modal" data-target="#accomodationModal">
																		<p>Deluxe Double Cabin<i class="fa fa-info-circle"></i></p>
																		<p>Shared room, 2 double beds, private bath, AC</p>
																	</a>
																</td>
																<td>$5,888.00</td>
															</tr>
															<tr>
																<td>
																	<a href="#!" data-toggle="modal" data-target="#accomodationModal">
																		<p>Deluxe Double Cabin<i class="fa fa-info-circle"></i></p>
																		<p>Shared room, 2 double beds, private bath, AC</p>
																	</a>
																</td>
																<td>$5,888.00</td>
															</tr>
															<tr>
																<td>
																	<a href="#!" data-toggle="modal" data-target="#accomodationModal">
																		<p>Deluxe Double Cabin<i class="fa fa-info-circle"></i></p>
																		<p>Shared room, 2 double beds, private bath, AC</p>
																	</a>
																</td>
																<td>$5,888.00</td>
															</tr>
															<tr>
																<td>
																	<a href="#!" data-toggle="modal" data-target="#accomodationModal">
																		<p>Deluxe Double Cabin<i class="fa fa-info-circle"></i></p>
																		<p>Shared room, 2 double beds, private bath, AC</p>
																	</a>
																</td>
																<td>$5,888.00</td>
															</tr>
															<tr>
																<td>
																	<a href="#!" data-toggle="modal" data-target="#accomodationModal">
																		<p>Deluxe Double Cabin<i class="fa fa-info-circle"></i></p>
																		<p>Shared room, 2 double beds, private bath, AC</p>
																	</a>
																</td>
																<td>$5,888.00</td>
															</tr>
															<tr>
																<td>
																	<a href="#!" data-toggle="modal" data-target="#accomodationModal">
																		<p>Deluxe Double Cabin<i class="fa fa-info-circle"></i></p>
																		<p>Shared room, 2 double beds, private bath, AC</p>
																	</a>
																</td>
																<td>$5,888.00</td>
															</tr>
															<tr>
																<td>
																	<a href="#!" data-toggle="modal" data-target="#accomodationModal">
																		<p>Deluxe Double Cabin<i class="fa fa-info-circle"></i></p>
																		<p>Shared room, 2 double beds, private bath, AC</p>
																	</a>
																</td>
																<td>$5,888.00</td>
															</tr>
															<tr>
																<td>
																	<a href="#!" data-toggle="modal" data-target="#accomodationModal">
																		<p>Deluxe Double Cabin<i class="fa fa-info-circle"></i></p>
																		<p>Shared room, 2 double beds, private bath, AC</p>
																	</a>
																</td>
																<td>$5,888.00</td>
															</tr>
															<tr>
																<td>
																	<a href="#!" data-toggle="modal" data-target="#accomodationModal">
																		<p>Deluxe Double Cabin<i class="fa fa-info-circle"></i></p>
																		<p>Shared room, 2 double beds, private bath, AC</p>
																	</a>
																</td>
																<td>$5,888.00</td>
															</tr>
															<tr>
																<td>
																	<a href="#!" data-toggle="modal" data-target="#accomodationModal">
																		<p>Deluxe Double Cabin<i class="fa fa-info-circle"></i></p>
																		<p>Shared room, 2 double beds, private bath, AC</p>
																	</a>
																</td>
																<td>$5,888.00</td>
															</tr>
														</tbody>
													</table>

												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="option3">
												<h4 class="panel-title">
													<a class="collapsed" role="button" data-toggle="collapse" data-parent="#feesAccordion" href="#suboption3" aria-expanded="false" aria-controls="suboption3">
													  Commute
													</a>
												 </h4>
											</div>
											<div id="suboption3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="option1">
												<div class="panel-body">

												</div>
											</div>
										</div>
									</div>
									<!-- tuition accomodation modal-->
									<!-- Modal -->
									<div class="modal fade" id="accomodationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<i class="fa fa-times"></i>
													</button>
													<h4 class="modal-title">Delux Double Cabin</h4>
												</div>
												<div class="modal-body">
													<div id="unit-carousel" class="carousel slide" data-ride="carousel">
														<!-- Wrapper for slides -->
														<div class="carousel-inner" role="listbox">
															<div class="item active background-image" style="background-image: url('images/hero3.jpeg')"></div>
															<div class="item background-image" style="background-image: url('images/hero.jpeg')"></div>
															<div class="item background-image" style="background-image: url('images/hero2.jpeg')"></div>
														</div>

														<!-- Controls -->
														<a class="left carousel-control" href="#unit-carousel" role="button" data-slide="prev">
															<span class="glyphicon fa fa-angle-left" aria-hidden="true"></span>
															<span class="sr-only">Previous</span>
														</a>
														<a class="right carousel-control" href="#unit-carousel" role="button" data-slide="next">
															<span class="glyphicon fa fa-angle-right" aria-hidden="true"></span>
															<span class="sr-only">Next</span>
														</a>
													</div>
													<div class="unit-details">
														<div class="unit-price"><strong>(Price – X nights)</strong></div>
														<div class="unit-description">
															<p>Shared Room with Private Bath 2 double beds. </p>
															<p>Heat. Air-conditioning. Wheelchair-accessible rooms available. </p>
															<p>Register with a companion, or a roommate will be assigned to you. </p>
														</div>
													</div>
												</div>
												<!--
												<div class="modal-footer">
													<button type="button" class="btn btn-default cta">Book Now <i class="fa fa-bookmark"></i> </button>
												</div>
-->
											</div>
										</div>
									</div>
								</div>
								<div id="wsContinuingEducation" class="ws-section">
									<h1 class="section-title">Continuing Education</h1>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>

									<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
								</div>
								<div id="wsScholarships" class="ws-section">
									<h1 class="section-title">Scolarship</h1>
									<p>Maecenas nec arcu sed libero ultricies tristique. Phasellus in laoreet nibh. Phasellus mi arcu, finibus sit amet urna sit amet, lacinia convallis neque. Nullam gravida tortor quis sollicitudin faucibus. Suspendisse in consequat dui. Integer in interdum nibh. Pellentesque commodo tempus scelerisque.</p>
								</div>
								<div id="wsSponsors" class="ws-section">
									<h1 class="section-title">Sponsors</h1>
									<div class="row">
										<div class="col-sm-12 text-center">
											<ul class="sponsor-list list-inline">
												<li>
													<a href="#!" target="_blank"><img src="images/sponsor1.png" alt="Sponsor 1"></a>
												</li>
												<li>
													<a href="#!" target="_blank"><img src="images/sponsor2.png"></a>
												</li>
												<li>
													<a href="#!" target="_blank"><img src="images/sponsor3.png"></a>
												</li>
												<li>
													<a href="#!" target="_blank"><img src="images/sponsor4.png"></a>
												</li>
												<li>
													<a href="#!" target="_blank"><img src="images/sponsor5.png"></a>
												</li>
												<li>
													<a href="#!" target="_blank"><img src="images/sponsor6.png"></a>
												</li>
												<li>
													<a href="#!" target="_blank"><img src="images/sponsor7.png"></a>
												</li>
												<li>
													<a href="#!" target="_blank"><img src="images/sponsor8.png"></a>
												</li>

											</ul>
											<ul class="sponsor-text-only">
												<li>
													<a href="#!" target="_blank">Sponsor Title Just Text</a>
												</li>
												<li>
													<a href="#!" target="_blank">Sponsor Title That is Longer</a>
												</li>
												<li>
													<a href="#!" target="_blank">Short Sponsor</a>
												</li>
												<li>
													<a href="#!" target="_blank">Simple Sonsor Title</a>
												</li>
												<li>
													<a href="#!" target="_blank">Sponsor Title Just Text</a>
												</li>
												<li>
													<a href="#!" target="_blank">Sponsor Title That is Longer</a>
												</li>
												<li>
													<a href="#!" target="_blank">Short Sponsor</a>
												</li>
												<li>
													<a href="#!" target="_blank">Simple Sonsor Title</a>
												</li>
											</ul>

										</div>
									</div>
								</div>
								<div id="wsVisitingOmega" class="ws-section">
									<h1 class="section-title">Visiting Omega</h1>
									<div class="row">
										<div class="col-sm-6">
											<div class="tile" style="background: url('/images/visit_omega1.jpg')">
												<a href="#!">Accomodations</a>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="tile" style="background: url('/images/visit_omega3.jpg')">
												<a href="#!">Planning your Stay</a>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="tile" style="background: url('/images/visit_omega2.jpg')">
												<a href="#!">Visit FAQ</a>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="tile" style="background: url('/images/visit_omega4.jpg')">
												<a href="#!">Wellness Center</a>
											</div>
										</div>
									</div>
								</div>
								<div id="wsDiscoverMore" class="ws-section">
									<div class="discover-more">
										<h1 class="section-title">Discover More</h1>
										<div class="tiles-grid">
											<div class="related">
												<h3 class="related-tiles">Related Workshops</h3>
												<div class="row">
													<div class="col-sm-6">
														<div class="panel panel-default tile  panel-workshop">
															<div class="favourite">
																<div class="checkbox">
																	<input type="checkbox" value="" id="favourite4">
																	<label for="favourite4"></label>
																</div>
															</div>
															<div class="panel-body">
																<div class="content-type">
																	<span>Workshop</span>
																</div>
																<div class="title">
																	<h1>Collaborating With Nature</h1>
																</div>
																<div class="person">
																	<ul class="list-unstyled list-inline person-images">
																		<li>
																			<img src="images/person.jpg">
																		</li>
																		<li> <img src="images/person.jpg"></li>
																	</ul>
																	<ul class="list-unstyled list-inline person-names">
																		<li>Thomas Robertson</li>
																		<li>Marry Anderson</li>
																	</ul>
																</div>
																<div class="date-location">
																	<ul class="list-inline">
																		<li><em>Feb 28 - Mar 06 2016</em></li>
																		<li><em>Rhinebeck, NY</em></li>
																	</ul>
																</div>
															</div>
															<div class="panel-footer">
																<a href="#!" class="expand-button">
																	<i class="fa fa-plus"></i>
																</a>
																<div class="expanded-tile-content animated fadeInUp">
																	<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
																	<a href="#!" class="btn white-transparent-button">Learn More</a>
																</div>
															</div>
														</div>
													</div>
													<div class="col-sm-6">
														<div class="panel panel-default tile  panel-workshop small">
															<div class="favourite">
																<div class="checkbox">
																	<input type="checkbox" value="" id="favourite4">
																	<label for="favourite4"></label>
																</div>
															</div>
															<div class="panel-body">
																<div class="content-type">
																	<span>Workshop</span>
																</div>
																<div class="title">
																	<h1>Collaborating With Nature</h1>
																</div>
																<div class="person">
																	<ul class="list-unstyled list-inline person-images">
																		<li>
																			<img src="images/person.jpg">
																		</li>
																		<li> <img src="images/person.jpg"></li>
																	</ul>
																	<ul class="list-unstyled list-inline person-names">
																		<li>Thomas Robertson</li>
																		<li>Marry Anderson</li>
																	</ul>
																</div>
																<div class="date-location">
																	<ul class="list-inline">
																		<li><em>Feb 28 - Mar 06 2016</em></li>
																		<li><em>Rhinebeck, NY</em></li>
																	</ul>
																</div>
															</div>
															<div class="panel-footer">
																<a href="#!" class="expand-button">
																	<i class="fa fa-plus"></i>
																</a>
																<div class="expanded-tile-content animated fadeInUp">
																	<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
																	<a href="#!" class="btn white-transparent-button">Learn More</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="related">
												<h3 class="related-tiles">Related Ideas</h3>
												<div class="row">
													<div class="col-sm-6">
														<div class="panel panel-default panel-idea tile">
															<div class="favourite">
																<div class="checkbox">
																	<input type="checkbox" value="" id="favourite1">
																	<label for="favourite1"></label>
																</div>
															</div>
															<div class="panel-heading" style="background: url('images/panel_idea.jpg')">

															</div>
															<div class="panel-body panel-article">
																<div class="content-type">
																	<span>Article</span>
																</div>
																<div class="title">
																	<h1>Bobby McFerrin: Circle Songs from Omega</h1>
																</div>
																<div class="person">
																	<ul class="by list-unstyled list-inline">
																		<li>Thomas Robertson</li>
																		<li>Marry Anderson</li>
																	</ul>
																	<span class="featuring">Featuring:</span>
																	<ul class="featuring list-unstyled list-inline">
																		<li>Thomas Robertson</li>
																		<li>Marry Anderson</li>
																	</ul>
																</div>
																<div class="duration-date">
																	<em class="duration">13 minute watch</em>
																	<em class="date-release">13th of September, 2016</em>
																</div>
															</div>

														</div>
													</div>
													<div class="col-sm-6">
														<div class="panel panel-default panel-idea tile small">
															<div class="favourite">
																<div class="checkbox">
																	<input type="checkbox" value="" id="favourite1">
																	<label for="favourite1"></label>
																</div>
															</div>
															<div class="panel-heading" style="background: url('images/panel_idea.jpg')">

															</div>
															<div class="panel-body panel-article">
																<div class="content-type">
																	<span>Article</span>
																</div>
																<div class="title">
																	<h1>Bobby McFerrin: Circle Songs from Omega</h1>
																</div>
																<div class="person">
																	<ul class="by list-unstyled list-inline">
																		<li>Thomas Robertson</li>
																		<li>Marry Anderson</li>
																	</ul>
																	<span class="featuring">Featuring:</span>
																	<ul class="featuring list-unstyled list-inline">
																		<li>Thomas Robertson</li>
																		<li>Marry Anderson</li>
																	</ul>
																</div>
																<div class="duration-date">
																	<em class="duration">13 minute watch</em>
																	<em class="date-release">13th of September, 2016</em>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-6">
														<div class="panel panel-default panel-idea tile small">
															<div class="favourite">
																<div class="checkbox">
																	<input type="checkbox" value="" id="favourite1">
																	<label for="favourite1"></label>
																</div>
															</div>
															<div class="panel-heading" style="background: url('images/panel_idea.jpg')">

															</div>
															<div class="panel-body panel-article">
																<div class="content-type">
																	<span>Article</span>
																</div>
																<div class="title">
																	<h1>Bobby McFerrin: Circle Songs from Omega</h1>
																</div>
																<div class="person">
																	<ul class="by list-unstyled list-inline">
																		<li>Thomas Robertson</li>
																		<li>Marry Anderson</li>
																	</ul>
																	<span class="featuring">Featuring:</span>
																	<ul class="featuring list-unstyled list-inline">
																		<li>Thomas Robertson</li>
																		<li>Marry Anderson</li>
																	</ul>
																</div>
																<div class="duration-date">
																	<em class="duration">13 minute watch</em>
																	<em class="date-release">13th of September, 2016</em>
																</div>
															</div>

														</div>
													</div>
													<div class="col-sm-6">
														<div class="panel panel-default panel-idea tile small">
															<div class="favourite">
																<div class="checkbox">
																	<input type="checkbox" value="" id="favourite1">
																	<label for="favourite1"></label>
																</div>
															</div>
															<div class="panel-heading" style="background: url('images/panel_idea.jpg')">

															</div>
															<div class="panel-body panel-article">
																<div class="content-type">
																	<span>Article</span>
																</div>
																<div class="title">
																	<h1>Bobby McFerrin: Circle Songs from Omega</h1>
																</div>
																<div class="person">
																	<ul class="by list-unstyled list-inline">
																		<li>Thomas Robertson</li>
																		<li>Marry Anderson</li>
																	</ul>
																	<span class="featuring">Featuring:</span>
																	<ul class="featuring list-unstyled list-inline">
																		<li>Thomas Robertson</li>
																		<li>Marry Anderson</li>
																	</ul>
																</div>
																<div class="duration-date">
																	<em class="duration">13 minute watch</em>
																	<em class="date-release">13th of September, 2016</em>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php include ("templates/footer.php"); ?>