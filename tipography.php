<?php include ("templates/header.php"); ?>


<div id="mainContent">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<ul class="discover-more-tiles list-unstyled hidden-xs">
					<li class="search-tile">
						<div class="tile-image">
							<img src="images/collection_tile.jpg" class="img-responsive">
						</div>
						<div class="tile-content">
							<div class="content-type">
								<a href="#!"><i class="fa fa-play-circle-o" aria-hidden="true"></i></a>
								<span>Video</span>
							</div>
							<h2 class="title interractive-text">How do you establish trust in a relationship</h2>
							<span class="tile-text block">Chödrön is the resident teacher at Gampo
Abbey in Cape Breton, Nova Scotia, the first Tibetan monastery for Westerners. An American Buddhist nun. </span>
							<div class="bottom-info">
								<span class="featured">Featuring: Featuring Richard Borofsky, Peter Parker, andAntra Kalnins Borofsky | <span class="video-duration">00:43:23</span>
								</span>
							</div>
						</div>
					</li>
					<li class="search-tile">
						<div class="tile-image">
							<img src="images/collection_tile2.jpg" class="img-responsive">
						</div>
						<div class="tile-content">
							<div class="content-type">
								<a href="#!"><i class="fa fa-play-circle-o" aria-hidden="true"></i></a>
								<span>Video</span>
							</div>
							<h2 class="title interractive-text">The Courage to Choose</h2>
							<span class="tile-text block">Chödrön is the resident teacher at Gampo
Abbey in Cape Breton, Nova Scotia, the first Tibetan monastery for Westerners. An American Buddhist nun. </span>
							<div class="bottom-info">
								<span class="featured">By Elizabeth Gilbert</span>
							</div>
						</div>
					</li>
					<li class="search-tile">
						<div class="tile-image">
							<img src="images/collection_tile3.jpg" class="img-responsive">
						</div>
						<div class="tile-content">
							<div class="content-type">
								<a href="#!"><i class="fa fa-play-circle-o" aria-hidden="true"></i></a>
								<span>Video</span>
							</div>
							<h2 class="title interractive-text">Reclaim Your Inner Eden</h2>
							<span class="tile-text block">Chödrön is the resident teacher at Gampo
Abbey in Cape Breton, Nova Scotia, the first Tibetan monastery for Westerners. An American Buddhist nun. </span>
							<div class="bottom-info">
								<span class="featured">By Katina I. Makris and Suzanne Kingsbury </span>
							</div>
						</div>
					</li>
				</ul>
				<div class="alert alert-block alert-danger messages error">
					<a class="close" data-dismiss="alert" href="#">×</a>
					<ul>
						<li>Email Address field is required.</li>
						<li>Confirm Email Address field is required.</li>
						<li>First Name field is required.</li>
						<li>Last Name field is required.</li>
						<li>I am most interested in learning about: field is required.</li>
						<li>How Did You Find Out About Omega? field is required.</li>
					</ul>
				</div>
				<div class="alert alert-block alert-success messages error">
					<a class="close" data-dismiss="alert" href="#">×</a>
					<ul>
						<li>Email Address field is required.</li>
						<li>Confirm Email Address field is required.</li>
						<li>First Name field is required.</li>
						<li>Last Name field is required.</li>
						<li>I am most interested in learning about: field is required.</li>
						<li>How Did You Find Out About Omega? field is required.</li>
					</ul>
				</div>
				<div class="alert alert-block alert-info messages error">
					<a class="close" data-dismiss="alert" href="#">×</a>
					<ul>
						<li>Email Address field is required.</li>
						<li>Confirm Email Address field is required.</li>
						<li>First Name field is required.</li>
						<li>Last Name field is required.</li>
						<li>I am most interested in learning about: field is required.</li>
						<li>How Did You Find Out About Omega? field is required.</li>
					</ul>
				</div>
				<div class="alert alert-block alert-warning messages error">
					<a class="close" data-dismiss="alert" href="#">×</a>
					<ul>
						<li>Email Address field is required.</li>
						<li>Confirm Email Address field is required.</li>
						<li>First Name field is required.</li>
						<li>Last Name field is required.</li>
						<li>I am most interested in learning about: field is required.</li>
						<li>How Did You Find Out About Omega? field is required.</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 field" style="background: #fff;padding: 30px;">
				<div style="padding: 40px 0;">
					<h1>Typography</h1>
				</div>

				<h1>Heading 1</h1>
				<h2>Heading 2</h2>
				<h3>Heading 3</h3>
				<h4>Heading 4</h4>
				<br/>
				<hr>
				<p>This is a paragraph Nostrud noster sunt in esse an qui lorem tamen varias consequat do fabulas anim enim pariatur illum, do do quae admodum sed ullamco ut sunt quibusdam aut aut fugiat minim ipsum vidisse, fugiat occaecat nescius te sed varias ingeniis. Cillum mentitum te praetermissum de ex dolor ullamco quibusdam.</p>
				<em>This is italic font. </em>Example:
				<code><em>This is italic font.</em></code>
				<br/>
				<em class="serif">To give it the serif font just add class <code>serif</code> to it. </em>Example:
				<code><em class="serif">This is italic font.</em></code>
				<p><a href="http://">This is a link</a> The link &lt;a href="http:// [...]"><em>link here</em> &lt; /a &gt; tag is wrapped inside a &lt; p &gt; to inherit it's size</p>
				<br/>
				<p><strong><em>Below is a list:</em></strong></p>
				<div id="edit-email-list-join" class="form-radios">
					<div class="form-item form-item-email-list-join form-type-radio radio"> <label class="control-label" for="edit-email-list-join-y"><input type="radio" id="edit-email-list-join-y" name="email_list_join" value="Y" checked="checked" class="form-radio">Yes, I want to join the eOmega Community to receive exclusive discounts, special invitations, and your monthly eNews.</label>
					</div>
					<div class="form-item form-item-email-list-join form-type-radio radio"> <label class="control-label" for="edit-email-list-join-"><input type="radio" id="edit-email-list-join-" name="email_list_join" value="" class="form-radio">I am already subscribed to your Email List</label>
					</div>
					<div class="form-item form-item-email-list-join form-type-radio radio"> <label class="control-label" for="edit-email-list-join-n"><input type="radio" id="edit-email-list-join-n" name="email_list_join" value="N" class="form-radio">No thanks.</label>
					</div>
				</div>
				<div class="form-item form-item-other-categories form-type-checkboxes form-group"> <label class="control-label" for="edit-other-categories">The Learning Paths and other categories I would like to learn about are:</label>
					<div id="edit-other-categories" class="form-checkboxes">
						<div class="form-item form-item-other-categories-1 form-type-checkbox checkbox"> <label class="control-label" for="edit-other-categories-1"><input type="checkbox" id="edit-other-categories-1" name="other_categories[1]" value="1" class="form-checkbox">Health &amp; Healing - I'm Trying to Live a Healthier Life</label>
						</div>
						<div class="form-item form-item-other-categories-2 form-type-checkbox checkbox"> <label class="control-label" for="edit-other-categories-2"><input type="checkbox" id="edit-other-categories-2" name="other_categories[2]" value="2" class="form-checkbox">Creative Expression - I Feel Creativity Calling</label>
						</div>
						<div class="form-item form-item-other-categories-3 form-type-checkbox checkbox"> <label class="control-label" for="edit-other-categories-3"><input type="checkbox" id="edit-other-categories-3" name="other_categories[3]" value="3" class="form-checkbox">Leadership &amp; Work - I Aspire to Do Meaningful Work</label>
						</div>
						<div class="form-item form-item-other-categories-4 form-type-checkbox checkbox"> <label class="control-label" for="edit-other-categories-4"><input type="checkbox" id="edit-other-categories-4" name="other_categories[4]" value="4" class="form-checkbox">Body, Mind, &amp; Spirit - I'm Looking for Balance</label>
						</div>
						<div class="form-item form-item-other-categories-5 form-type-checkbox checkbox"> <label class="control-label" for="edit-other-categories-5"><input type="checkbox" id="edit-other-categories-5" name="other_categories[5]" value="5" class="form-checkbox">Relationships &amp; Family - I'd Like to Improve My Relationships</label>
						</div>
						<div class="form-item form-item-other-categories-6 form-type-checkbox checkbox"> <label class="control-label" for="edit-other-categories-6"><input type="checkbox" id="edit-other-categories-6" name="other_categories[6]" value="6" class="form-checkbox">Sustainable Living - I Want to Get Back to Nature</label>
						</div>
						<div class="form-item form-item-other-categories-7 form-type-checkbox checkbox"> <label class="control-label" for="edit-other-categories-7"><input type="checkbox" id="edit-other-categories-7" name="other_categories[7]" value="7" class="form-checkbox">Professional Trainings</label>
						</div>
						<div class="form-item form-item-other-categories-11 form-type-checkbox checkbox"> <label class="control-label" for="edit-other-categories-11"><input type="checkbox" id="edit-other-categories-11" name="other_categories[11]" value="11" class="form-checkbox">Yoga</label>
						</div>
						<div class="form-item form-item-other-categories-8 form-type-checkbox checkbox"> <label class="control-label" for="edit-other-categories-8"><input type="checkbox" id="edit-other-categories-8" name="other_categories[8]" value="8" class="form-checkbox">Omega Women's Leadership Center (OWLC)</label>
						</div>
						<div class="form-item form-item-other-categories-9 form-type-checkbox checkbox"> <label class="control-label" for="edit-other-categories-9"><input type="checkbox" id="edit-other-categories-9" name="other_categories[9]" value="9" class="form-checkbox">Gay &amp; Lesbian Workshops</label>
						</div>
						<div class="form-item form-item-other-categories-10 form-type-checkbox checkbox"> <label class="control-label" for="edit-other-categories-10"><input type="checkbox" id="edit-other-categories-10" name="other_categories[10]" value="10" class="form-checkbox">Men's Programs</label>
						</div>
						<div class="form-item form-item-other-categories-12 form-type-checkbox checkbox"> <label class="control-label" for="edit-other-categories-12"><input type="checkbox" id="edit-other-categories-12" name="other_categories[12]" value="12" class="form-checkbox">Online Learning</label>
						</div>
						<div class="form-item form-item-other-categories-13 form-type-checkbox checkbox"> <label class="control-label" for="edit-other-categories-13"><input type="checkbox" id="edit-other-categories-13" name="other_categories[13]" value="13" class="form-checkbox">Omega Center for Sustainable Living (OCSL)</label>
						</div>
					</div>
				</div>
				<ul>
					<li>
						<p>This is a paragraph item</p>
					</li>
					<li>
						<p><a href="http://">This is a link item</a></p>
					</li>
					<li><em class="serif">This is an italic text with serif font</em></li>
				</ul>

				<hr>
				<p class="font-300">This paragraph has <code>font-weight: 300;</code></p>
				<p class="font-400">This paragraph has <code>font-weight: 400;</code></p>
				<p class="font-700">This paragraph has <code>font-weight: 700;</code></p>

				<hr>
				<code>To align an image to left just use <p class='text-left'><img src='images/initiative3.jpg'></p></code>

				<p class="text-left"><img src="images/initiative3.jpg"></p>

				<code>To align an image to center just use <p class="text-center"><img src="images/initiative3.jpg"></p></code>
				<p class="text-center"><img src="images/initiative3.jpg"></p>

				<code>To align an image to right just use <p class="text-right"><img src="images/initiative3.jpg"></p></code>
				<p class="text-right"><img src="images/initiative3.jpg"></p>
				<p>Appellat aut laborum, quid quamquam exercitation, magna exquisitaque mandaremus veniam cupidatat et iis dolor consequat ab nam summis eram elit expetendis, probant sint qui singulis concursionibus, ut se concursionibus an laboris sint vidisse cupidatat. Quid senserit voluptatibus, dolore in do magna nescius, se eiusmod sempiternum, non ubi adipisicing. Esse hic eiusmod. Quamquam sed labore, laborum ex incurreret, ab iis concursionibus in de mentitum coniunctione. Cupidatat est aliqua ingeniis se lorem adipisicing fabulas dolore offendit. Malis vidisse appellat, do senserit adipisicing hic ubi dolor quamquam, doctrina duis multos ne quorum. Voluptate ita possumus, te quem vidisse. Minim laborum ad aliqua sint ubi anim possumus distinguantur, tamen occaecat incurreret aut vidisse summis probant ingeniis aut nulla hic ab enim officia ita ea quo efflorescere, ab amet laborum praesentibus, legam si sed multos laborum.</p>
				<hr>
				<p>
					Floated images have the following markup
					<br/>
					<code><img src="images/person.jpg" class="left-align"></code>
					<br/>
					<code><img src="images/person.jpg" class="right-align"></code>
					<br/>
					<code><img src="images/person.jpg" class="full-width"></code>
					<br/> Apply the classes right-align or left-align or full-width to float the images to left ro right or display it as 90% of it's parent's width and align it center (see all cases below)

				</p>
				<p><strong>Left floated image</strong></p>

				<img src="images/panel_idea.jpg" class="left-align">
				<p>
					Appellat aut laborum, quid quamquam exercitation, magna exquisitaque mandaremus veniam cupidatat et iis dolor consequat ab nam summis eram elit expetendis, probant sint qui singulis concursionibus, ut se concursionibus an laboris sint vidisse cupidatat. Quid senserit voluptatibus, dolore in do magna nescius, se eiusmod sempiternum, non ubi adipisicing. Esse hic eiusmod. Quamquam sed labore, laborum ex incurreret, ab iis concursionibus in de mentitum coniunctione. Cupidatat est aliqua ingeniis se lorem adipisicing fabulas dolore offendit. Malis vidisse appellat, do senserit adipisicing hic ubi dolor quamquam, doctrina duis multos ne quorum. Voluptate ita possumus, te quem vidisse. Minim laborum ad aliqua sint ubi anim possumus distinguantur, tamen occaecat incurreret aut vidisse summis probant ingeniis aut nulla hic ab enim officia ita ea quo efflorescere, ab amet laborum praesentibus, legam si sed multos laborum.
				</p>

				<p><strong>Right floated image</strong></p>

				<img src="images/person.jpg" class="right-align">
				<p>Appellat aut laborum, quid quamquam exercitation, magna exquisitaque mandaremus veniam cupidatat et iis dolor consequat ab nam summis eram elit expetendis, probant sint qui singulis concursionibus, ut se concursionibus an laboris sint vidisse cupidatat. Quid senserit voluptatibus, dolore in do magna nescius, se eiusmod sempiternum, non ubi adipisicing. Esse hic eiusmod. Quamquam sed labore, laborum ex incurreret, ab iis concursionibus in de mentitum coniunctione. Cupidatat est aliqua ingeniis se lorem adipisicing fabulas dolore offendit. Malis vidisse appellat, do senserit adipisicing hic ubi dolor quamquam, doctrina duis multos ne quorum. Voluptate ita possumus, te quem vidisse. Minim laborum ad aliqua sint ubi anim possumus distinguantur, tamen occaecat incurreret aut vidisse summis probant ingeniis aut nulla hic ab enim officia ita ea quo efflorescere, ab amet laborum praesentibus, legam si sed multos laborum. Noster e incididunt, amet quibusdam aut fore minim. Summis e pariatur. Labore expetendis exercitation, excepteur irure commodo senserit ita anim e officia ex qui quis occaecat transferrem. Varias a mentitum ut irure ab laboris de dolore offendit ut litteris, te quid summis eu iudicem ne admodum praetermissum id doctrina nam anim non eu quorum laboris eu te illum ingeniis doctrina an culpa excepteur tempor. Fabulas duis ita deserunt eruditionem. Senserit qui eram admodum, ad si fore ullamco. Quamquam quis qui iudicem coniunctione ab nam magna elit fugiat fabulas qui quo ex despicationes an appellat quo vidisse.</p>

				<p><strong>Full-width(90% actually) image</strong></p>
				<img src="images/initiative1.jpg" class="full-width">
				<p>Officia legam velit iis dolore est laboris illum fabulas, amet ea mandaremus ab laboris culpa aliquip doctrina. Se elit malis esse commodo. Aute admodum ad laborum, do aute si tamen, eu legam expetendis quo ita ad graviterque. Ad a fore lorem dolor, cupidatat dolor minim iudicem quorum iis proident e esse ab tempor velit quem nostrud legam. Quo dolore singulis tempor, tempor fugiat incurreret admodum, e fore pariatur exquisitaque, si senserit de cernantur. Consequat dolore iis litteris cohaerescant se in mandaremus voluptatibus.</p>

				<h3>Colors</h3>
				<table valign="top">
					<tbody>
						<tr>
							<td>
								<p class="teal">Teal</p>
								<code><p class="teal">Teal</p></code>
								<div class="teal-bg"></div>
								<code><div class="teal-bg"></div></code>
							</td>
							<td>
								<p class="orange">Orange - OWLC</p>

								<code><p class="orange">Orange</p></code>
								<div class="orange-bg"></div>
								<code><div class="orange-bg"></div></code>
							</td>
							<td>
								<p class="green">Green - OCSL</p>

								<code><p class="green">Green</p></code>
								<div class="green-bg"></div>
								<code><div class="green-bg"></div></code>
							</td>
							<td>
								<p class="light-teal">Light Teal - OTHER</p>
								<code><p class="light-teal">Teal</p></code>
								<div class="light-teal-bg"></div>
								<code><div class="light-teal-bg"></div></code>
							</td>
						</tr>
						<tr>
							<td>
								<p class="yellow">Yellow</p>
								<code><p class="yellow">Teal</p></code>
								<div class="yellow-bg"></div>
								<code><div class="yellow-bg"></div></code>
							</td>
							<td>

								<div class="idea-bg"></div>
								<code><div class="idea-bg"></div></code>
							</td>
							<td>

								<div class="workshop-bg"></div>
								<code><div class="workshop-bg"></div></code>
							</td>
							<td>

								<div class="onlinelearn-bg"></div>
								<code><div class="onlinelearn-bg"></div></code>
							</td>
						</tr>
						<tr>
							<td>

								<div class="special-bg"></div>
								<code><div class="special-bg"></div></code>
							</td>
							<td>

								<div class="teacher-bg"></div>
								<code><div class="teacher-bg"></div></code>
							</td>
							<td>

								<div class="light-bg"></div>
								<code><div class="light-bg"></div></code>
							</td>
							<td>

								<div class="dark-bg"></div>
								<code><div class="dark-bg"></div></code>
							</td>
						</tr>
						<tr>
							<td>
								<p class="interractive-text">interractive-text</p>
								<code><p class="interractive-text">interractive-text</p></code>

							</td>
							<td>
								<p class="header-text ">header-text </p>

								<code><p class="header-text ">header-text </p></code>

							</td>
							<td>
								<p class="body-text">body-text</p>

								<code><p class="body-text">body-text</p></code>

							</td>
							<td style="background:black">
								<p class="white-text ">white-text </p>
								<code><p class="white-text ">white-text </p></code>

							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script>
	$("code").each(function() {
		$(this).html($(this).html().replace(/</g, '&lt;').replace(/>/g, '&gt;'));
	});

</script>
<style>
	code {
		margin-bottom: 10px;
		display: inline-block;
	}

	table tr td {
		padding: 8px;
	}

	table tr td div {
		width: 50px;
		height: 50px;
		margin-bottom: 10px;
	}

	table code {
		display: block;
		margin-bottom: 10px;
	}

	table p {
		margin: 0;
		font-weight: 400;
	}

</style>

<?php include ("templates/footer.php"); ?>
