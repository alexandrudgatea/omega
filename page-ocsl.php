	<?php include ("templates/header.php"); ?>


		<div id="mainContent" class="fancy-page ocsl">
			<!--	this is the landingpage nav-->
			<nav class="navbar navbar-default page-navbar" role="navigation">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".page-menu">
					<span class="visible-xs mobile-menu-toggle"> Explore the OWLC <i class="fa fa-angle-down"></i> </span>
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".mobile-join">
					<span class="visible-xs mobile-menu"> Join</span>
				</button>

			</div>
			<div class="container-fluid">
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse page-menu">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="#">About the OWLC</a></li>
						<!--page-nav dropdown begin-->
						<li class="dropdown">
							<a href="http://google.com" class="dropdown-toggle" data-toggle="dropdown">Programs</a>
							<ul class="dropdown-menu">
								<li><a href="http://google.com">2016 Workshops</a></li>
								<li><a href="http://google.com">Leadership in Sustainable Education Award</a></li>
								<li><a href="http://google.com">OCSL Past Programs</a></li>

								<!-- in case necessary this is markup for lvl2 dropdown-->
								<!--
								<li class="dropdown-submenu">
									<a class="submenu-drop" tabindex="-1" href="#">New dropdown </a>
									<ul class="dropdown-menu">
										<li><a href="#">2nd level dropdown</a></li>
										<li><a href="#">2nd level dropdown</a></li>
										<li><a href="#">2nd level dropdown</a></li>
									</ul>
								</li>
-->
							</ul>
						</li>
						<!--page-nav dropdown end-->
						<li><a href="#">Resources</a></li>
						<li><a href="#">Support the OWLC</a></li>
					</ul>
				</div>
				<div class="collapse navbar-collapse mobile-join">
					<ul class="nav navbar-nav navbar-right visible-xs">
						<li>
							<div class="lp-not-logged">
								<div class="container">
									<div class="row">
										<div class="col-sm-12 center-block text-center">
											<form class="form-inline">
												<div class="form-group">
													<label for="owlcSignUp">Get OWLC Updates: </label>
													<input type="email" class="form-control" id="owlcSignUp" placeholder="you@email.com">
												</div>
												<button type="submit" class="btn btn-default">Sign Up</button>
											</form>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12 text-center">
											<a href="#" class="log-in">
												Already Registered? Login 
												<span class="fa-stack fa-lg">
													<i class="fa fa-circle fa-stack-1x"></i>
													<i class="fa fa-angle-right fa-stack-1x fa-inverse"></i>
												</span>
											</a>
										</div>
									</div>
								</div>
							</div>
						</li>
						<li>
							<div class="lp-logged">
								<div class="container">
									<div class="row">
										<div class="col-sm-12 center-block text-center">
											<form class="form-inline">
												<div class="form-group">
													<div class="checkbox">
														<label for="owlcUpdates">
															<input type="checkbox" value="" id="owlcUpdates">Get OWLC Updates
														</label>
													</div>
												</div>
											</form>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12 text-center">
											<a href="#" class="view-profile interractive-text">
												View your profile 
												<span class="fa-stack fa-lg">
													<i class="fa fa-circle fa-stack-1x "></i>
													<i class="fa fa-angle-right fa-stack-1x fa-inverse"></i>
												</span>
											</a>
										</div>
									</div>
								</div>
							</div>
						</li>

					</ul>
				</div>
			</div>
			<!-- /.navbar-collapse -->
		</nav>

			<div id="hero" class="fancy-page background-image" style="background: url('/images/hero_owlc.jpg')">
				<div class="container full-height">
					<div class="row">
						<div class="col-sm-6 green-bg transparent">
							<div class="page-info field-text">
								<h4 class="page-category">Omega Women’s Leadership Center</h4>
								<h1 class="page-title">Vision &amp; Purpose</h1>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="pageContent">
				<div class="container">
					<div class="row">
						<div class="col-sm-3 sidebar">
							<ul class="list-unstyled">
								<li class="page-share">
									<ul class="list-inline social-links">
										<li>
											<a href="#!">
												<span class="fa-stack fa-lg">
												<i class="fa fa-circle fa-stack-2x"></i>
												<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
											</span>
											</a>
										</li>
										<li>
											<a href="#!">
												<span class="fa-stack fa-lg">
												<i class="fa fa-circle fa-stack-2x"></i>
												<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
											</span>
											</a>
										</li>
										<li>
											<a href="#!">
												<span class="fa-stack fa-lg">
												<i class="fa fa-circle fa-stack-2x"></i>
												<i class="fa fa-envelope-o fa-stack-1x fa-inverse"></i>
											</span>
											</a>
										</li>
										<li>
											<a href="#!">
												<span class="fa-stack fa-lg">
												<i class="fa fa-circle fa-stack-2x"></i>
												<i class="fa fa-share-alt fa-stack-1x fa-inverse"></i>
											</span>
											</a>
										</li>
									</ul>
								</li>
								<li class="side-menu">
									<ul class="list-unstyled">
										<li class="active"><a href="#!">Core Values</a></li>
										<li><a href="#!">Our History</a></li>
										<li><a href="#!">Bright Future</a></li>
									</ul>
								</li>
							</ul>
						</div>
						<div class="col-sm-9 no-padding">
							<div class="page-body field">
								<div class="page-text">
									<p>
										Vision
									</p>
									<p><a href="http://develop.eomega.org">This is a link that leads nowhere</a></p>
									<p>
										We envision women’s leadership advancing a future where:
									</p>
									<p>
										Women are valued for their full human potential, live in safety, equality and freedom, and can express themselves and contribute meaningfully in all spheres of life;
									</p>
									<p>
										Men are free to express the full range of human qualities and share equitably with women in life’s responsibilities and joys;
									</p>
									<p>
										Our global society fosters nurturing and mutual relationships, healthy families and communities, and a peaceful, just, and sustainable world—for everybody.
									</p>
									<p>
										Purpose
									</p>
									<p>
										The Omega Women's Leadership Center aspires to:
									</p>
									<p>
										Promote women’s leadership by convening, inspiring, and training women to lead from their own authentic voice, values, and vision.
									</p>
									<p>
										Create a power and leadership paradigm shift, from “power over” to “power with” others.
									</p>
									<p>
										Foster a balance between masculine and feminine principles, both of which are essential to creating a more just and sustainable world.
									</p>
								</div>
								<div class="carousel-section">
									<div class="dark-bg field-text">
										<div class="text-block">
											<h1 class="field-text section-title">The Photographic Visions of Douglas Beasley</h1>
											<span class="author block">By Briana Boles</span>
											<span class="date block">May 03, 2016</span>
											<span class="description"> Arts Week teacher Douglas Beasley is founder and director of Vision Quest Photo Workshops. As an artist, he believes that we always have the opportunity to transform an emotional  experience into a visual one.</span>
										</div>

										<div id="articleCarousel" class="carousel slide carousel-fade" data-ride="carousel">
											<ol class="carousel-indicators">
												<li data-target="#articleCarousel" data-slide-to="0" class="active"> </li>
												<li data-target="#articleCarousel" data-slide-to="1"> </li>
												<li data-target="#articleCarousel" data-slide-to="2"> </li>
											</ol>
											<div class="carousel-inner">
												<div class="item active">
													<img src="images/hero.jpeg" alt="First slide" />
												</div>
												<div class="item">
													<img src="images/hero2.jpeg" alt="Second slide" />
												</div>
												<div class="item">
													<img src="images/hero3.jpeg" alt="Third slide" />
												</div>
											</div>
											<a class="left carousel-control" href="#articleCarousel" data-slide="prev">

											</a>
											<a class="right carousel-control" href="#articleCarousel" data-slide="next">

											</a>
										</div>
										<div class="text-block">
											<span class="description">
								Douglas describes his own journey as one through religion toward spirituality, and he credits
photography as helping him become more authentic in his own vision. 
								<br><br>
								“My work as a photographer is about seeking the sacred,” he says, “looking to find the divine in everything. My photography is about recognizing the spirit that inhabits all things, animate and inanimate, big and small.”
								</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

<?php include ("templates/footer.php"); ?>