<?php include ("templates/header.php"); ?>

	<div id="hero" class="collections background-image" style="background: url('/images/hero_collections.jpg')">
		<div class="container full-height">
			<div class="row">
				<div class="col-sm-6 idea-bg transparent">
					<div class="article-info field-text">
						<h4 class="article-category ">Omega Women’s Leadership Center</h4>
						<h1 class="article-title ">The Keys to Happiness</h1>
						<span class="article-subtitle ">A Collection of Ideas</span>
						<ul class="event-details list-unstyled">
							<li class="curator">Curator</li>
							<li class="curator-name">Ania Nassbaum</li>
							<li class="event-date">Apr 22, 2015</li>
						</ul>
					</div>

				</div>
			</div>
		</div>
	</div>
	<div id="mainContent" class="collections">
		<div class="container">
			<div class="row">
				<div class="col-sm-3 sidebar">
					<ul class="article-info list-unstyled">

						<li class="article-share">
							<ul class="list-inline social-links">
								<li>
									<a href="#!">
										<span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x teal"></i>
									<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
								</span>
									</a>
								</li>
								<li>
									<a href="#!">
										<span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x teal"></i>
									<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
								</span>
									</a>
								</li>
								<li>
									<a href="#!">
										<span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x teal"></i>
									<i class="fa fa-envelope-o fa-stack-1x fa-inverse"></i>
								</span>
									</a>
								</li>
								<li>
									<a href="#!">
										<span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x teal"></i>
									<i class="fa fa-share-alt fa-stack-1x fa-inverse"></i>
								</span>
									</a>
								</li>
							</ul>
							<a href="#!" class="btn cta icon-button rounded-corners"><i class="fa fa-bookmark icon"></i>See Another</a>
						</li>
					</ul>
					<ul class="future-events list-unstyled">
						<li>
							<div class="event-type">Workshop</div>
							<h3 class="event-title">Growing and Sustaining A Yoga Service Orginzation</h3>
							<span class="event-participants">Ania Nussbaum, Grenadine Wright, Rene Zenllenger, and more</span>
							<span class="event-date">Jun 12 – Jun 18 2016</span>
						</li>
						<li>
							<div class="event-type">Conference</div>
							<h3 class="event-title">Transformational Cleaning for the Body and Soul</h3>
							<span class="event-participants">Ania Nussbaum and Grenadine Wright</span>
							<span class="event-date">Feb 28 – Mar 06 2016</span>
						</li>
						<li>
							<div class="event-type">Workshop</div>
							<h3 class="event-title">Collaborating with Nature</h3>
							<span class="event-participants">Peter Parker and Grenadine Wright</span>
							<span class="event-date">Jun 12 – Jun 18 2016</span>
						</li>
					</ul>
				</div>
				<div class="col-sm-9 no-padding">
					<div class="article-body">
						<span class="article-intro interractive-text">
						Holidays like Valentine’s Day, are marked by the practice of showing love and appreciation for those we are romantically involved with. Other holidays, like Thanksgiving and Christmas, are traditionally characterized by sharing time.
Other holidays, like Thanksgiving and Christmas, are traditionally characterized by sharing time characterized.
						</span>
						<ul class="collections-list list-unstyled">
							<li>
								<div class="tiles-grid">
									<div class="row">
										<div class="col-sm-6">
											<div class="panel panel-default panel-idea tile">
												<div class="favourite">
													<div class="checkbox">
														<input type="checkbox" value="" id="favourite1">
														<label for="favourite1"></label>
													</div>
												</div>
												<div class="panel-heading" style="background: url('images/panel_idea.jpg')"> </div>
												<div class="panel-body panel-audio-video">
													<div class="content-type"> <a href="#!"><i class="fa fa-play-circle-o"></i></a> <span>Audio</span> </div>
													<div class="title">
														<h1>Bobby McFerrin: Circle Songs from Omega</h1> </div>
													<div class="person">
														<ul class="by list-unstyled list-inline">
															<li>Thomas Robertson</li>
															<li>Marry Anderson</li>
														</ul>
														<ul class="featuring list-unstyled list-inline">
															<li>Thomas Robertson</li>
															<li>Marry Anderson</li>
														</ul>
													</div>
													<div class="duration-date"> <em class="duration">13 minute watch</em> <em class="date-release">13th of September, 2016</em> </div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="item-content">
									<h1 class="title bottom-bar-left-teal">Make Friends and Family a Priority</h1>
									<span class="block body-text description">Even if there were a day of observance set aside for the appreciation of friends, it seems like one day would not be enough to convey the importance and distinction that friends bring to our lives. Even if there were a day of observance set aside for the appreciation of friends. </span>
								</div>
							</li>
							<li>
								<div class="tiles-grid">
									<div class="row">
										<div class="col-sm-6">
											<div class="panel panel-default panel-idea tile">
												<div class="favourite">
													<div class="checkbox">
														<input type="checkbox" value="" id="favourite1">
														<label for="favourite1"></label>
													</div>
												</div>
												<div class="panel-heading" style="background: url('images/panel_idea.jpg')"> </div>
												<div class="panel-body panel-audio-video">
													<div class="content-type"> <a href="#!"><i class="fa fa-play-circle-o"></i></a> <span>Audio</span> </div>
													<div class="title">
														<h1>Bobby McFerrin: Circle Songs from Omega</h1> </div>
													<div class="person">
														<ul class="by list-unstyled list-inline">
															<li>Thomas Robertson</li>
															<li>Marry Anderson</li>
														</ul>
														<ul class="featuring list-unstyled list-inline">
															<li>Thomas Robertson</li>
															<li>Marry Anderson</li>
														</ul>
													</div>
													<div class="duration-date"> <em class="duration">13 minute watch</em> <em class="date-release">13th of September, 2016</em> </div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="item-content">
									<h1 class="title bottom-bar-left-teal">... Especially on Weekends</h1>
									<span class="block body-text description">Busy Lives can get in the way of happiness. Our feelings of wellbeing peak on weekends, especially because we spend more time with friends and family, this web developer says. </span>
								</div>
							</li>
							<li>
								<div class="tiles-grid">
									<div class="row">
										<div class="col-sm-6">
											<div class="panel panel-default panel-idea tile">
												<div class="favourite">
													<div class="checkbox">
														<input type="checkbox" value="" id="favourite1">
														<label for="favourite1"></label>
													</div>
												</div>
												<div class="panel-heading" style="background: url('images/panel_idea.jpg')"> </div>
												<div class="panel-body panel-audio-video">
													<div class="content-type"> <a href="#!"><i class="fa fa-play-circle-o"></i></a> <span>Audio</span> </div>
													<div class="title">
														<h1>Bobby McFerrin: Circle Songs from Omega</h1> </div>
													<div class="person">
														<ul class="by list-unstyled list-inline">
															<li>Thomas Robertson</li>
															<li>Marry Anderson</li>
														</ul>
														<ul class="featuring list-unstyled list-inline">
															<li>Thomas Robertson</li>
															<li>Marry Anderson</li>
														</ul>
													</div>
													<div class="duration-date"> <em class="duration">13 minute watch</em> <em class="date-release">13th of September, 2016</em> </div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="item-content">
									<h1 class="title bottom-bar-left-teal">Gratitude Helps</h1>
									<span class="block body-text description">If the only prayer you ever learn is “Thank you,” that’s good enough.</span>
								</div>
							</li>
						</ul>
					</div>
					<div class="discover-more clearfix">
						<h1 class="section-title">More about the Keys to Happiness</h1>
						<ul class="discover-more-tiles list-unstyled hidden-xs">
							<li>
								<div class="tile-image">
									<img src="images/collection_tile.jpg" class="img-responsive">
								</div>
								<div class="tile-content">
									<div class="content-type">
										<a href="#!"><i class="fa fa-play-circle-o"></i></a>
										<span>Video</span>
									</div>
									<h2 class="title interractive-text">How do you establish trust in a relationship</h2>
									<span class="tile-text block">Chödrön is the resident teacher at Gampo
Abbey in Cape Breton, Nova Scotia, the first Tibetan monastery for Westerners. An American Buddhist nun. </span>
									<div class="bottom-info">
										<span class="featured">Featuring: Featuring Richard Borofsky, Peter Parker, andAntra Kalnins Borofsky | <span class="video-duration">00:43:23</span>
										</span>
									</div>
								</div>
							</li>
							<li>
								<div class="tile-image">
									<img src="images/collection_tile2.jpg" class="img-responsive">
								</div>
								<div class="tile-content">
									<div class="content-type">
										<a href="#!"><i class="fa fa-play-circle-o"></i></a>
										<span>Video</span>
									</div>
									<h2 class="title interractive-text">The Courage to Choose</h2>
									<span class="tile-text block">Chödrön is the resident teacher at Gampo
Abbey in Cape Breton, Nova Scotia, the first Tibetan monastery for Westerners. An American Buddhist nun. </span>
									<div class="bottom-info">
										<span class="featured">By Elizabeth Gilbert</span>
									</div>
								</div>
							</li>
							<li>
								<div class="tile-image">
									<img src="images/collection_tile3.jpg" class="img-responsive">
								</div>
								<div class="tile-content">
									<div class="content-type">
										<a href="#!"><i class="fa fa-play-circle-o"></i></a>
										<span>Video</span>
									</div>
									<h2 class="title interractive-text">Reclaim Your Inner Eden</h2>
									<span class="tile-text block">Chödrön is the resident teacher at Gampo
Abbey in Cape Breton, Nova Scotia, the first Tibetan monastery for Westerners. An American Buddhist nun. </span>
									<div class="bottom-info">
										<span class="featured">By Katina I. Makris and Suzanne Kingsbury </span>
									</div>
								</div>
							</li>
						</ul>
						<!-- collections discover more visible on xs only-->
						<div class="collections-mobile-tiles visible-xs">
							<div class="tiles-grid">
								<div class="row">
									<div class="col-sm-4">
										<div class="panel panel-default tile  panel-idea">
											<div class="favourite">
												<div class="checkbox">
													<input type="checkbox" value="" id="favourite1">
													<label for="favourite1"></label>
												</div>
											</div>
											<div class="panel-heading" style="background: url('images/panel_idea.jpg')">

											</div>
											<div class="panel-body panel-article">
												<div class="content-type">
													<span>Article</span>
												</div>
												<div class="title">
													<h1>Bobby McFerrin: Circle Songs from Omega</h1>
												</div>
												<div class="person">
													<ul class="by list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
													<span class="featuring">Featuring:</span>
													<ul class="featuring list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
												</div>
												<div class="duration-date">
													<em class="duration">13 minute watch</em>
													<em class="date-release">13th of September, 2016</em>
												</div>
											</div>
											<div class="panel-footer">
												<a href="#!" class="expand-button">
													<i class="fa fa-plus"></i>
												</a>
												<div class="expanded-tile-content animated fadeInUp">
													<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
													<a href="#!" class="btn white-transparent-button">Learn More</a>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="panel panel-default tile  panel-idea">
											<div class="favourite">
												<div class="checkbox">
													<input type="checkbox" value="" id="favourite2">
													<label for="favourite2"></label>
												</div>
											</div>
											<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
											</div>
											<div class="panel-body panel-audio-video">
												<div class="content-type">
													<a href="#!"><i class="fa fa-play-circle-o"></i></a>
													<span>Video</span>
												</div>
												<div class="title">
													<h1>Bobby McFerrin: Circle Songs from Omega</h1>
												</div>
												<div class="person">
													<ul class="by list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
													<ul class="featuring list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
												</div>
												<div class="duration-date">
													<em class="duration">13 minute watch</em>
													<em class="date-release">13th of September, 2016</em>
												</div>
											</div>
											<div class="panel-footer">
												<a href="#!" class="expand-button">
													<i class="fa fa-plus"></i>
												</a>
												<div class="expanded-tile-content animated fadeInUp">
													<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
													<a href="#!" class="btn white-transparent-button">Learn More</a>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="panel panel-default tile  panel-idea">
											<div class="favourite">
												<div class="checkbox">
													<input type="checkbox" value="" id="favourite3">
													<label for="favourite3"></label>
												</div>
											</div>
											<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
											</div>
											<div class="panel-body panel-press">
												<div class="content-type">
													<span>Press Release</span>
												</div>
												<div class="title">
													<h1>Bobby McFerrin: Circle Songs from Omega</h1>
												</div>
												<div class="person">
													<ul class="by list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
													<span class="featuring">Featuring:</span>
													<ul class="featuring list-unstyled list-inline">
														<li>Thomas Robertson</li>
														<li>Marry Anderson</li>
													</ul>
												</div>
												<div class="duration-date">
													<em class="duration">13 minute watch</em>
													<em class="date-release">13<sup>th</sup> of September, 2016</em>
												</div>
											</div>
											<div class="panel-footer">
												<a href="#!" class="expand-button">
													<i class="fa fa-plus"></i>
												</a>
												<div class="expanded-tile-content animated fadeInUp">
													<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
													<a href="#!" class="btn white-transparent-button">Learn More</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php include ("templates/footer.php"); ?>