<?php include ("templates/header.php"); ?>
<div class="video-popup">
    <div class="close">
        <i class="fa fa-times"></i>
    </div>
    <video id="video" data-video-id="5062983866001" data-account="1967227982001" data-player="default" data-embed="default" data-application-id class="video-js" controls></video>
    <script src="//players.brightcove.net/1967227982001/default_default/index.min.js"></script>




</div>

<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(document).on("click", "#hero .play-icon", function() {
            $(".video-popup").toggleClass("open");
            var myPlayer = videojs("video");
            myPlayer.play();
        });

        $(document).on("click", ".video-popup .close", function() {
            $(".video-popup").toggleClass("open");
            var myPlayer = videojs("video");
            myPlayer.pause();
        });
    });

</script>


<div id="hero" class="video background-image" style="background: url('/images/hero_video.jpg')">
    <div class="container full-height">
        <div class="row">
            <div class="col-sm-6 special-bg transparent">
                <div class="article-info field-text">
                    <h4 class="article-category ">Omega Women’s Leadership Center</h4>
                    <h1 class="article-title ">How Mindfulness-Based Practices Help the Brain</h1>
                    <span class="article-subtitle ">Learn to relax and be at peace</span>
                    <a href="#!" class="block play-icon BCL-btn" onclick="playVideo()">
                        <img src="images/icons/play_icon_big.png">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="mainContent" class="video">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 sidebar">
                <ul class="article-info list-unstyled">
                    <li>
                        <span class="article-author block">
								Featuring <a href="/workshops/teachers/alex-grey">Alex Grey</a>
							</span>
                        <span class="article-date block">Sep 08, 2016</span>
                        <span class="type"></span>
                    </li>
                </ul>
                <div class="panel-pane pane-views-panes pane-ideas-social-shares-panel-social-shares">
                    <div class="pane-content">
                        <div class="view view-ideas-social-shares view-id-ideas_social_shares view-display-id-panel_social_shares view-dom-id-90428e30a51a38e633978107475caf34">
                            <div class="view-content">
                                <ul class="article-info list-unstyled">
                                    <li class="article-share">
                                        <ul class="list-inline social-links">
                                            <li>
                                                <a id="facebook-share" style="cursor: pointer;">
                                                    <span class="fa-stack fa-lg">
					<i class="fa fa-circle fa-stack-2x teal" aria-hidden="true"></i>
					<i class="fa fa-facebook fa-stack-1x fa-inverse" aria-hidden="true"></i>
				</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a id="twitter-share" href="http://www.twitter.com/share?url=http://test2.eomega.org/videos/jon-kabat-zinn-awareness-of-awareness" target="_blank">
                                                    <span class="fa-stack fa-lg">
					<i class="fa fa-circle fa-stack-2x teal" aria-hidden="true"></i>
					<i class="fa fa-twitter fa-stack-1x fa-inverse" aria-hidden="true"></i>
				</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="mailto:">
                                                    <span class="fa-stack fa-lg">
					<i class="fa fa-circle fa-stack-2x teal" aria-hidden="true"></i>
					<i class="fa fa-envelope-o fa-stack-1x fa-inverse" aria-hidden="true"></i>
				</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#!">
                                                    <span class="fa-stack fa-lg">
					<i class="fa fa-circle fa-stack-2x teal" aria-hidden="true"></i>
					<i class="fa fa-share-alt fa-stack-1x fa-inverse" aria-hidden="true"></i>
				</span>
                                                </a>
                                            </li>
                                        </ul>
                                        <a href="#!" class="btn cta icon-button rounded-corners"><i class="fa fa-bookmark icon" aria-hidden="true"></i>Add to favorites</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-separator"></div>
                <div class="panel-pane pane-views-panes pane-ideas-learning-path-workshops-panel-learning-path-workshops">
                    <div class="pane-content">
                        <div class="view view-ideas-learning-path-workshops view-id-ideas_learning_path_workshops view-display-id-panel_learning_path_workshops view-dom-id-37d2c7076dda9c95cf6a6dd42d0b99a5">
                            <div class="view-content">
                                <ul class="future-events list-unstyled">
                                    <li>
                                        <div class="event-type">Workshop</div>
                                        <a href="/workshops/access-the-wisdom-of-the-spirit-world">
                                            <h3 class="event-title">Access the Wisdom of the Spirit World</h3>
                                        </a>
                                        <span class="event-participants">
													<a href="/workshops/teachers/james-van-praagh">James Van Praagh</a>
													<a href="/workshops/teachers/alex-grey">Alex Grey</a>
											</span>
                                        <span class="event-date">Aug 13 - Aug 18 2017</span>
                                    </li>
                                    <li>
                                        <div class="event-type">Workshop</div>
                                        <a href="/workshops/mindfulness-based-stress-reduction-in-mind-body-medicine-2">
                                            <h3 class="event-title">Mindfulness-Based Stress Reduction in&thinsp;Mind Body Medicine</h3>
                                        </a>
                                        <span class="event-participants">
													<a href="/workshops/teachers/saki-f-santorelli">Saki F. Santorelli</a>
														   <a href="/workshops/teachers/florence-meleo-meyer">Florence Meleo-Meyer</a>
															<a href="/workshops/teachers/judson-brewer-0">Judson Brewer</a>
														   <a href="/workshops/teachers/alex-grey">Alex Grey</a>
											</span>
                                        <span class="event-date">Jun 16 - Jun 23 2017</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-9 no-padding">
                <div class="article-body clearfix">
                    <div class="quote-section pull-right">
                        <div class="quote-icon">&lsquo;&lsquo; </div>
                        <h2 class="quote">You must have chaos within you to give birth to a dancing star.</h2>
                        <span class="quote-author block">Friedrich Nietzsche</span>
                        <span class="quote-author-profession block">Philosopher, cultural critic, and poet</span>
                        <ul class="list-inline social-links">
                            <li>
                                <a href="#!">
                                    <span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x teal"></i>
									<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
								</span>
                                </a>
                            </li>
                            <li>
                                <a href="#!">
                                    <span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x teal"></i>
									<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
								</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <span class="article-text">
						Even if there were a day of observance set aside for the appreciation of friends, it seems like one day would not be enough to convey the importance and distinction that friends bring to our lives. They are our sounding boards, our support systems when family fails us or simply
						becomes too challenging, and they provide invaluable perspective and reflection that helps us make sense of our.
						</span>


                    <div class="discover-more">
                        <h1 class="section-title">Discover More</h1>
                        <div class="carousel-section">
                            <div id="videoCarousel">
                                <div class="video-carousel">
                                    <ul class="list-inline clearfix">
                                        <li class="item">
                                            <div class="video-frame">
                                                <div style="display: block; position: relative; max-width: 100%; height:100%">
                                                    <div style="padding-top: 56.25%;">
                                                        <iframe class="videoframe" src="//players.brightcove.net/1967227982001/rkxtHYEfW_default/index.html" allowfullscreen webkitallowfullscreen mozallowfullscreen style="width: 100%; height: 100%; position: absolute; top: 0px; bottom: 0px; right: 0px; left: 0px;"></iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="item active">
                                            <div class="video-frame">
                                                <div style="display: block; position: relative; max-width: 100%; height:100%">
                                                    <div style="padding-top: 56.25%;">
                                                        <iframe class="videoframe" src="//players.brightcove.net/1967227982001/rkxtHYEfW_default/index.html" allowfullscreen webkitallowfullscreen mozallowfullscreen style="width: 100%; height: 100%; position: absolute; top: 0px; bottom: 0px; right: 0px; left: 0px;"></iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="item">
                                            <div class="video-frame">
                                                <div style="display: block; position: relative; max-width: 100%; height:100%">
                                                    <div style="padding-top: 56.25%;">
                                                        <iframe class="videoframe" src="//players.brightcove.net/1967227982001/rkxtHYEfW_default/index.html" allowfullscreen webkitallowfullscreen mozallowfullscreen style="width: 100%; height: 100%; position: absolute; top: 0px; bottom: 0px; right: 0px; left: 0px;"></iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="item">
                                            <div class="video-frame">
                                                <div style="display: block; position: relative; max-width: 100%; height:100%">
                                                    <div style="padding-top: 56.25%;">
                                                        <iframe class="videoframe" src="//players.brightcove.net/1967227982001/rkxtHYEfW_default/index.html" allowfullscreen webkitallowfullscreen mozallowfullscreen style="width: 100%; height: 100%; position: absolute; top: 0px; bottom: 0px; right: 0px; left: 0px;"></iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <style>
                                    .vjs-big-play-button {
                                        display: none;
                                    }

                                </style>
                                <div class="jcarousel-control-prev left carousel-control"></div>
                                <div class="jcarousel-control-next right carousel-control"></div>
                            </div>
                        </div>
                        <div class="tiles-grid hidden-xs">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="panel panel-default panel-idea tile">
                                        <div class="favourite">
                                            <div class="checkbox">
                                                <input type="checkbox" value="" id="favourite1">
                                                <label for="favourite1"></label>
                                            </div>
                                        </div>
                                        <div class="panel-heading" style="background: url('images/panel_idea.jpg')">

                                        </div>
                                        <div class="panel-body panel-article">
                                            <div class="content-type">
                                                <span>Article</span>
                                            </div>
                                            <div class="title">
                                                <h1>Bobby McFerrin: Circle Songs from Omega</h1>
                                            </div>
                                            <div class="person">
                                                <ul class="by list-unstyled list-inline">
                                                    <li>Thomas Robertson</li>
                                                    <li>Marry Anderson</li>
                                                </ul>
                                                <span class="featuring">Featuring:</span>
                                                <ul class="featuring list-unstyled list-inline">
                                                    <li>Thomas Robertson</li>
                                                    <li>Marry Anderson</li>
                                                </ul>
                                            </div>
                                            <div class="duration-date">
                                                <em class="duration">13 minute watch</em>
                                                <em class="date-release">13th of September, 2016</em>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="panel panel-default panel-idea tile">
                                        <div class="favourite">
                                            <div class="checkbox">
                                                <input type="checkbox" value="" id="favourite1">
                                                <label for="favourite1"></label>
                                            </div>
                                        </div>
                                        <div class="panel-heading" style="background: url('images/panel_idea.jpg')">

                                        </div>
                                        <div class="panel-body panel-article">
                                            <div class="content-type">
                                                <span>Article</span>
                                            </div>
                                            <div class="title">
                                                <h1>Bobby McFerrin: Circle Songs from Omega</h1>
                                            </div>
                                            <div class="person">
                                                <ul class="by list-unstyled list-inline">
                                                    <li>Thomas Robertson</li>
                                                    <li>Marry Anderson</li>
                                                </ul>
                                                <span class="featuring">Featuring:</span>
                                                <ul class="featuring list-unstyled list-inline">
                                                    <li>Thomas Robertson</li>
                                                    <li>Marry Anderson</li>
                                                </ul>
                                            </div>
                                            <div class="duration-date">
                                                <em class="duration">13 minute watch</em>
                                                <em class="date-release">13th of September, 2016</em>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="panel panel-default panel-idea tile">
                                        <div class="favourite">
                                            <div class="checkbox">
                                                <input type="checkbox" value="" id="favourite1">
                                                <label for="favourite1"></label>
                                            </div>
                                        </div>
                                        <div class="panel-heading" style="background: url('images/panel_idea.jpg')">

                                        </div>
                                        <div class="panel-body panel-article">
                                            <div class="content-type">
                                                <span>Article</span>
                                            </div>
                                            <div class="title">
                                                <h1>Bobby McFerrin: Circle Songs from Omega</h1>
                                            </div>
                                            <div class="person">
                                                <ul class="by list-unstyled list-inline">
                                                    <li>Thomas Robertson</li>
                                                    <li>Marry Anderson</li>
                                                </ul>
                                                <span class="featuring">Featuring:</span>
                                                <ul class="featuring list-unstyled list-inline">
                                                    <li>Thomas Robertson</li>
                                                    <li>Marry Anderson</li>
                                                </ul>
                                            </div>
                                            <div class="duration-date">
                                                <em class="duration">13 minute watch</em>
                                                <em class="date-release">13th of September, 2016</em>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="panel panel-default panel-idea tile">
                                        <div class="favourite">
                                            <div class="checkbox">
                                                <input type="checkbox" value="" id="favourite1">
                                                <label for="favourite1"></label>
                                            </div>
                                        </div>
                                        <div class="panel-heading" style="background: url('images/panel_idea.jpg')">

                                        </div>
                                        <div class="panel-body panel-article">
                                            <div class="content-type">
                                                <span>Article</span>
                                            </div>
                                            <div class="title">
                                                <h1>Bobby McFerrin: Circle Songs from Omega</h1>
                                            </div>
                                            <div class="person">
                                                <ul class="by list-unstyled list-inline">
                                                    <li>Thomas Robertson</li>
                                                    <li>Marry Anderson</li>
                                                </ul>
                                                <span class="featuring">Featuring:</span>
                                                <ul class="featuring list-unstyled list-inline">
                                                    <li>Thomas Robertson</li>
                                                    <li>Marry Anderson</li>
                                                </ul>
                                            </div>
                                            <div class="duration-date">
                                                <em class="duration">13 minute watch</em>
                                                <em class="date-release">13th of September, 2016</em>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="panel panel-default panel-idea tile">
                                        <div class="favourite">
                                            <div class="checkbox">
                                                <input type="checkbox" value="" id="favourite1">
                                                <label for="favourite1"></label>
                                            </div>
                                        </div>
                                        <div class="panel-heading" style="background: url('images/panel_idea.jpg')">

                                        </div>
                                        <div class="panel-body panel-article">
                                            <div class="content-type">
                                                <span>Article</span>
                                            </div>
                                            <div class="title">
                                                <h1>Bobby McFerrin: Circle Songs from Omega</h1>
                                            </div>
                                            <div class="person">
                                                <ul class="by list-unstyled list-inline">
                                                    <li>Thomas Robertson</li>
                                                    <li>Marry Anderson</li>
                                                </ul>
                                                <span class="featuring">Featuring:</span>
                                                <ul class="featuring list-unstyled list-inline">
                                                    <li>Thomas Robertson</li>
                                                    <li>Marry Anderson</li>
                                                </ul>
                                            </div>
                                            <div class="duration-date">
                                                <em class="duration">13 minute watch</em>
                                                <em class="date-release">13th of September, 2016</em>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="panel panel-default panel-idea tile">
                                        <div class="favourite">
                                            <div class="checkbox">
                                                <input type="checkbox" value="" id="favourite1">
                                                <label for="favourite1"></label>
                                            </div>
                                        </div>
                                        <div class="panel-heading" style="background: url('images/panel_idea.jpg')">

                                        </div>
                                        <div class="panel-body panel-article">
                                            <div class="content-type">
                                                <span>Article</span>
                                            </div>
                                            <div class="title">
                                                <h1>Bobby McFerrin: Circle Songs from Omega</h1>
                                            </div>
                                            <div class="person">
                                                <ul class="by list-unstyled list-inline">
                                                    <li>Thomas Robertson</li>
                                                    <li>Marry Anderson</li>
                                                </ul>
                                                <span class="featuring">Featuring:</span>
                                                <ul class="featuring list-unstyled list-inline">
                                                    <li>Thomas Robertson</li>
                                                    <li>Marry Anderson</li>
                                                </ul>
                                            </div>
                                            <div class="duration-date">
                                                <em class="duration">13 minute watch</em>
                                                <em class="date-release">13th of September, 2016</em>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tiles-grid visible-xs">
                            <div class="related">
                                <h3 class="related-tiles">Related Workshops</h3>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="panel panel-default tile  panel-workshop">
                                            <div class="favourite">
                                                <div class="checkbox">
                                                    <input type="checkbox" value="" id="favourite4">
                                                    <label for="favourite4"></label>
                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <div class="content-type">
                                                    <span>Workshop</span>
                                                </div>
                                                <div class="title">
                                                    <h1>Collaborating With Nature</h1>
                                                </div>
                                                <div class="person">
                                                    <ul class="list-unstyled list-inline person-images">
                                                        <li>
                                                            <img src="images/person.jpg">
                                                        </li>
                                                        <li> <img src="images/person.jpg"></li>
                                                    </ul>
                                                    <ul class="list-unstyled list-inline person-names">
                                                        <li>Thomas Robertson</li>
                                                        <li>Marry Anderson</li>
                                                    </ul>
                                                </div>
                                                <div class="date-location">
                                                    <ul class="list-inline">
                                                        <li><em>Feb 28 - Mar 06 2016</em></li>
                                                        <li><em>Rhinebeck, NY</em></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="panel-footer">
                                                <a href="#!" class="expand-button">
                                                    <i class="fa fa-plus"></i>
                                                </a>
                                                <div class="expanded-tile-content animated fadeInUp">
                                                    <span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
                                                    <a href="#!" class="btn white-transparent-button">Learn More</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="panel panel-default tile small panel-workshop">
                                            <div class="favourite">
                                                <div class="checkbox">
                                                    <input type="checkbox" value="" id="favourite4">
                                                    <label for="favourite4"></label>
                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <div class="content-type">
                                                    <span>Workshop</span>
                                                </div>
                                                <div class="title">
                                                    <h1>Collaborating With Nature</h1>
                                                </div>
                                                <div class="person">
                                                    <ul class="list-unstyled list-inline person-images">
                                                        <li>
                                                            <img src="images/person.jpg">
                                                        </li>
                                                        <li> <img src="images/person.jpg"></li>
                                                    </ul>
                                                    <ul class="list-unstyled list-inline person-names">
                                                        <li>Thomas Robertson</li>
                                                        <li>Marry Anderson</li>
                                                    </ul>
                                                </div>
                                                <div class="date-location">
                                                    <ul class="list-inline">
                                                        <li><em>Feb 28 - Mar 06 2016</em></li>
                                                        <li><em>Rhinebeck, NY</em></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="panel-footer">
                                                <a href="#!" class="expand-button">
                                                    <i class="fa fa-plus"></i>
                                                </a>
                                                <div class="expanded-tile-content animated fadeInUp">
                                                    <span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
                                                    <a href="#!" class="btn white-transparent-button">Learn More</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="panel panel-default tile small panel-workshop">
                                            <div class="favourite">
                                                <div class="checkbox">
                                                    <input type="checkbox" value="" id="favourite4">
                                                    <label for="favourite4"></label>
                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <div class="content-type">
                                                    <span>Workshop</span>
                                                </div>
                                                <div class="title">
                                                    <h1>Collaborating With Nature</h1>
                                                </div>
                                                <div class="person">
                                                    <ul class="list-unstyled list-inline person-images">
                                                        <li>
                                                            <img src="images/person.jpg">
                                                        </li>
                                                        <li> <img src="images/person.jpg"></li>
                                                    </ul>
                                                    <ul class="list-unstyled list-inline person-names">
                                                        <li>Thomas Robertson</li>
                                                        <li>Marry Anderson</li>
                                                    </ul>
                                                </div>
                                                <div class="date-location">
                                                    <ul class="list-inline">
                                                        <li><em>Feb 28 - Mar 06 2016</em></li>
                                                        <li><em>Rhinebeck, NY</em></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="panel-footer">
                                                <a href="#!" class="expand-button">
                                                    <i class="fa fa-plus"></i>
                                                </a>
                                                <div class="expanded-tile-content animated fadeInUp">
                                                    <span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
                                                    <a href="#!" class="btn white-transparent-button">Learn More</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="panel panel-default tile small panel-workshop">
                                            <div class="favourite">
                                                <div class="checkbox">
                                                    <input type="checkbox" value="" id="favourite4">
                                                    <label for="favourite4"></label>
                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <div class="content-type">
                                                    <span>Workshop</span>
                                                </div>
                                                <div class="title">
                                                    <h1>Collaborating With Nature</h1>
                                                </div>
                                                <div class="person">
                                                    <ul class="list-unstyled list-inline person-images">
                                                        <li>
                                                            <img src="images/person.jpg">
                                                        </li>
                                                        <li> <img src="images/person.jpg"></li>
                                                    </ul>
                                                    <ul class="list-unstyled list-inline person-names">
                                                        <li>Thomas Robertson</li>
                                                        <li>Marry Anderson</li>
                                                    </ul>
                                                </div>
                                                <div class="date-location">
                                                    <ul class="list-inline">
                                                        <li><em>Feb 28 - Mar 06 2016</em></li>
                                                        <li><em>Rhinebeck, NY</em></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="panel-footer">
                                                <a href="#!" class="expand-button">
                                                    <i class="fa fa-plus"></i>
                                                </a>
                                                <div class="expanded-tile-content animated fadeInUp">
                                                    <span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
                                                    <a href="#!" class="btn white-transparent-button">Learn More</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="related">
                                <h3 class="related-tiles">Related Ideas</h3>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="panel panel-default panel-idea tile">
                                            <div class="favourite">
                                                <div class="checkbox">
                                                    <input type="checkbox" value="" id="favourite1">
                                                    <label for="favourite1"></label>
                                                </div>
                                            </div>
                                            <div class="panel-heading" style="background: url('images/panel_idea.jpg')">

                                            </div>
                                            <div class="panel-body panel-article">
                                                <div class="content-type">
                                                    <span>Article</span>
                                                </div>
                                                <div class="title">
                                                    <h1>Bobby McFerrin: Circle Songs from Omega</h1>
                                                </div>
                                                <div class="person">
                                                    <ul class="by list-unstyled list-inline">
                                                        <li>Thomas Robertson</li>
                                                        <li>Marry Anderson</li>
                                                    </ul>
                                                    <span class="featuring">Featuring:</span>
                                                    <ul class="featuring list-unstyled list-inline">
                                                        <li>Thomas Robertson</li>
                                                        <li>Marry Anderson</li>
                                                    </ul>
                                                </div>
                                                <div class="duration-date">
                                                    <em class="duration">13 minute watch</em>
                                                    <em class="date-release">13th of September, 2016</em>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="panel panel-default panel-idea tile small">
                                            <div class="favourite">
                                                <div class="checkbox">
                                                    <input type="checkbox" value="" id="favourite1">
                                                    <label for="favourite1"></label>
                                                </div>
                                            </div>
                                            <div class="panel-heading" style="background: url('images/panel_idea.jpg')">

                                            </div>
                                            <div class="panel-body panel-article">
                                                <div class="content-type">
                                                    <span>Article</span>
                                                </div>
                                                <div class="title">
                                                    <h1>Bobby McFerrin: Circle Songs from Omega</h1>
                                                </div>
                                                <div class="person">
                                                    <ul class="by list-unstyled list-inline">
                                                        <li>Thomas Robertson</li>
                                                        <li>Marry Anderson</li>
                                                    </ul>
                                                    <span class="featuring">Featuring:</span>
                                                    <ul class="featuring list-unstyled list-inline">
                                                        <li>Thomas Robertson</li>
                                                        <li>Marry Anderson</li>
                                                    </ul>
                                                </div>
                                                <div class="duration-date">
                                                    <em class="duration">13 minute watch</em>
                                                    <em class="date-release">13th of September, 2016</em>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="panel panel-default panel-idea tile small">
                                            <div class="favourite">
                                                <div class="checkbox">
                                                    <input type="checkbox" value="" id="favourite1">
                                                    <label for="favourite1"></label>
                                                </div>
                                            </div>
                                            <div class="panel-heading" style="background: url('images/panel_idea.jpg')">

                                            </div>
                                            <div class="panel-body panel-article">
                                                <div class="content-type">
                                                    <span>Article</span>
                                                </div>
                                                <div class="title">
                                                    <h1>Bobby McFerrin: Circle Songs from Omega</h1>
                                                </div>
                                                <div class="person">
                                                    <ul class="by list-unstyled list-inline">
                                                        <li>Thomas Robertson</li>
                                                        <li>Marry Anderson</li>
                                                    </ul>
                                                    <span class="featuring">Featuring:</span>
                                                    <ul class="featuring list-unstyled list-inline">
                                                        <li>Thomas Robertson</li>
                                                        <li>Marry Anderson</li>
                                                    </ul>
                                                </div>
                                                <div class="duration-date">
                                                    <em class="duration">13 minute watch</em>
                                                    <em class="date-release">13th of September, 2016</em>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="panel panel-default panel-idea tile small">
                                            <div class="favourite">
                                                <div class="checkbox">
                                                    <input type="checkbox" value="" id="favourite1">
                                                    <label for="favourite1"></label>
                                                </div>
                                            </div>
                                            <div class="panel-heading" style="background: url('images/panel_idea.jpg')">

                                            </div>
                                            <div class="panel-body panel-article">
                                                <div class="content-type">
                                                    <span>Article</span>
                                                </div>
                                                <div class="title">
                                                    <h1>Bobby McFerrin: Circle Songs from Omega</h1>
                                                </div>
                                                <div class="person">
                                                    <ul class="by list-unstyled list-inline">
                                                        <li>Thomas Robertson</li>
                                                        <li>Marry Anderson</li>
                                                    </ul>
                                                    <span class="featuring">Featuring:</span>
                                                    <ul class="featuring list-unstyled list-inline">
                                                        <li>Thomas Robertson</li>
                                                        <li>Marry Anderson</li>
                                                    </ul>
                                                </div>
                                                <div class="duration-date">
                                                    <em class="duration">13 minute watch</em>
                                                    <em class="date-release">13th of September, 2016</em>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function($) {
        $("#videoCarousel item.active ").on("click", function() {
            $(this).siblings(".video-overlay").addClass("hidden");
        });
    });

</script>

<?php include ("templates/footer.php"); ?>
