<?php include ("templates/header.php"); ?>

<div id="mainContent" class="sponsors-page">
	<!--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
	<!--OUR SPONSORS-->

	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h2 class="interractive-text text-left">Sponsors &amp; partners</h2>
				<p>
					<a href="mailto:" target="_blank">Learn more about becoming a Sponsor</a>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 text-center">

				<!-- ===== Sponsors  lvl1 =====-->
				<h3 class="interractive-text text-left mt20 sponsor-level">Level 1</h3>
				<ul class="sponsor-list list-inline">

					<li>
						<a href="#!" target="_blank"><img src="images/sponsor4.png"></a>
					</li>
					<li>
						<a href="#!" target="_blank"><img src="images/sponsor5.png"></a>
					</li>
					<li>
						<a href="#!" target="_blank"><span>Sponsor name because he does not have a logo</span></a>
					</li>
					<li>
						<a href="#!" target="_blank"><img src="images/sponsor7.png"></a>
					</li>
					<li>
						<a href="#!" target="_blank"><img src="images/sponsor8.png"></a>
					</li>
					<li>
						<a href="#!" target="_blank"><img src="images/sponsor9.png"></a>
					</li>
					<li>
						<a href="#!" target="_blank"><img src="images/sponsor10.png"></a>
					</li>
				</ul>

				<!-- ===== Sponsors  lvl2 =====-->
				<h3 class="interractive-text text-left mt20  sponsor-level">Level 2</h3>
				<ul class="sponsor-list list-inline">

					<li>
						<a href="#!" target="_blank"><img src="images/initiative1.jpg"></a>
					</li>
					<li>
						<a href="#!" target="_blank"><img src="images/initiative2.jpg"></a>
					</li>
					<li>
						<a href="#!" target="_blank"><img src="images/initiative4.jpg"></a>
					</li>
					<li>
						<a href="#!" target="_blank"><img src="images/sponsor3.png"></a>
					</li>
					<li>
						<a href="#!" target="_blank"><img src="images/sponsor4.png"></a>
					</li>
					<li>
						<a href="#!" target="_blank"><img src="images/sponsor5.png"></a>
					</li>
					<li>
						<a href="#!" target="_blank"><span>Sponsor name because he does not have a logo</span></a>
					</li>
					<li>
						<a href="#!" target="_blank"><img src="images/sponsor7.png"></a>
					</li>
					<li>
						<a href="#!" target="_blank"><img src="images/sponsor8.png"></a>
					</li>
					<li>
						<a href="#!" target="_blank"><img src="images/sponsor9.png"></a>
					</li>
					<li>
						<a href="#!" target="_blank"><img src="images/sponsor10.png"></a>
					</li>
				</ul>

				<!-- ===== Sponsors  lvl3 =====-->
				<h3 class="interractive-text text-left mt20  sponsor-level">Level 3</h3>
				<ul class="sponsor-list list-inline">


					<li>
						<a href="#!" target="_blank"><img src="images/sponsor5.png"></a>
					</li>
					<li>
						<a href="#!" target="_blank"><span>Sponsor name because he does not have a logo</span></a>
					</li>
					<li>
						<a href="#!" target="_blank"><img src="images/sponsor7.png"></a>
					</li>
					<li>
						<a href="#!" target="_blank"><img src="images/sponsor8.png"></a>
					</li>
					<li>
						<a href="#!" target="_blank"><img src="images/sponsor9.png"></a>
					</li>
					<li>
						<a href="#!" target="_blank"><img src="images/sponsor10.png"></a>
					</li>
				</ul>

				<!-- ===== Sponsors  lvl4 =====-->
				<h3 class="interractive-text text-left mt20  sponsor-level">Level 4</h3>
				<ul class="sponsor-list list-inline">
					<li>
						<a href="#!" target="_blank"><img src="images/sponsor1.png"></a>
					</li>
					<li>
						<a href="#!" target="_blank"><img src="images/sponsor2.png"></a>
					</li>
					<li>
						<a href="#!" target="_blank"><img src="images/sponsor3.png"></a>
					</li>
					<li>
						<a href="#!" target="_blank"><img src="images/sponsor4.png"></a>
					</li>
					<li>
						<a href="#!" target="_blank"><img src="images/sponsor5.png"></a>
					</li>
					<li>
						<a href="#!" target="_blank"><img src="images/sponsor6.png"></a>
					</li>
					<li>
						<a href="#!" target="_blank"><img src="images/sponsor7.png"></a>
					</li>

				</ul>
				
				<!-- ===== Sponsors  lvl5 =====-->
				<h3 class="interractive-text text-left mt20  sponsor-level">Level 5</h3>
				<ul class="sponsor-list list-inline">
					<li>
						<a href="#!" target="_blank"><span>Sponsor name because he does not have a logo</span></a>
					</li>
					<li>
						<a href="#!" target="_blank"><span>Sponsor name because he does not have a logo</span></a>
					</li>
					<li>
						<a href="#!" target="_blank"><span>Sponsor name because he does not have a logo</span></a>
					</li>
					<li>
						<a href="#!" target="_blank"><span>Sponsor name because he does not have a logo</span></a>
					</li>
					<li>
						<a href="#!" target="_blank"><span>Sponsor name because he does not have a logo</span></a>
					</li>
					<li>
						<a href="#!" target="_blank"><span>Sponsor name because he does not have a logo</span></a>
					</li>
					<li>
						<a href="#!" target="_blank"><span>Sponsor name because he does not have a logo</span></a>
					</li>
					<li>
						<a href="#!" target="_blank"><span>Sponsor name because he does not have a logo</span></a>
					</li>
					<li>
						<a href="#!" target="_blank"><span>Sponsor name because he does not have a logo</span></a>
					</li>
					<li>
						<a href="#!" target="_blank"><span>Sponsor name because he does not have a logo</span></a>
					</li>
					<li>
						<a href="#!" target="_blank"><span>Sponsor name because he does not have a logo</span></a>
					</li>
				</ul>
			</div>
		</div>
	</div>

</div>

<?php include ("templates/footer.php"); ?>
