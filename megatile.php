<?php include ("templates/header.php"); ?>

	
	<div id="mainContent">
		<!--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
		<!-- TILES-->
		<div class="tiles-grid">
			<div class="container">
				<!--IDEA TILES-->
				<div class="row">
					<div class="col-sm-4">
						<div class="panel panel-default tile  panel-idea">
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite2">
									<label for="favourite2"></label>
								</div>
							</div>
							<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
							</div>
							<div class="panel-body panel-audio-video">
								<div class="content-type">
									<a href="#!"><i class="fa fa-play-circle-o"></i></a>
									<span>Video</span>
								</div>
								<div class="title">
									<h1>Bobby McFerrin: Circle Songs from Omega</h1>
								</div>
								<div class="person">
									<ul class="by list-unstyled list-inline">
										<li>Thomas Robertson</li>
										<li>Marry Anderson</li>
									</ul>
									<ul class="featuring list-unstyled list-inline">
										<li>Thomas Robertson</li>
										<li>Marry Anderson</li>
									</ul>
								</div>
								<div class="duration-date">
									<em class="duration">13 minute watch</em>
									<em class="date-release">13th of September, 2016</em>
								</div>
							</div>
							<div class="panel-footer">
								<a href="#!" class="expand-button">
									<i class="fa fa-plus"></i>
								</a>
								<div class="expanded-tile-content animated fadeInUp">
									<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
									<a href="#!" class="btn white-transparent-button">Learn More</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="panel tile multipurpose small">
							<a href="#!" class="tile-link"></a>
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite1">
									<label for="favourite1"></label>
								</div>
							</div>
							
							<div class="panel-heading" style="background: url('images/panel_idea.jpg')">

							</div>
							<div class="type">Scolarship</div>
							<div class="panel-body">
							<div class="category" style="background: url('images/workshop_events.jpg')">
									<h1><a href="#!">Workshops &amp; Events</a></h1>
							</div>
								<div class="content-type">
									<span>Article</span>
								</div>
								<div class="title">
									<h1><a href="#!">Bobby McFerrin: Circle Songs from Omega</a></h1>
								</div>
								<div class="image-tiles">
									<ul class="list-inline">
										<li style="background:url('images/workshop_events.jpg');"></li>
										<li style="background:url('images/person.jpg');"></li>
										<li style="background:url('images/online_learning.jpg');"></li>
										<li style="background:url('images/idea.jpg');"></li>
									</ul>
								</div>
								<div class="person">
									<ul class="by list-unstyled list-inline">
										<li>Thomas Robertson</li>
										<li>Marry Anderson</li>
									</ul>
									<ul class="featuring list-unstyled list-inline">
										<li>Thomas Robertson</li>
										<li>Marry Anderson</li>
									</ul>
									<ul class="list-unstyled list-inline person-images">
										<li>
											<img src="images/person.jpg">
										</li>
										<li> <img src="images/person.jpg"></li>
									</ul>
									<ul class="list-unstyled list-inline person-names">
										<li>Thomas Robertson</li>
										<li>Marry Anderson</li>
										<li>Marry Anderson</li>
									</ul>
								</div>
								<div class="duration-date">
									<em class="duration">13 minute watch</em>
									<em class="date-release">13th of September, 2016</em>
								</div>
								<div class="date-location">
									<ul class="list-inline">
										<li><em>Feb 28 - Mar 06 2016</em></li>
										<li><em>Rhinebeck, NY</em></li>
									</ul>
								</div>
							</div>
							<div class="panel-footer">
								<a href="#!" class="expand-button">
									<i class="fa fa-plus"></i>
								</a>
								<div class="expanded-tile-content animated fadeInUp">
									<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
									<a href="#!" class="btn white-transparent-button">Learn More</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="panel panel-default tile  panel-workshop">
							<a href="#!" class="tile-link"></a>
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite4">
									<label for="favourite4"></label>
								</div>
							</div>
							<div class="panel-body">
								<div class="content-type">
									<span>Workshop</span>
								</div>
								<div class="title">
									<h1>Collaborating With Nature</h1>
								</div>
								<div class="person">
									<ul class="list-unstyled list-inline person-images">
										<li>
											<img src="images/person.jpg">
										</li>
										<li> <img src="images/person.jpg"></li>
									</ul>
									<ul class="list-unstyled list-inline person-names">
										<li>Thomas Robertson</li>
										<li>Marry Anderson</li>
									</ul>
								</div>
								<div class="date-location">
									<ul class="list-inline">
										<li><em>Feb 28 - Mar 06 2016</em></li>
										<li><em>Rhinebeck, NY</em></li>
									</ul>
								</div>
							</div>
							<div class="panel-footer">
								<a href="#!" class="expand-button">
									<i class="fa fa-plus"></i>
								</a>
								<div class="expanded-tile-content animated fadeInUp">
									<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
									<a href="#!" class="btn white-transparent-button">Learn More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<div class="panel panel-default tile panel-learn">
							<a href="#!" class="tile-link"></a>
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite7">
									<label for="favourite7"></label>
								</div>
							</div>
							<div class="panel-body">
								<div class="content-type">
									<span>E-course</span>
								</div>
								<div class="title">
									<h1>Growing &amp; Sustaining A Yoga Service Organization</h1>
								</div>
								<div class="person">
									<ul class="list-unstyled list-inline person-images">
										<li>
											<img src="images/person.jpg">
										</li>
										<li> <img src="images/person.jpg"></li>
										<li> <img src="images/person.jpg"></li>
									</ul>
									<ul class="list-unstyled list-inline person-names">
										<li>Thomas Robertson</li>
										<li>Marry Anderson</li>
										<li>Marry Anderson</li>
									</ul>
								</div>
							</div>
							<div class="panel-footer">
								<a href="#!" class="expand-button">
									<i class="fa fa-plus"></i>
								</a>
								<div class="expanded-tile-content animated fadeInUp">
									<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
									<a href="#!" class="btn white-transparent-button">Learn More</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="panel panel-default tile  panel-collection">
							<a href="#!" class="tile-link"></a>
							<div class="favourite">
								<div class="checkbox">
									<input type="checkbox" value="" id="favourite10">
									<label for="favourite10"></label>
								</div>
							</div>
							<div class="panel-body">
								<div class="content-type">
									<span>Playlist</span>
								</div>
								<div class="title">
									<h1>Pema Chodron's Words of Wisdom</h1>
								</div>
								<div class="image-tiles">
									<ul class="list-inline">
										<li style="background:url('images/workshop_events.jpg');"></li>
										<li style="background:url('images/person.jpg');"></li>
										<li style="background:url('images/online_learning.jpg');"></li>
										<li style="background:url('images/idea.jpg');"></li>
									</ul>
								</div>
								<div class="person">
									<ul class="list-unstyled list-inline person-names">
										<li>Thomas Robertson</li>
										<li>Marry Anderson</li>
										<li>Marry Anderson</li>
									</ul>
								</div>
							</div>
							<div class="panel-footer">
								<a href="#!" class="expand-button">
									<i class="fa fa-plus"></i>
								</a>
								<div class="expanded-tile-content animated fadeInUp">
									<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
									<a href="#!" class="btn white-transparent-button">Learn More</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="panel panel-default tile  panel-category" style="background: url('images/workshop_events.jpg')">
							<div class="panel-body">
								<div class="category" >
									<h1><a href="#!">Workshops &amp; Events</a></h1>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- TILES END-->
		<!--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
	</div>
	<div id="tile-alert">
		<div class="alert text-center" role="alert">
			<button type="button" class="close"><span aria-hidden="true">&times;</span></button>
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="alert-logged-in">
							<span class="tile-name">Tile name</span> has beed added to your favorites.
							<strong><a href="#" class="alert-link"><i class="fa fa-user"></i>&nbsp;View Favorites</a></strong>
						</div>
						<div class="alert-not-logged-in">
							<span>You are not logged in. Please <strong><a href="#" class="alert-link"><i class="fa fa-sign-in"></i>&nbsp;login</a></strong> to fav this tile</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php include ("templates/footer.php"); ?>