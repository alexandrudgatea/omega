<?php include ("templates/header.php"); ?>

<div id="hero" class="homepage">
	<div id="homeCarousel" class="carousel slide carousel-fade" data-ride="carousel">
		<div class="carousel-inner">
			<div class="item active" style="background: url('images/hero.jpeg')">
				<div class="item-caption animated fadeInUpMod">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<div class="caption-content">
									<span class="omega-moment-tag text-uppercase">#MyOmegaMoment</span>
									<span class="caption-text">Out on a hike in the marin Headlines.</span>
									<span class="caption-author-date">Stephanie, 06/15/16</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="item" style="background: url('images/hero2.jpeg')">
				<div class="item-caption animated fadeInUpMod">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<div class="caption-content">
									<span class="omega-moment-tag text-uppercase">#MyOmegaMoment</span>
									<span class="caption-text">Out on a hike in the marin Headlines.</span>
									<span class="caption-author-date">Stephanie, 06/15/15</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="item" style="background: url('images/hero3.jpeg')">
				<div class="item-caption animated fadeInUpMod">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<div class="caption-content">
									<span class="omega-moment-tag text-uppercase">#MyOmegaMoment</span>
									<span class="caption-text">Out on a hike in the marin Headlines.</span>
									<span class="caption-author-date">Stephanie, 06/15/14</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="mainContent">
	<!--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
	<!-- TILES-->
	<div class="tiles-grid">
		<div class="container">
			<!--IDEA TILES-->
			<div class="row">
				<div class="col-sm-4">
					<div class="panel panel-default  tile panel-category" style="background: url('images/workshop_events.jpg')">
						<div class="panel-body">
							<div class="category">
								<h1><a href="#!">Workshops &amp; Events</a></h1>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default  tile panel-workshop">
						<div class="favourite">
							<div class="checkbox">
								<input type="checkbox" value="" id="favourite6">
								<label for="favourite6"></label>
							</div>
						</div>
						<div class="type"> Scolarship</div>
						<div class="panel-body">
							<div class="content-type">
								<span>Workshop</span>
							</div>
							<div class="title">
								<h1>Collaborating With Nature</h1>
							</div>
							<div class="person">
								<ul class="list-unstyled list-inline person-images">
									<li>
										<img src="images/person.jpg">
									</li>
									<li> <img src="images/person.jpg"></li>
									<li> <img src="images/person.jpg"></li>
									<li> <a href="#!" class="more field-text block text-center full-height"><i class="fa fa-plus"></i></a> </li>
								</ul>
								<ul class="list-unstyled list-inline person-names">
									<li>Thomas Bell</li>
									<li>Marry Ann</li>
									<li>Michael Doe</li>
									<li>more</li>
								</ul>
							</div>
							<div class="date-location">
								<ul class="list-inline">
									<li><em>Feb 28 - Mar 06 2016</em></li>
									<li><em>Rhinebeck, NY</em></li>
								</ul>
							</div>
						</div>
						<div class="panel-footer">
							<a href="#!" class="expand-button">
								<i class="fa fa-plus"></i>
							</a>
							<div class="expanded-tile-content animated fadeInUp">
								<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
								<a href="#!" class="btn white-transparent-button">Learn More</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default  tile panel-learn">
						<div class="favourite">
							<div class="checkbox">
								<input type="checkbox" value="" id="favourite9">
								<label for="favourite9"></label>
							</div>
						</div>
						<div class="panel-body">
							<div class="content-type">
								<span>E-course</span>
							</div>
							<div class="title">
								<h1>Growing &amp; Sustaining A Yoga Service Organization</h1>
							</div>
							<div class="person">
								<ul class="list-unstyled list-inline person-images">
									<li>
										<img src="images/person.jpg">
									</li>
									<li> <img src="images/person.jpg"></li>
									<li> <img src="images/person.jpg"></li>
								</ul>
								<ul class="list-unstyled list-inline person-names">
									<li>Thomas Robertson</li>
									<li>Marry Anderson</li>
									<li>Marry Anderson</li>
								</ul>
							</div>

						</div>
						<div class="panel-footer">
							<a href="#!" class="expand-button">
								<i class="fa fa-plus"></i>
							</a>
							<div class="expanded-tile-content animated fadeInUp">
								<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
								<a href="#!" class="btn white-transparent-button">Learn More</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="panel panel-default  tile panel-general-purpose">
						<div class="favourite">
							<div class="checkbox">
								<input type="checkbox" value="" id="favourite13">
								<label for="favourite13"></label>
							</div>
						</div>
						<div class="panel-heading" style="background: url('images/panel_idea.jpg')">

						</div>
						<div class="panel-body panel-article">
							<div class="content-type">
								<span>Take Action</span>
							</div>
							<div class="title">
								<h1>Learning to let go</h1>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default  tile panel-learn">
						<div class="favourite">
							<div class="checkbox">
								<input type="checkbox" value="" id="favourite9">
								<label for="favourite9"></label>
							</div>
						</div>
						<div class="panel-body">
							<div class="content-type">
								<span>E-course</span>
							</div>
							<div class="title">
								<h1>Growing &amp; Sustaining A Yoga Service Organization</h1>
							</div>
							<div class="person">
								<ul class="list-unstyled list-inline person-images">
									<li>
										<img src="images/person.jpg">
									</li>
									<li> <img src="images/person.jpg"></li>
									<li> <img src="images/person.jpg"></li>
								</ul>
								<ul class="list-unstyled list-inline person-names">
									<li>Thomas Robertson</li>
									<li>Marry Anderson</li>
									<li>Marry Anderson</li>
								</ul>
							</div>
						</div>
						<div class="panel-footer">
							<a href="#!" class="expand-button">
								<i class="fa fa-plus"></i>
							</a>
							<div class="expanded-tile-content animated fadeInUp">
								<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
								<a href="#!" class="btn white-transparent-button">Learn More</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default  tile panel-category" style="background: url('images/online_learning.jpg')">
						<div class="panel-body">
							<div class="category">
								<h1><a href="#!">Online Learning</a></h1>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="panel panel-default  tile panel-category" style="background: url('images/idea.jpg')">
						<div class="panel-body">
							<div class="category">
								<h1><a href="#!">Ideas</a></h1>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default  tile panel-workshop">
						<div class="favourite">
							<div class="checkbox">
								<input type="checkbox" value="" id="favourite6">
								<label for="favourite6"></label>
							</div>
						</div>
						<div class="panel-body">
							<div class="content-type">
								<span>Workshop</span>
							</div>
							<div class="title">
								<h1>Collaborating With Nature</h1>
							</div>
							<div class="person">
								<ul class="list-unstyled list-inline person-images">
									<li>
										<img src="images/person.jpg">
									</li>
									<li> <img src="images/person.jpg"></li>
								</ul>
								<ul class="list-unstyled list-inline person-names">
									<li>Thomas Robertson</li>
									<li>Marry Anderson</li>
								</ul>
							</div>
							<div class="date-location">
								<ul class="list-inline">
									<li><em>Feb 28 - Mar 06 2016</em></li>
									<li><em>Rhinebeck, NY</em></li>
								</ul>
							</div>
						</div>
						<div class="panel-footer">
							<a href="#!" class="expand-button">
								<i class="fa fa-plus"></i>
							</a>
							<div class="expanded-tile-content animated fadeInUp">
								<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
								<a href="#!" class="btn white-transparent-button">Learn More</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default  tile panel-idea">
						<div class="favourite">
							<div class="checkbox">
								<input type="checkbox" value="" id="favourite2">
								<label for="favourite2"></label>
							</div>
						</div>
						<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
						</div>
						<div class="panel-body panel-audio-video">
							<div class="content-type">
								<a href="#!"><i class="fa fa-play-circle-o"></i></a>
								<span>Video</span>
							</div>
							<div class="title">
								<h1>Bobby McFerrin: Circle Songs from Omega</h1>
							</div>
							<div class="person">
								<ul class="by list-unstyled list-inline">
									<li>Thomas Robertson</li>
									<li>Marry Anderson</li>
								</ul>
								<ul class="featuring list-unstyled list-inline">
									<li>Thomas Robertson</li>
									<li>Marry Anderson</li>
								</ul>
							</div>
							<div class="duration-date">
								<em class="duration">13 minute watch</em>
								<em class="date-release">13th of September, 2016</em>
							</div>
						</div>
						<div class="panel-footer">
							<a href="#!" class="expand-button">
								<i class="fa fa-plus"></i>
							</a>
							<div class="expanded-tile-content animated fadeInUp">
								<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
								<a href="#!" class="btn white-transparent-button">Learn More</a>
							</div>
						</div>

					</div>
				</div>
			</div>

		</div>
	</div>
	<!-- TILES END-->
	<!--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
	<!-- QUOTE SECTION-->
	<div class="quote-section">
		<div id="quoteCarousel" class="carousel slide carousel-fade" data-ride="carousel">
			<div class="carousel-inner">
				<div class="item active" style="background: url('images/quote-section_large.jpg')">
					<div class="container full-height">
						<div class="row">
							<div class="col-sm-8">
								<h1 class="quote-text"> "I lead with my own unique story of struggle and close with the principles, insights, and strategies I used to overcome them to live more powerfully." </h1>
								<div class="author">
									<a href="#!" class="author-name block top-bar-teal">Preston Smiles</a>
									<span class="author-ocupation block">Omega Instructor</span>
									<a href="#quoteCarousel" data-slide="next" class="btn cta icon-button"><i class="glyphicon glyphicon-repeat icon"></i>See Another</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="item" style="background: url('images/quote-section_large.jpg')">
					<div class="container full-height">
						<div class="row">
							<div class="col-sm-8">
								<h1 class="quote-text"> "I lead with my own unique story of." </h1>
								<div class="author">
									<a href="#!" class="author-name block top-bar-teal">Preston Smiles</a>
									<span class="author-ocupation block">Omega Instructor</span>
									<a href="#quoteCarousel" data-slide="next" class="btn cta icon-button"><i class="glyphicon glyphicon-repeat icon"></i>See Another</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="item" style="background: url('images/quote-section_large.jpg')">
					<div class="container full-height">
						<div class="row">
							<div class="col-sm-8">
								<h1 class="quote-text"> "I lead with my own unique story of struggle and close with the principles, insights.." </h1>
								<div class="author">
									<a href="#!" class="author-name block top-bar-teal">Preston Smiles</a>
									<span class="author-ocupation block">Omega Instructor</span>
									<a href="#quoteCarousel" data-slide="next" class="btn cta icon-button"><i class="glyphicon glyphicon-repeat icon"></i>See Another</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- WOMEN LEADERSHIP SECTION-->
	<div class="women-leadership tiles-grid" style="background: url('images/women-leadership.jpg')">
		<div class="women-leadership-mobile background-image visible-xs" style="background: url('images/women-leadership.jpg')"></div>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-push-4">
					<div class="row">
						<div class="col-sm-9 col-sm-push-3">
							<h1 class="section-title top-bar-orange">The Omega Women’s Leadership Center</h1>
							<span class="block section-subtitle">
									Supporting and growing a community of women who are leading global change.
								</span>
							<a href="#!" class="btn orange-bg">Learn More</a>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="panel panel-default  tile panel-idea">
								<div class="favourite">
									<div class="checkbox">
										<input type="checkbox" value="" id="favourite2">
										<label for="favourite2"></label>
									</div>
								</div>
								<div class="panel-heading" style="background: url('images/panel_idea.jpg')">
								</div>
								<div class="panel-body panel-audio-video">
									<div class="content-type">
										<a href="#!"><i class="fa fa-play-circle-o"></i></a>
										<span>Video</span>
									</div>
									<div class="title">
										<h1>Bobby McFerrin: Circle Songs from Omega</h1>
									</div>
									<div class="person">
										<ul class="by list-unstyled list-inline">
											<li>Thomas Robertson</li>
											<li>Marry Anderson</li>
										</ul>
										<ul class="featuring list-unstyled list-inline">
											<li>Thomas Robertson</li>
											<li>Marry Anderson</li>
										</ul>
									</div>
									<div class="duration-date">
										<em class="duration">13 minute watch</em>
										<em class="date-release">13th of September, 2016</em>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="panel panel-default  tile panel-learn">
								<div class="favourite">
									<div class="checkbox">
										<input type="checkbox" value="" id="favourite9">
										<label for="favourite9"></label>
									</div>
								</div>
								<div class="panel-body">
									<div class="content-type">
										<span>E-course</span>
									</div>
									<div class="title">
										<h1>Growing &amp; Sustaining A Yoga Service Organization</h1>
									</div>
									<div class="person">
										<ul class="list-unstyled list-inline person-images">
											<li>
												<img src="images/person.jpg">
											</li>
											<li> <img src="images/person.jpg"></li>
											<li> <img src="images/person.jpg"></li>
										</ul>
										<ul class="list-unstyled list-inline person-names">
											<li>Thomas Robertson</li>
											<li>Marry Anderson</li>
											<li>Marry Anderson</li>
										</ul>
									</div>
								</div>
								<div class="panel-footer">
									<a href="#!" class="expand-button">
										<i class="fa fa-plus"></i>
									</a>
									<div class="expanded-tile-content animated fadeInUp">
										<span class="field-text text-block">Bobby McFerrin leads 200+ Omega workshop participants in joyful song at the conclusion of Circlesongs 2014. Described as a life-changing journey by attendees, this improvisational singing workshop with Bobby McFerrin invites you to play, sing, and create—in music and in life. Many students make this workshop a yearly ritual.</span>
										<a href="#!" class="btn white-transparent-button">Learn More</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="panel panel-default  tile panel-collection">
								<div class="favourite">
									<div class="checkbox">
										<input type="checkbox" value="" id="favourite10">
										<label for="favourite10"></label>
									</div>
								</div>
								<div class="panel-body">
									<div class="content-type">
										<span>Playlist</span>
									</div>
									<div class="title">
										<h1>Pema Chodron's Words of Wisdom</h1>
									</div>
									<div class="image-tiles">
										<ul class="list-inline">
											<li style="background:url('images/workshop_events.jpg');"></li>
											<li style="background:url('images/person.jpg');"></li>
											<li style="background:url('images/online_learning.jpg');"></li>
											<li style="background:url('images/idea.jpg');"></li>
										</ul>
									</div>
									<div class="person">
										<ul class="list-unstyled list-inline person-names">
											<li>Thomas Robertson</li>
											<li>Marry Anderson</li>
											<li>Marry Anderson</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="panel panel-default  tile panel-general-purpose">
								<div class="favourite">
									<div class="checkbox">
										<input type="checkbox" value="" id="favourite13">
										<label for="favourite13"></label>
									</div>
								</div>
								<div class="panel-heading" style="background: url('images/panel_idea.jpg')">

								</div>
								<div class="panel-body panel-article">
									<div class="content-type">
										<span>Take Action</span>
									</div>
									<div class="title">
										<h1>Learning to let go</h1>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- SERVICES SECTION-->
	<div class="panel-pane pane-views-panes pane-homepage-initiatives-list-panel-initiatives-list">

		<h2 class="pane-title">
			See all the ways we are cultivating a moral compassionate, connected, balanced world. </h2>


		<div class="pane-content">
			<div class="view view-homepage-initiatives-list view-id-homepage_initiatives_list view-display-id-panel_initiatives_list view-dom-id-8a5d5800db4db88d1e0879ac150e478c">



				<div class="view-content ">
					<div class="initiatives tiles-grid">
						<div class="container">
							<div class="row">
								<div class="col-sm-12">
									<ul class="initiatives-list list-inline">
										<li>
											<div class="initiative-content" style="background: url('http://test2.eomega.org/sites/default/files/initiative6_0.jpg')">
												<a href="/key-initiative/community-engagement" class="block">
													<span class="initiative">Community Engagement</span>
												</a>
											</div>
										</li>
										<li>
											<div class="initiative-content" style="background: url('http://test2.eomega.org/sites/default/files/initiative4.jpg')">
												<a href="/key-initiative/mindfulness" class="block">
													<span class="initiative">Mindfulness</span>
												</a>
											</div>
										</li>
										<li>
											<div class="initiative-content" style="background: url('http://test2.eomega.org/sites/default/files/initiative2.jpg')">
												<a href="/key-initiative/nonprofit-retreats-at-omega" class="block">
													<span class="initiative">Nonprofit Retreats at Omega</span>
												</a>
											</div>
										</li>
										<li>
											<div class="initiative-content" style="background: url('http://test2.eomega.org/sites/default/files/initiative1.jpg')">
												<a href="/key-initiative/omega-center-sustainable-living" class="block">
													<span class="initiative">Omega Center for Sustainable Living</span>
												</a>
											</div>
										</li>
										<li>
											<div class="initiative-content" style="background: url('http://test2.eomega.org/sites/default/files/initiative3.jpg')">
												<a href="/key-initiative/veterans-initiatives" class="block">
													<span class="initiative">Veterans Initiatives</span>
												</a>
											</div>
										</li>
										<li>
											<div class="initiative-content" style="background: url('http://test2.eomega.org/sites/default/files/initiative5.jpg')">
												<a href="/key-initiative/yoga-service" class="block">
													<span class="initiative">Yoga Service</span>
												</a>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- CAMPUS OVERVIEW-->
	<div class="campus" style="background: url('images/campus_large.jpg')">
		<div class="campus-mobile background-image visible-xs" style="background: url('images/campus_large.jpg')"></div>
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-sm-push-1">
					<h1 class="section-title">Rest. De-stress. Renew. Sometimes awakening just means slowing down.</h1>
					<span class="section-subtitle top-bar-teal block">A short introduction to the Omega
campuses will go here in this quick little blurb of copy.</span>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<ul class="campus-tiles list-inline">
						<li>
							<a href="#!" class="block">
								<span class="initiative">Getaway Retreats</span>
							</a>
						</li>
						<li>
							<a href="#!" class="block">
								<span class="initiative">Theme Weeks</span>
							</a>
						</li>
						<li>
							<a href="#!" class="block">
								<span class="initiative">Wellness Center</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!--OUR SPONSORS-->
	<div class="sponsors">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h1 class="section-title-big interractive-text text-center bottom-bar-teal">Our Sponsors</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 text-center">
					<ul class="sponsor-list list-inline">
						<li>
							<a href="#!" target="_blank"><img src="images/sponsor1.png" alt="Sponsor 1"></a>
						</li>
						<li>
							<a href="#!" target="_blank"><img src="images/sponsor2.png"></a>
						</li>
						<li>
							<a href="#!" target="_blank"><img src="images/sponsor3.png"></a>
						</li>
						<li>
							<a href="#!" target="_blank"><img src="images/sponsor4.png"></a>
						</li>
						<li>
							<a href="#!" target="_blank"><img src="images/sponsor5.png"></a>
						</li>
						<li>
							<a href="#!" target="_blank"><img src="images/sponsor6.png"></a>
						</li>
						<li>
							<a href="#!" target="_blank"><img src="images/sponsor7.png"></a>
						</li>
						<li>
							<a href="#!" target="_blank"><img src="images/sponsor8.png"></a>
						</li>
						<li>
							<a href="#!" target="_blank"><img src="images/sponsor9.png"></a>
						</li>
						<li>
							<a href="#!" target="_blank"><img src="images/sponsor10.png"></a>
						</li>
					</ul>
					<a href="#!" class='interractive-text text-center see-more'>+ See more</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include ("templates/footer.php"); ?>
